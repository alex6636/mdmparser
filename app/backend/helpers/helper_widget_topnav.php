<?php
/**
 * @package  pFramework
 * @class    helper_widget_topnav
 * @author   Alex Sergey (createtruesite@gmail.com)
 * @date     October, 2013
 * @version  $Id
 *
 * Helper to navigate on the page top
 */

namespace app\backend\helpers;
use lib\pf\session;

class helper_widget_topnav extends helper_widget_nav_base
{
    /**
     * Constructor
     *
     * @param string $view_name
     * @param array $view_params
     */
    public function __construct($view_name, array $view_params = NULL) {
        parent::__construct($view_name, $view_params);
        $session = session::get_instance();
        if (isset($session->id, $session->login)) {
            $this->get_view()->extract(array(
                'id'   =>$session->id,
                'login'=>$session->login
            ));
        }
    }

    /**
     * Getting navigation
     *
     * @return array
     */
    public function get_nav() {
        $router = $this->get_controller()->get_router();
        return array(
            array('Home', $router->assemble_route('backend', array('controller'=>'system'))),
            '|',
            array('Administrators', $router->assemble_route('backend', array('controller'=>'admin'))),
            '|',
            array('Visit site', '/'),
        );
    }
}