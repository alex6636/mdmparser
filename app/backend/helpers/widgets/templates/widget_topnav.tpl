{{ extends _widget }}
<div id="nav">
	<a title="" href="{{ $router->assemble_route('backend', array('controller'=>'system', 'action'=>'logout')) }}">{{ $app->t('Log out', 'backend') }}</a>
</div>