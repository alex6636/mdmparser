<?php
/**
 * @package  pFramework
 * @class    helper_widget_base
 * @author   Alex Sergey (createtruesite@gmail.com)
 * @date     September, 2013
 * @version  $Id
 *
 * Base widget
 */

namespace app\backend\helpers;
use lib\pf\pf;
use lib\pf\helpers\helper_widget;

abstract class helper_widget_base extends helper_widget
{
    /**
     * Getting view configuration
     *
     * @return array
     */
    public function get_view_config() {
        return array(
            'tpl_dir'=>PF_APP_DIR . '/backend/helpers/widgets/templates',
            'cache_lifetime'=>0
        ) + (array) pf::get_app()->get_config()->get('template');
    }
}