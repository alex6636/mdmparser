<?php
/**
 * @package  pFramework
 * @class    helper_widget_nav_base
 * @author   Alex Sergey (createtruesite@gmail.com)
 * @date     October, 2013
 * @version  $Id
 *
 * Helper to navigate base
 */

namespace app\backend\helpers;

abstract class helper_widget_nav_base extends helper_widget_base
{
    /**
     * Constructor
     *
     * @param string $view_name
     * @param array $view_params
     */
    public function __construct($view_name, array $view_params = NULL) {
        parent::__construct($view_name, $view_params);
        $this->view->set_param('nav', $this->get_nav());
    }

    /**
     * Getting navigation
     *
     * @return array
     */
    abstract public function get_nav();
}