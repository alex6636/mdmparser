<?php
/**
 * @package Backend
 * @class   controller_admin
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    November, 2013
 * @version $Id
 *
 * Admin controller
 */

namespace app\backend\controllers;
use app\backend\models\model_admin_access;

class controller_admin extends controller_action
{
    /**
     * Access role
     *
     * @return int
     */
    protected function get_access_role() {
        return model_admin_access::ROLE_SUPERADMIN;
    }
}