<?php
/**
 * @package Backend
 * @class   controller_parser_yelp
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Parser yelp.com
 */

namespace app\backend\controllers;
use app\backend\parser\parser_yelp_results;
use lib\pf\pf;
use lib\pf\arr;
use \Exception;

class controller_parser_yelp extends controller_parser_base
{
    public function action_index() {
        $result = array();
        list($keyword, $location, $fields, $export_format) =
            arr::extract($_REQUEST, array('keyword'=>'', 'location'=>'', 'fields'=>'', 'export_format'=>''), false);
        if (!empty($keyword) && !empty($location) && !empty($fields) && !empty($export_format)) {
            $config = (array) pf::get_app()->get_config()->get(array('parser'=>'yelp')) + array(
                'limit'=>-1,
                'log'=>true,
            );
            $parser = new parser_yelp_results(array_keys($fields));
            $parser->set_limit($config['limit'])->set_log($config['log']);
            try {
                $results = $parser->get_results();
                if (!empty($results)) {
                    $filename = 'yelp_' . date('m-d-Y-H-i-s') . '_' . $keyword . '_' . $location . '.csv';
                    $result = $this->get_result_save($results, $fields, $filename, $export_format);
                }
            } catch(Exception $exception) {
                $result = $this->get_result_error($exception);
            }
        }
        echo json_encode($result);
    }

    protected function get_files_dir() {
        return PARSER_FILES_YELP_DIR;
    }
}