<?php
/**
 * @package Backend
 * @class   controller_auth
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Controller for authorization to the system
 */

namespace app\backend\controllers;
use app\backend\models\model_admin_access;
use app\backend\models\model_login;
use app\backend\models\model_admin;
use app\backend\models\model_auth_form;
use lib\captcha\SimpleCaptcha;

class controller_auth extends controller_base
{
    protected $model_admin;

    /**
     * Constructor
     */
    public function __construct() {
        $this->model_admin = new model_admin;
        parent::__construct();
    }

    /**
     * Login index action
     */
    public function action_index() {
        $model_login = new model_login;
        $view = $this->get_default_view();
        if ($this->request->is_method('POST')) {
            $model_auth_form = new model_auth_form;
            if (!$model_login->is_allowed()) {
                $model_auth_form->valid_captcha(true);
            }
            list($values, $errors) = $model_auth_form->handle_form($_POST);
            if (empty($errors)) {
                if ($this->model_admin->login($values['login'], $values['password'], $values['remember'])) {
                    $model_login->delete_attempts();
                    $this->redirect_back();
                }
                $model_login->increase_attempts();
                if ($message = $this->model_admin->get_message()->get_last()) {
                    $view->extract($message);
                }
            } else {
                $view->error = $errors;
            }
        }
        if (!$model_login->is_allowed()) {
            $view->captcha = true;
        }
    }

    /**
     * Recovery password action
     */
    public function action_recovery() {
        $view = $this->get_default_view();
        if ($this->request->is_method('POST')) {
            $model_auth_form = new model_auth_form;
            list($values, $errors) = $model_auth_form->handle_form($_POST);
            if (empty($errors)) {
                $recovery_key = $this->model_admin->generate_unique_key();
                $recovery_url = 'http://' . $_SERVER['SERVER_NAME'] . $this->router->assemble_route(
                    'backend', array('controller'=>'auth', 'action'=>'changepass', 'param1'=>$recovery_key)
                );
                $this->model_admin->recovery($values['email'], $recovery_key, $recovery_url);
                if ($message = $this->model_admin->get_message()->get_last()) {
                    $view->extract($message);
                }
            } else {
                $view->error = $errors;
            }
        }
    }

    /**
     * Change password action
     *
     * @return bool false if the recovery key is not passed
     */
    public function action_changepass() {
        $recovery_key = $this->request->get_param('param1');
        $view = $this->get_default_view();
        if (!$recovery_key) {
            return false;
        }
        if ($this->request->is_method('POST')) {
            $model_auth_form = new model_auth_form;
            list($values, $errors) = $model_auth_form->handle_form($_POST);
            if (empty($errors)) {
                $this->model_admin->change_password_by_recovery_key($values['password'], $recovery_key);
                if ($message = $this->model_admin->get_message()->get_last()) {
                    $view->extract($message);
                }
            } else {
                $view->error = $errors;
            }
        }
        return true;
    }

    /**
     * Captcha
     */
    public function action_captcha() {
        $captcha = new SimpleCaptcha();
        $captcha->minWordLength = 4;
        $captcha->maxWordLength = 6;
        $captcha->CreateImage();
    }

    /**
     * Checking access
     *
     * @return controller_base
     */
    protected function check_access() {
        if (model_admin_access::is_login()) {
            $this->request->redirect($this->router->assemble_route(
                'backend', array('controller'=>'system')
            ));
        } elseif ($this->model_admin->is_remembered()) {
            $this->redirect_back();
        }
        return $this;
    }

    /**
     * Return to back
     *
     * @param string $param name of the request parameter,
     * value is used as an URL
     */
    protected function redirect_back($param = 'param1') {
        $back_url = $this->request->get_param($param);
        $this->request->redirect(
            $back_url ? '/' . base64_decode($back_url) : $this->router->assemble_route('backend')
        );
    }
}