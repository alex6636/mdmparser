<?php
/**
 * @package Backend
 * @class   controller_action
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    June, 2014
 * @version $Id
 *
 * Controller for administration panel, CRUD
 */

namespace app\backend\controllers;
use app\backend\models\model_action;
use app\backend\models\model_form;
use lib\pf\exceptions\exception_runtime;
use lib\pf\exceptions\exception_argument;

abstract class controller_action extends controller_base
{
   /** @var model_action $model */
    protected $model;

   /** @var model_form $model_form */
    protected $model_form;

    protected
        $route_param1 = 'param1',
        $route_param2 = 'param2';

    /**
     * Getting model name
     *
     * @return string
     */
    public function get_model_name() {
        return '\app\backend\models\model_' . $this->request->get_route()->get_controller();
    }

    /**
     * Getting model form name
     *
     * @return string
     */
    public function get_model_form_name() {
        return '\app\backend\models\model_' . $this->request->get_route()->get_controller() . '_form';
    }

    /**
     * Getting route name
     *
     * @return string
     */
    public function get_route_name() {
        return 'backend';
    }

    /**
     * Getting model
     *
     * @throws exception_runtime if the model class does not exist
     * @throws exception_argument if the model is not a type of model_action
     * @return model_action
     */
    public function get_model() {
        if (!$this->model) {
            $model_name = $this->get_model_name();
            if (!class_exists($model_name)) {
                throw new exception_runtime($model_name, 1);
            }
            $this->model = new $model_name;
            if (!$this->model instanceof model_action) {
                throw new exception_argument('model_action', 7);
            }
        }
        return $this->model;
    }

    /**
     * Getting model form
     *
     * @throws exception_runtime if the model class does not exist
     * @throws exception_argument if the model is not a type of model_form
     * @return model_form
     */
    public function get_model_form() {
        if (!$this->model_form) {
            $model_form_name = $this->get_model_form_name();
            if (!class_exists($model_form_name)) {
                throw new exception_runtime($model_form_name, 1);
            }
            $this->model_form = new $model_form_name;
            if (!$this->model_form instanceof model_form) {
                throw new exception_argument('model_form', 7);
            }
        }
        return $this->model_form;
    }

    /**
     * Index action
     */
    public function action_index() {
        $model = $this->get_model();
        $view = $this->get_default_view();
        if ($message = $model->get_message()->get_last()) {
            $view->extract($message);
        }
        list($view->rows, $view->links) = $model->get_pagination(
            $this->assemble_url(array('action'=>'index'))
        );
    }

    /**
     * Add action
     */
    public function action_add() {
        $view = $this->get_default_view();
        if ($this->request->is_method('POST')) {
             $model_form = $this->get_model_form();
            list($values, $errors) = $model_form->handle_form($_POST);
            if (empty($errors)) {
                $model = $this->get_model();
                if (!$model->add($values)) {
                    $view->extract($values);
                }
                if ($message = $model->get_message()->get_last()) {
                    $view->extract($message);
                }
            } else {
                $view->error = $errors;
                $view->extract($values);
            }
        }
    }

    /**
     * Edit action
     */
    public function action_edit() {
        $model = $this->get_model();
        $view = $this->get_default_view();
        $id = $this->request->get_param($this->route_param1);
        if ($this->request->is_method('POST')) {
            $model_form = $this->get_model_form();
            list($values, $errors) = $model_form->handle_form($_POST);
            if (empty($errors)) {
                $model->edit_by_id($values, $id);
                if ($message = $model->get_message()->get_last()) {
                    $view->extract($message);
                }
            } else {
                $view->error = $errors;
            }
            $view->extract($values);
        } else {
            $view->extract((array) $model->get_by_id($id));
        }
    }

    /**
     * Delete action
     */
    public function action_delete() {
        $model = $this->get_model();
        $id = $this->request->get_param($this->route_param1);
        $model->delete_by_id($id);
        $this->forward('index');
    }

    /**
     * Assemble URL for current controller
     *
     * @param array $params
     * @return string
     */
    public function assemble_url(array $params = NULL) {
        return $this->router->assemble_route(
            $this->get_route_name(), array('controller'=>$this->request->get_route()->get_controller()) + (array) $params
        );
    }
}