<?php
/**
 * @package Backend
 * @class   controller_base
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    November, 2013
 * @version $Id
 *
 * Base controller for administration panel
 */

namespace app\backend\controllers;
use app\backend\models\model_admin_access;
use lib\pf\controllers\controller_default;
use lib\pf\i18n\i18n;
use lib\pf\exceptions\exception_argument;

abstract class controller_base extends controller_default
{
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        $this->check_access()
             ->check_lang();
    }

    /**
     * Getting default view configuration
     *
     * @return array
     */
    public function get_default_view_config() {
        return array(
            'tpl_dir'=>PF_APP_DIR . '/backend/views/templates',
            'cache_lifetime'=>0
        ) + (array) $this->app->get_config()->get('template');
    }

    /**
     * Access role
     *
     * @return int
     */
    protected function get_access_role() {
        return model_admin_access::ROLE_ADMIN;
    }

    /**
     * Checking access
     *
     * @return controller_base
     */
    protected function check_access() {
        if (!model_admin_access::is_login()) {
            $back_url = $this->request->get_parser_url()->get_path();
            $this->request->redirect($this->router->assemble_route(
                'backend', array('controller'=>'auth', 'param1'=>base64_encode($back_url))
            ));
        } elseif (!model_admin_access::check_privilege($this->get_access_role())) {
            $this->request->redirect($this->router->assemble_route(
                'backend', array('controller'=>'system', 'action'=>'privilege')
            ));
        }
        return $this;
    }

    /**
     * Checking language
     *
     * @throws exception_argument if i18n is not initialized in the application
     * @return controller_base
     */
    protected function check_lang() {
        return $this;
        /*
        $i18n = $this->app->get_i18n();
        if (!$i18n instanceof i18n) {
            throw new exception_argument('i18n', 7);
        }
        if (($lang = $this->request->get_param('lang')) && $i18n->change_lang($lang)) {
            $route = $this->request->get_route();
            $this->request->redirect(
                $route->assemble_route(array('lang'=>$lang) + $route->get_all_params())
            );
        }
        return $this;
        */
    }
}