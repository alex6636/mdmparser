<?php
/**
 * @package Backend
 * @class   controller_system
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * System controller
 */

namespace app\backend\controllers;
use app\backend\models\model_admin_access;

class controller_system extends controller_base
{
    /**
     * Index action
     */
    public function action_index() {
        $this->request->redirect(
            $this->router->assemble_route('backend', array('controller'=>'parser'))
        );
    }

    /**
     * Logout action
     */
    public function action_logout() {
        model_admin_access::logout();
        $this->request->redirect(
            $this->router->assemble_route('backend', array('controller'=>'auth'))
        );
    }

    /**
     * Page for administrators who do not have privileges
     */
    public function action_privilege() {
        if (model_admin_access::check_privilege(model_admin_access::ROLE_SUPERADMIN)) {
            $this->request->redirect(
                $this->router->assemble_route('backend', array('controller'=>'system'))
            );
        }
    }
}