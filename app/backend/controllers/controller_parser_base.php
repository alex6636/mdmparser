<?php
/**
 * @package Backend
 * @class   controller_parser_base
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Parser base
 */

namespace app\backend\controllers;
use app\backend\parser\csv;
use app\backend\parser\csv2xls;
use lib\pf\arr;
use \Exception;

abstract class controller_parser_base extends controller_base
{
    public function action_download() {
        if ($filename = $this->request->get_param('param1')) {
            $file_path = $this->get_files_dir() . '/' . $filename;
            if (is_file($file_path)) {
                header('Content-Type: octet/stream');
                header('Content-Disposition: attachment;filename=' . $filename);
                echo file_get_contents($file_path);
                return true;
            }
        }
        return false;
    }

    abstract protected function get_files_dir();

    protected function get_result_save(array $results, array $fields, $filename, $file_format) {
        $filename = $this->prepare_filename($filename);
        $this->save2csv($filename, $fields, $results);
        if ($file_format === 'xls') {
            $filename = basename(csv2xls::convert($this->get_files_dir() . '/' . $filename));
        }
        return array(
            'file'=>array(
                'filename'=>$filename,
                'href'=>$this->router->assemble_route(
                    'backend',
                    array('controller'=>$this->request->get_route()->get_controller(), 'action'=>'download', 'param1'=>$filename)
                ),
                'count'=>count($results),
            )
        );
    }

    protected function get_result_error(Exception $exception) {
        return array(
            'error'=>array(
                'message'=>$exception->getMessage()
            ),
        );
    }

    protected function save2csv($filename, array $fields, array $results) {
        $csv = new csv; //$csv->set_delimeter(';');
        $csv_headers = array(array_values($fields));
        // sort results by headers
        $results = array_map(function($a) use ($fields) {
            return arr::sort_by_keys($a, array_keys($fields));
        }, $results);
        $file_path = $this->get_files_dir() . '/' . $filename;
        $csv->put_file($file_path, array_merge($csv_headers, $results));
    }

    protected function prepare_filename($filename) {
        return preg_replace("/-+/ui", '-', preg_replace("/[^\w-\.]/ui", '-', $filename));
    }
}