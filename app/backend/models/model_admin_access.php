<?php
/**
 * @package Backend
 * @class   model_admin_access
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * Model for checking administrator access
 */

namespace app\backend\models;
use lib\pf\session;
use lib\pf\cookie;

class model_admin_access
{
    const
        // roles
        ROLE_ADMIN      = 0,
        ROLE_SUPERADMIN = 1;

    protected static $cookie_remember = 'rkey';

    /**
     * Checking whether the administrator is logged
     *
     * @return bool
     */
    public static function is_login() {
        $session = session::get_instance();
        return (
            isset($session->id, $session->token, $session->role)
            &&
            $session->token === static::generate_token()
        );
    }

    /**
     * Checking privileges
     *
     * @param int $role
     * @return bool
     */
    public static function check_privilege($role = self::ROLE_ADMIN) {
        return (session::get_instance()->role >= $role);
    }

    /**
     * Logout
     */
    public static function logout() {
        $session = session::get_instance();
        unset($session->id, $session->login, $session->token, $session->role);
        cookie::delete(static::$cookie_remember);
    }

    /**
     * Getting administrator id
     *
     * @return bool
     */
    public static function get_admin_id() {
        $session = session::get_instance();
        return isset($session->id) ? $session->id : NULL;
    }

    /**
     * Setting access
     *
     * @param int $id
     * @param int $login
     * @param int $role
     */
    public static function set_access($id, $login, $role) {
        $session = session::get_instance();
        $session->id    = $id;
        $session->login = $login;
        $session->role  = $role;
        $session->token = static::generate_token();
    }

    /**
     * Getting key to remember administrator
     *
     * @return string
     */
    public static function get_remember_key() {
        return cookie::get(static::$cookie_remember);
    }

    /**
     * Setting key to remember administrator
     *
     * @param string $remember_key ключ
     * @return bool
     */
    public static function set_remember_key($remember_key) {
        return cookie::set(static::$cookie_remember, $remember_key);
    }

    /**
     * Deleting the key to remember administrator
     *
     * @return bool
     */
    public static function delete_remember_key() {
        return cookie::delete(static::$cookie_remember);
    }

    /**
     * Token generation
     *
     * @return string token
     */
    public static function generate_token() {
        return md5($_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT']);
    }
}