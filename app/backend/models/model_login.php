<?php
/**
 * @package Backend
 * @class   model_login
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    December, 2013
 * @version $Id
 *
 * Login model
 */

namespace app\backend\models;

class model_login extends model_action
{
    protected
        $max_attempts = 5,
        $interval     = 600;

    /**
     * Check authorization is available
     *
     * @return bool
     */
    public function is_allowed() {
        $query = 'SELECT attempts FROM ' . $this->get_table() . PHP_EOL .
                 'WHERE ip=:ip AND time>:time';
        $query_params = array(':ip'=>$_SERVER['REMOTE_ADDR'], ':time'=>time() - $this->interval);
        if ($attempts = $this->db_driver->query($query, $query_params)->fetchColumn()) {
            return ($attempts <= $this->max_attempts);
        }
        return true;
    }

    /**
     * Increasing the number of attempts
     *
     * @return model_login
     */
    public function increase_attempts() {
        $query = 'SELECT * FROM ' . $this->get_table() . PHP_EOL .
                 'WHERE ip=:ip';
        $query_params = array(':ip'=>$_SERVER['REMOTE_ADDR']);
        if ($auth = $this->db_driver->query($query, $query_params)->fetch()) {
            $this->edit_row_by_id(array(
                'attempts'=>$auth['attempts'] + 1,
                'time'=>time()
            ), $auth['id']);
        } else {
            $this->add_row(array(
                'ip'=>$_SERVER['REMOTE_ADDR'],
                'time'=>time()
            ));
        }
    }

    /**
     * Deleting the number of attempts
     *
     * @return model_login
     */
    public function delete_attempts() {
        $this->delete_row_by_field('ip', $_SERVER['REMOTE_ADDR']);
        $query = 'DELETE FROM ' . $this->get_table() . PHP_EOL .
                 'WHERE time<:time';
        $query_params = array(':time'=>time() - $this->interval);
        $this->db_driver->query($query, $query_params);
        return $this;
    }
}