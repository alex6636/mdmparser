<?php
/**
 * @package Backend
 * @class   model_config_form
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    June, 2014
 * @version $Id
 *
 * Configuration form
 */

namespace app\backend\models;
use lib\pf\pf;

class model_config_form extends model_form
{
    /**
     * Rules
     *
     * @return array
     */
    public function get_rules() {
        return array(
            array('db[pdo][dsn]', model_form::FIELD_TEXT, 'The connection string can not be empty'),
            array('db[pdo][username]', model_form::FIELD_TEXT, 'The username can not be empty'),
            array('db[pdo][password]', model_form::FIELD_TEXT),
            array('template[cache_lifetime]', 'intval'),
            array('pagination[num_rows]', 'intval', 'Number of rows must be greater than zero',
                function ($val) {
                    return $val > 0;
                }),
            array('pagination[num_links]', 'intval', 'Number of links must be greater than zero',
                function ($val) {
                    return $val > 0;
                }),
            array('i18n[default]', model_form::FIELD_TEXT, 'Selected language is invalid',
                function ($lang) {
                    return in_array($lang, (array) pf::get_app()->get_config()->get(array('i18n'=>'allowed_langs')));
                }),
            array('i18n[auto_detect]', model_form::FIELD_FLAG),
        );
    }
}