<?php
/**
 * @package Backend
 * @class   model_form
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    November, 2013
 * @version $Id
 *
 * Model form
 */

namespace app\backend\models;
use lib\pf\form;

abstract class model_form extends form
{

}