<?php
/**
 * @package Backend
 * @class   model_action
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    November, 2013
 * @version $Id
 *
 * Model CRUD
 */

namespace app\backend\models;
use lib\pf\db\table_row;
use lib\pf\pf;
use lib\pf\arr;
use lib\pf\str;
use lib\pf\message;
use lib\pf\paginators\paginator_db;
use lib\pf\db\drivers\driver_pdo;

abstract class model_action extends table_row
{
   /** @var message $message */
    protected $message;

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct($this->get_db_driver_instance());
         $this->message = new message($this->messages());
    }

    /**
     * Getting table name
     *
     * @return string
     */
    public function get_table() {
        // replace \ by / for linux basename
        return str::ltrim_substr(basename(str_replace('\\', '/', get_called_class())), 'model_');
    }

    /**
     * Getting by id
     *
     * @param mixed $id
     * @param string $fields
     * @return mixed data array of table row
     * in PDO::ATTR_DEFAULT_FETCH_MODE format or false
     */
    public function get_by_id($id, $fields = '*') {
        return $this->get_row_by_id($id, $fields);
    }

    /**
     * Adding
     *
     * @param array $arr as
     * array('field_name'=>'field_value'[,'field_name2'=>'field_value2',[...]])
     * @return string last inserted id
     */
    public function add(array $arr) {
        $insert_id = $this->add_row($arr);
        $this->message->add('add', $insert_id ? 'success' : 'error');
        return $insert_id;
    }

    /**
     * Editing by id
     *
     * @param array $arr as
     * array('field_name'=>'field_value'[,'field_name2'=>'field_value2',[...]])
     * @param int $id
     * @return int $affected_rows number of updated rows
     */
    public function edit_by_id(array $arr, $id) {
        $affected_rows = $this->edit_row_by_id($arr, $id);
        $this->message->add('edit', 'success');
        return $affected_rows;
    }

    /**
     * Deleting by id
     *
     * @param mixed $id
     * @return int $deleted_rows number of deleted rows
     */
    public function delete_by_id($id) {
        $deleted_rows = $this->delete_row_by_id($id);
        $this->message->add('delete', $deleted_rows ? 'success' : 'error');
        return $deleted_rows;
    }

    /**
     * Pagination
     *
     * @param string $url
     * @return array as
     * array(array $rows, mixed $links)
     */
    public function get_pagination($url = NULL) {
        $query = 'SELECT * FROM ' . $this->get_table() . PHP_EOL .
                 'ORDER BY ' . $this->get_table_pk();
        $paginator = new paginator_db($this->db_driver, $query);
        $config = (array) pf::get_app()->get_config()->get('pagination');
        $paginator->set_num_rows(arr::get($config, 'num_rows', 20));
        $paginator->set_num_links(arr::get($config, 'num_links', 3));
        if ($url !== NULL) {
            $paginator->set_script_url($url);
        }
        return $paginator->get_pagination();
    }

    /**
     * Getting message
     *
     * @return message
     */
    public function get_message() {
        return $this->message;
    }

    /**
     * Getting database driver
     *
     * @return driver_pdo
     */
    protected function get_db_driver_instance() {
        return pf::get_app()->get_db_driver();
    }

    /**
     * Messages about the results of implementation of action
     *
     * @return array as
     * array(
     *     'action'=>array(
     *         'type'=>'message'[,...]
     *     ) [,...]
     * )
     */
    protected function messages() {
        return array(
            'add'=>array(
                'success'=>'The data has been added',
                'error'  =>'Failed to add data',
            ),
            'edit'=>array(
                'success'=>'The data has been edited',
                'error'  =>'Failed to edit data',
            ),
            'delete'=>array(
                'success'=>'The data has been deleted',
                'error'  =>'Failed to delete data',
            ),
        );
    }
}