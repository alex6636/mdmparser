<?php
/**
 * @package Backend
 * @class   model_admin_form
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    November, 2013
 * @version $Id
 *
 * Model admin form
 */

namespace app\backend\models;

class model_admin_form extends model_form
{
    /**
     * Rules
     *
     * @return array
     */
    public function get_rules() {
        return array(
            array('email',    model_form::FIELD_TEXT, 'Please enter email', model_form::VALIDATE_EMAIL),
            array('login',    model_form::FIELD_TEXT, 'Login length must be 3 to 20 characters',
                model_form::VALIDATE_LENGTH, array('min'=>3, 'max'=>20)),
            array('password', model_form::FIELD_TEXT, 'Password length must be minimum 5 characters',
                model_form::VALIDATE_LENGTH, array('min'=>5), 'action'=>'add'),
            // for editing password is not required
            array('password', model_form::FIELD_TEXT, 'Password length must be minimum 5 characters',
                model_form::VALIDATE_LENGTH, array('min'=>5), 'required'=>false, 'action'=>'edit'),
            array('role',     model_form::FIELD_FLAG)
        );
    }
}