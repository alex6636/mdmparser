<?php
/**
 * @package Backend
 * @class   model_auth_form
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Model for unauthorized administrators
 */

namespace app\backend\models;
use lib\pf\arr;

class model_auth_form extends model_form
{
    protected $valid_captcha = false;

    /**
     * Rules
     *
     * @return array $rules
     */
    public function get_rules() {
        $rules = array(
            // login
            array('login',    model_form::FIELD_TEXT, 'Please enter login', 'action'=>'index'),
            array('password', model_form::FIELD_TEXT, 'Please enter password', 'action'=>'index'),
            array('remember', model_form::FIELD_FLAG, 'action'=>'index'),
            // recovery
            array('email',    model_form::FIELD_TEXT, 'Please enter email', model_form::VALIDATE_EMAIL, 'action'=>'recovery'),
            // changepass
            array('password', model_form::FIELD_TEXT, 'Password length must be minimum 5 characters',
                  model_form::VALIDATE_LENGTH, array(5), 'action'=>'changepass'),
            array('confirm_password', model_form::FIELD_TEXT, 'Passwords do not match',
                  array($this, 'confirm'), 'action'=>'changepass')
        );
        if ($this->valid_captcha) {
            $rules[] = array('captcha', model_form::FIELD_TEXT, 'Please enter a valid code', model_form::VALIDATE_CAPTCHA);
        }
        return $rules;
    }

    /**
     * Captcha validation
     *
     * @param bool $flag
     * @return $this model_auth_form
     */
    public function valid_captcha($flag) {
        $this->valid_captcha = $flag;
        return $this;
    }

    /**
     * Checking password confirmation
     *
     * @param string $confirm_password
     * @return bool
     */
    public function confirm($confirm_password) {
        return ($confirm_password === arr::get($_REQUEST, 'password'));
    }
}