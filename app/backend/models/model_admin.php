<?php
/**
 * @package Backend
 * @class   model_admin
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    June, 2014
 * @version $Id
 *
 * Model administrator
 */

namespace app\backend\models;
use lib\pf\pf;
use lib\pf\mailers\mailer;

class model_admin extends model_action
{
    const PASSWORD_SALT = 'tKd&4ir*%dh#$a!f';

    /**
     * Adding
     *
     * @param array $arr as
     * array('field_name'=>'field_value'[,'field_name2'=>'field_value2',[...]])
     * @return string last inserted id
     */
    public function add(array $arr) {
        if ($this->get_row_by_field('email', $arr['email'])) {
            $this->message->add('check_email', 'error');
        } elseif ($this->get_row_by_field('login', $arr['login'])) {
            $this->message->add('check_login', 'error');
        } else {
            $arr['password'] = $this->generate_password_hash($arr['password']);
            return parent::add($arr);
        }
        return 0;
    }

    /**
     * Editing by id
     *
     * @param array $arr as
     * array('field_name'=>'field_value'[,'field_name2'=>'field_value2',[...]])
     * @param mixed $id
     * @return int number of updated rows
    */
    public function edit_by_id(array $arr, $id) {
        if ($this->get_row_by_field_ignore_id('email', $arr['email'], $id)) {
            $this->message->add('check_email', 'error');
        } elseif ($this->get_row_by_field_ignore_id('login', $arr['login'], $id)) {
            $this->message->add('check_login', 'error');
        } else {
            if ($arr['role'] < model_admin_access::ROLE_SUPERADMIN) {
                if (!$this->exists_other_superadmin($id)) {
                    $this->message->add('check_role', 'error');
                    return 0;
                }
            }
            if (empty($arr['password'])) {
                unset($arr['password']);
            } else {
                $arr['password'] = $this->generate_password_hash($arr['password']);
            }
            return parent::edit_by_id($arr, $id);
        }
        return 0;
    }

    /**
     * Deleting by id
     *
     * @param mixed $id
     * @return int $deleted_rows number of deleted rows
     */
    public function delete_by_id($id) {
        if (!$this->exists_other_superadmin($id) || ($id === model_admin_access::get_admin_id())) {
            $this->message->add('delete', 'error');
        } else {
            return parent::delete_by_id($id);
        }
        return 0;
    }

    /**
     * Login
     *
     * @param string $login_or_email
     * @param string $password
     * @param bool $remember flag
     * @return bool
     */
    public function login($login_or_email, $password, $remember = true) {
        $query  = 'SELECT * FROM ' . $this->get_table() . PHP_EOL .
                  'WHERE (login=:login_or_email OR email=:login_or_email) AND password=:password';
        $query_params = array(':login_or_email'=>$login_or_email, ':password'=>$this->generate_password_hash($password));
        if ($admin = $this->db_driver->query($query, $query_params)->fetch()) {
            model_admin_access::set_access($admin['id'], $admin['login'], $admin['role']);
            if ($remember) {
                $this->remember($admin['id']);
            }
            $this->edit_row_by_id(array('last_visit'=>time()), $admin['id']);
            return true;
        }
        $this->message->add('login', 'error');
        return false;
    }

    /**
     * Sending confirmation for password recovery
     *
     * @param string $email administrator email
     * @param string $recovery_key
     * @param string $recovery_url will be sent to administrator email
     * @return bool $result
     */
    public function recovery($email, $recovery_key, $recovery_url) {
        $result = false;
        $query = 'UPDATE ' . $this->get_table() . PHP_EOL .
                 'SET recovery_key=:recovery_key' . PHP_EOL .
                 'WHERE email=:email';
        $query_params = array(':recovery_key'=>$recovery_key, ':email'=>$email);
        if ($this->db_driver->query($query, $query_params)->rowCount()) {
            $subject = pf::get_app()->t('Password recovery confirmation', 'backend');
            $message = pf::get_app()->t('If you want to change your password, please, follow the link', 'backend') .
                       ' <a href="' . $recovery_url . '">' . $recovery_url . '</a>';
            $from = 'no-reply@' . str_replace('www.', '', $_SERVER['SERVER_NAME']);
            $mailer = new mailer;
            $result = $mailer->set_content_type($mailer::CONTENT_TYPE_TEXT_HTML)
                             ->send($email, $subject, $message, $from);
        }
        // recovery is success in any case for more protection
        $this->message->add('recovery', 'success');
        return $result;
    }

    /**
     * Changing the password by recovery key
     *
     * @param string $new_password
     * @param string $recovery_key
     */
    public function change_password_by_recovery_key($new_password, $recovery_key) {
        $query = 'UPDATE ' . $this->get_table() . PHP_EOL .
                 'SET password=:password,' . PHP_EOL .
                     'recovery_key=:recovery_key_empty' .  PHP_EOL .
                 'WHERE recovery_key=:recovery_key';
        $query_params = array(
            ':password'=>$this->generate_password_hash($new_password),
            ':recovery_key_empty'=>'',
            ':recovery_key'=>$recovery_key
        );
        if ($this->db_driver->query($query, $query_params)->rowCount()) {
            $this->message->add('changepass', 'success');
        } else {
            $this->message->add('changepass', 'error');
        }
    }

    /**
     * Checking whether the administrator remembered
     *
     * @return bool
     */
    public function is_remembered() {
        if ($remember_key = model_admin_access::get_remember_key()) {
            if ($admin = $this->get_row_by_field('remember_key', $remember_key)) {
                model_admin_access::set_access($admin['id'], $admin['login'], $admin['role']);
                return true;
            }
        }
        return false;
    }

    /**
     * Remember administrator
     *
     * @param int $id
     */
    protected function remember($id) {
        $remember_key = $this->generate_unique_key();
        $this->edit_row_by_id(array('remember_key'=>$remember_key), $id);
        model_admin_access::set_remember_key($remember_key);
    }

    /**
     * Checking the existence of super admins except the selected
     *
     * @param int $id administrator id
     * @return bool
     */
    protected function exists_other_superadmin($id) {
        $query = 'SELECT COUNT(*) FROM ' . $this->get_table() . PHP_EOL .
                 'WHERE role=:role' . PHP_EOL .
                 'AND '  . $this->get_table_pk() . '!=:id';
        $query_params = array(':role'=>model_admin_access::ROLE_SUPERADMIN, ':id'=>$id);
        return (bool) $this->db_driver->query($query, $query_params)->fetchColumn();
    }

    /**
     * Messages about the results of implementation of action
     *
     * @return array as
     * array(
     *     'action'=>array(
     *         'type'=>'message'[,...]
     *     ) [,...]
     * )
     */
    protected function messages() {
        return array(
            'login'=>array(
                'error'=>'Incorrect login or password',
            ),
            'add'=>array(
                'success'=>'The administrator has been added',
                'error'  =>'Failed to add administrator',
            ),
            'edit'=>array(
                'success'=>'The administrator has been edited',
                'error'  =>'Failed to edit administrator',
            ),
            'delete'=>array(
                'success'=>'The administrator has been deleted',
                'error'  =>'Failed to delete administrator',
            ),
            'recovery'=>array(
                'success'=>'Instructions are sent at your email',
            ),
            'changepass'=>array(
                'success'=>'Your password has been changed',
                'error'  =>'Failed to change password',
            ),
            'check_email'=>array(
                'error'=>'This email is already in used',
            ),
            'check_login'=>array(
                'error'=>'This login is already in used',
            ),
            'check_role'=>array(
                'error'=>'You can\'t remove privileges from an administrator',
            ),
        );
    }

    /**
     * Generation password hash
     *
     * @param string $password
     * @return string
     */
    public function generate_password_hash($password) {
        return md5(self::PASSWORD_SALT . $password);
    }

    /**
     * Generation unique key
     *
     * @return string
     */
    public function generate_unique_key() {
        return md5(uniqid(rand(), true));
    }
}