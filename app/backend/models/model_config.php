<?php
/**
 * @package Backend
 * @class   model_config
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    November, 2013
 * @version $Id
 *
 * Configuration model
 */

namespace app\backend\models;
use lib\pf\pf;
use lib\pf\message;
use lib\pf\config\config;

class model_config
{
   /** @var message $message */
    protected $message;

    /**
     * Constructor
     */
    public function __construct() {
        $this->message = new message(array(
            'edit'=>array(
                'success'=>'The configuration has been saved',
            )
        ));
    }

    /**
     * Getting message
     *
     * @return message
     */
    public function get_message() {
        return $this->message;
    }

    /**
     * Saving configuration
     *
     * @param array $config_arr
     */
    public function save_config(array $config_arr) {
       /** @var config $config */
        $config = pf::get_app()->get_config();
        foreach ($config_arr as $key_section=>$param) {
            foreach ($param as $key_param=>$value) {
                $config->set(array($key_section=>$key_param), $value);
            }
        }
        if ($config->write()) {
            $this->message->add('edit', 'success');
        }
    }
}