<?php
/**
 * @package Backend
 * @class   dom
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * DOM
 */

namespace app\backend\parser;
use \simple_html_dom;

require_once SIMPLE_HTML_DOM;

class dom
{
   /** @var simple_html_dom $dom */
    protected $dom;

    protected $content;

    public function __construct($content) {
        $this->content = $content;
        $this->dom = str_get_html($content);
    }

    public function dom() {
        return $this->dom;
    }

    public function get_content() {
        return $this->content;
    }
}