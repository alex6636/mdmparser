<?php
/**
 * @package Backend
 * @class   csv2xls
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * CSV to XLS
 */

namespace app\backend\parser;
use \PHPExcel_IOFactory;
use lib\pf\exceptions\exception_file;

require_once PHPEXCEL_DIR . 'PHPExcel/IOFactory.php';

class csv2xls
{
    public static function convert($csv_file) {
        if (!is_file($csv_file)) {
            throw new exception_file($csv_file, 1);
        }
        $input_file_type  = 'CSV';
        $output_file_type = 'Excel5';
        $xls_file = preg_replace("/\.csv$/ui", '.xls', $csv_file);
        $objReader = PHPExcel_IOFactory::createReader($input_file_type);
        $objWriter = PHPExcel_IOFactory::createWriter($objReader->load($csv_file), $output_file_type);
        $objWriter->save($xls_file);
        return $xls_file;
    }
}