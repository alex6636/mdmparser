<?php
/**
 * @package Backend
 * @class   parser_yellowpages_results
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Parser yellowpages.com
 */

namespace app\backend\parser;
use \simple_html_dom_node;
use lib\pf\arr;
use lib\pf\str;
use \Exception;

class parser_yellowpages_results extends parser_results
{
    /**
     * @param string $url
     * @throws Exception
     * @return array $results
     * array(
     *     array(
     *         'field_name'=>'field_value'[,...]
     *     )[,...]
     * )
     */
    public function get_results($url = NULL) {
        static $results = array(), $parse_again_attempts = 0, $i = 0;
        $stop = false;
        if ($url === NULL) {
            $url = $this->get_results_start_url();
        }
        if ($this->log) $this->log('Load: ' . $url);
        list($content, $curl_error, $curl_info) = $this->curl->exec($url);
        if ($curl_error !== false) {
            throw new Exception($curl_error[1], $curl_error[0]);
        }
        $dom = new dom($content);
        $nodes = (array) $dom->dom()->find($this->get_results_nodes_selector());
        if (!empty($nodes)) {
            $parse_again_attempts = 0;
            foreach ($nodes as $node) {
                foreach ($this->fields_callbacks as $field=>$callback) {
                    $results[$i][$field] = call_user_func_array(
                        $callback[0],
                        isset($callback[1]) ? array($node, $callback[1]) : array($node)
                    );
                }
                ++ $i;
                if ($this->limit === $i) {
                    $stop = true;
                    break;
                }
            }
            if (!$stop && ($next_search_url = $this->get_next_page_url($dom))) {
                // find on next page
                $this->get_results($next_search_url);
            }
        } else {
            if (++ $parse_again_attempts < $this->max_parse_again_attempts) {
                // try to search on this page again
                if ($this->log) $this->log('Try again: ' . $url);
                $this->get_results($url);
            } else {
                if ($this->curl instanceof curl_proxy) {
                    $parse_again_attempts = 0;
                    // change proxy
                    if ($this->log) $this->log('Change proxy: ' . $curl_info['primary_ip']);
                    $this->curl->remove_proxy($curl_info['primary_ip']);
                    $this->get_results($url);
                }
            }
        }
        return $results;
    }

    /**
     * @return string
     */
    protected function get_results_start_url() {
        list($keyword, $location) = arr::extract($_REQUEST, array('keyword'=>'', 'location'=>''), false);
        return 'http://www.yellowpages.com/search?search_terms=' . urlencode($keyword) .
               '&geo_location_terms=' . urlencode($location) . '&tracks=true';
    }

    /**
     * @return string
     */
    protected function get_results_nodes_selector() {
        return '#results .result';
    }

    /**
     * @return array array('field'=>array(callback[, 'selector']), [,...])
     */
    protected function get_fields_callbacks() {
        return array(
            'category'  =>array(array($this, 'get_field_category')),
            'name'      =>array(array($this, 'get_field_plaintext'), '.srp-business-name'),
            'city'      =>array(array($this, 'get_field_plaintext'), '.locality'),
            'state'     =>array(array($this, 'get_field_plaintext'), '.region'),
            'zip'       =>array(array($this, 'get_field_plaintext'), '.postal-code'),
            'address'   =>array(array($this, 'get_field_address')),
            'phone'     =>array(array($this, 'get_field_plaintext'), '.business-phone'),
            'categories'=>array(array($this, 'get_field_plaintext'), '.categories'),
            'website'   =>array(array($this, 'get_field_href'),      'a.track-visit-website'),
            //'service',  =>'',
        );
    }

    /**
     * @return string
     */
    protected function get_log_filename() {
        return 'yellowpages.txt';
    }

   /**
    * @param simple_html_dom_node $node result node
    * @return string
    */
    protected function get_field_category(simple_html_dom_node $node) {
        return htmlspecialchars_decode($node->getAttribute('data-category'));
    }

   /**
    * @param simple_html_dom_node $node result node
    * @return string
    */
    protected function get_field_address(simple_html_dom_node $node) {
        if ($field_node = $node->find('.listing-address', 0)) {
            return htmlspecialchars_decode(str_replace(' ,', ',', str::reduce_spaces(trim($field_node->plaintext))));
        }
        return '';
    }

   /**
    * @param dom $dom
    * @return mixed string or false
    */
    protected function get_next_page_url(dom $dom) {
        if ($link = $dom->dom()->find('link[rel=next]', 0)) {
            return str_replace('amp;', '', $link->href);
        }
        return false;
    }
}