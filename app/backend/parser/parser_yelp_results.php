<?php
/**
 * @package Backend
 * @class   parser_yelp_results
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Parser yelp.com
 */

namespace app\backend\parser;
use \simple_html_dom_node;
use lib\pf\arr;

class parser_yelp_results extends parser_results
{
    /**
     * @return string
     */
    protected function get_results_start_url() {
        list($keyword, $location) = arr::extract($_REQUEST, array('keyword'=>'', 'location'=>''), false);
        return 'http://www.yelp.com/search?find_desc=' . urlencode($keyword) .
               '&find_loc=' . urlencode($location) . '&ns=1';
    }

    /**
     * @return string
     */
    protected function get_results_nodes_selector() {
        return '.search-results .natural-search-result';
    }

    /**
     * @return array array('field'=>array(callback[, 'selector']), [,...])
     */
    protected function get_fields_callbacks() {
        return array(
            'category'    =>array(array($this, 'get_field_plaintext'), '.category-str-list'),
            'name'        =>array(array($this, 'get_field_plaintext'), '.biz-name'),
            'city'        =>array(array($this, 'get_field_city')),
            //'website'     =>array(array($this, 'get_field_href'),      'a.track-visit-website'),
            'neighborhood'=>array(array($this, 'get_field_plaintext'), '.neighborhood-str-list'),
            //'custom'      =>array(array($this, 'get_field_plaintext'), '.postal-code'),
        );
    }

    /**
     * @return string
     */
    protected function get_log_filename() {
        return 'yelp.txt';
    }

   /**
    * @param simple_html_dom_node $node result node
    * @return string
    */
    protected function get_field_city($node) {
        if ($field_node = $node->find('address', 0)) {
            $address = $field_node->outertext;
            preg_match("/<br>([^\,]+)\,/ui", $address, $matches);
            if (isset($matches[1])) {
                return $matches[1];
            }
        }
        return '';
    }
}