<?php
/**
 * @package Backend
 * @class   parser_results
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Parser abstract results
 */

namespace app\backend\parser;
use \simple_html_dom_node;
use lib\pf\pf;
use lib\pf\str;
use \Exception;

abstract class parser_results extends parser
{
   /** @var curl $curl */
    protected $curl;
    protected
        $fields_callbacks = array(),
        $limit = -1, // unlimited
        $max_parse_again_attempts = 3;

    /**
     * @param array $fields array('field', [,...])
     */
    public function __construct(array $fields) {
        $this->fields_callbacks = $this->filter_fields_callbacks($fields);
        $this->curl = curl_factory::make(
            (array) pf::get_app()->get_config()->get(array('parser'=>'curl'))
        );
    }

    /**
     * @param string $url
     * @throws Exception
     * @return array $results
     * array(
     *     array(
     *         'field_name'=>'field_value'[,...]
     *     )[,...]
     * )
     */
    public function get_results($url = NULL) {
        static $results = array(), $parse_again_attempts = 0, $i = 0;
        $stop = false;
        if ($url === NULL) {
            $url = $this->get_results_start_url();
        }
        if ($this->log) $this->log('Load: ' . $url);
        list($content, $curl_error, $curl_info) = $this->curl->exec($url);
        if ($curl_error !== false) {
            throw new Exception($curl_error[1], $curl_error[0]);
        }
        $dom = new dom($content);
        $nodes = (array) $dom->dom()->find($this->get_results_nodes_selector());
        if (!empty($nodes)) {
            $parse_again_attempts = 0;
            foreach ($nodes as $node) {
                foreach ($this->fields_callbacks as $field=>$callback) {
                    $results[$i][$field] = call_user_func_array(
                        $callback[0],
                        isset($callback[1]) ? array($node, $callback[1]) : array($node)
                    );
                }
                ++ $i;
                if ($this->limit === $i) {
                    $stop = true;
                    break;
                }
            }
            if (!$stop && ($next_search_url = $this->get_next_page_url($dom))) {
                // find on next page
                $this->get_results($next_search_url);
            }
        } else {
            if (++ $parse_again_attempts < $this->max_parse_again_attempts) {
                // try to search on this page again
                if ($this->log) $this->log('Try again: ' . $url);
                $this->get_results($url);
            }
        }
        return $results;
    }

    /**
     * @return int
     */
    public function get_limit() {
        return $this->limit;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function set_limit($limit) {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return string
     */
    abstract protected function get_results_start_url();

    /**
     * @return string
     */
    abstract protected function get_results_nodes_selector();

    /**
     * @return array array('field'=>array(callback[, 'selector']), [,...])
     */
    abstract protected function get_fields_callbacks();

    /**
     * @param array $fields
     * @return array $fields array('field', [,...])
     */
    protected function filter_fields_callbacks(array $fields) {
        $fields_callbacks = $this->get_fields_callbacks();
        foreach ($fields_callbacks as $field=>$callback) {
            if (!in_array($field, $fields)) {
                unset($fields_callbacks[$field]);
            }
        }
        return $fields_callbacks;
    }

   /**
    * @param simple_html_dom_node $node result node
    * @param string $selector
    * @return string
    */
    protected function get_field_plaintext(simple_html_dom_node $node, $selector) {
        if ($field_node = $node->find($selector, 0)) {
            return htmlspecialchars_decode(str::reduce_spaces(trim($field_node->plaintext)));
        }
        return '';
    }

   /**
    * @param simple_html_dom_node $node result node
    * @param string $selector
    * @return string
    */
    protected function get_field_href(simple_html_dom_node $node, $selector) {
        if ($field_node = $node->find($selector, 0)) {
            return $field_node->href;
        }
        return '';
    }

   /**
    * @param dom $dom
    * @return mixed string or false
    */
    protected function get_next_page_url(dom $dom) {
        if ($link = $dom->dom()->find('link[rel=next]', 0)) {
            return htmlspecialchars_decode($link->href);
        }
        return false;
    }
}