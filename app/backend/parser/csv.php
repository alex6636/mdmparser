<?php
/**
 * @package Backend
 * @class   csv
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * CSV
 */

namespace app\backend\parser;
use lib\pf\exceptions\exception_file;

class csv
{
    protected $delimeter = ',';

    public function set_delimeter($delimeter) {
        $this->delimeter = $delimeter;
        return $this;
    }

    public function get_file($filename, array $arr) {
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment;filename=' . $filename);
        return $this->array2csv_str($arr);
    }

    public function put_file($file_path, array $arr) {
        if (!$handle = fopen($file_path, 'w+')) {
            throw new exception_file($file_path, 2);
        }
        foreach ($arr as $a) {
            if (fputcsv($handle, (array) $a, $this->delimeter) === false) {
                throw new exception_file($file_path, 4);
            }
        }
        fclose($handle);
    }

    /**
     * $obj->array2csv_str(array(
     *    array('Name', 'Email'),
     *    array('Alex', 'createtruesite@gmail.com'),
     *    array('Dmity', 'tester@test.com'),
     * ));
     */
    public function array2csv_str(array $arr) {
        ob_start();
        $output = fopen('php://output', 'w');
        foreach ($arr as $a) {
            if (fputcsv($output, (array) $a, $this->delimeter) === false) {
                throw new exception_file('php://output', 4);
            }
        }
        fclose($output);
        return ob_get_clean();
    }
}