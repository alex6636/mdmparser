<?php
/**
 * @package Backend
 * @class   curl_factory
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Curl, Factory
 */

namespace app\backend\parser;

class curl_factory
{
    /**
     * Creation an curl-object
     *
     * @param array $config curl configuration
     * array(
     *    ['type'      =>'proxy',]
     *    ['proxy_list'=>'proxy_list.php',]
     * )
     * @return curl $curl
     */
    public static function make(array $config) {
        $config = $config + array(
            'type'      =>NULL,
            'proxy_list'=>NULL,
        );
        if ($config['type'] === 'proxy') {
            $curl = new curl_proxy;
            $curl->set_proxy_list($config['proxy_list']);
        } else {
            $curl = new curl;
        }
        return $curl;
    }
}