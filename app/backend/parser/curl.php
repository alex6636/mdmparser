<?php
/**
 * @package Backend
 * @class   curl
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Curl
 */

namespace app\backend\parser;
use lib\pf\exceptions\exception_runtime;

class curl
{
    const
        DEFAULT_CURL_ENCODING       = '',
        DEFAULT_CURL_USERAGENT      = 'spider',
        DEFAULT_CURL_CONNECTTIMEOUT = 9999,
        DEFAULT_CURL_TIMEOUT        = 1000,
        DEFAULT_CURL_MAXREDIRS      = 1000;

    /**
     * @throws exception_runtime if extension curl is not loaded
     */
    public function __construct() {
        if (!extension_loaded('curl')) {
            throw new exception_runtime('curl', 5);
        }
    }

    /**
     * @param string $url
     * @param array $options
     * @return array array(mixed $content, mixed $error, mixed $info)
     */
    public function exec($url, array $options = array()) {
        static $i = 1;
        $ch = curl_init($url);
        curl_setopt_array($ch, $options + array(
            CURLOPT_RETURNTRANSFER=>true,                              // return web page
            CURLOPT_HEADER        =>false,                             // do not return headers
            CURLOPT_FOLLOWLOCATION=>true,                              // follow redirects
            CURLOPT_ENCODING      =>self::DEFAULT_CURL_ENCODING,       // handle all encodings
            CURLOPT_USERAGENT     =>self::DEFAULT_CURL_USERAGENT,      // who am i
            CURLOPT_AUTOREFERER   =>true,                              // set referer on redirect
            CURLOPT_CONNECTTIMEOUT=>self::DEFAULT_CURL_CONNECTTIMEOUT, // timeout on connect
            CURLOPT_TIMEOUT       =>self::DEFAULT_CURL_TIMEOUT,        // timeout on response
            CURLOPT_MAXREDIRS     =>self::DEFAULT_CURL_MAXREDIRS,      // stop after redirects
        ));
        $content = curl_exec($ch);
        $info = false;
        $error = false;
        if ($err_no = curl_errno($ch)) {
            $error = array($err_no, curl_error($ch));
        } else {
            $info = curl_getinfo($ch);
        }
        curl_close($ch);
        return array($content, $error, $info);
    }
}