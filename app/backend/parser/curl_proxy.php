<?php
/**
 * @package Backend
 * @class   curl_proxy
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Curl with proxy
 */

namespace app\backend\parser;
use lib\pf\exceptions\exception_file;

class curl_proxy extends curl
{
    const DEFAULT_CURL_REFERER = 'http://www.google.com';

    protected $proxy_list = array();

    /**
     * @param string $url
     * @param array $options
     * @return array array(mixed $content, mixed $error, mixed $info)
     */
    public function exec($url, array $options = array()) {
        if (!empty($this->proxy_list)) {
            foreach ($this->proxy_list as $proxy) {
                list($proxy_port, $user_pwd) = explode('@', trim($proxy));
                list($proxy, $port) = explode(':', $proxy_port);
                $ch = curl_init();
                curl_setopt_array($ch, $options + array(
                    CURLOPT_URL           =>$url,
                    CURLOPT_HEADER        =>false,
                    CURLOPT_RETURNTRANSFER=>true,
                    CURLOPT_FOLLOWLOCATION=>true,
                    CURLOPT_PROXYPORT     =>$port,
                    CURLOPT_PROXYTYPE     =>'HTTP',
                    CURLOPT_PROXY         =>$proxy,
                    CURLOPT_PROXYUSERPWD  =>$user_pwd,
                    CURLOPT_CONNECTTIMEOUT=>self::DEFAULT_CURL_CONNECTTIMEOUT,
                    CURLOPT_TIMEOUT       =>self::DEFAULT_CURL_TIMEOUT,
                    CURLOPT_MAXREDIRS     =>self::DEFAULT_CURL_MAXREDIRS,
                    CURLOPT_REFERER       =>self::DEFAULT_CURL_REFERER,
                ));
                $content = curl_exec($ch);
                $info = false;
                $error = false;
                if ($err_no = curl_errno($ch)) {
                    $error = array($err_no, curl_error($ch));
                } else {
                    $info = curl_getinfo($ch);
                }
                curl_close($ch);
                if (($content !== false) && ($error === false) && ($info['http_code'] === 200)) {
                    // valid proxy
                    return array($content, $error, $info);
                }
            }
        }
        return array(false, array(-1, 'All proxy is invalid'), false);
    }

    public function get_proxy_list() {
        return $this->proxy_list;
    }

    public function set_proxy_list($file) {
        if (!is_file($file)) {
            throw new exception_file($file, 1);
        }
        $this->proxy_list = file($file);
        return $this;
    }

    public function remove_proxy($remove_proxy) {
        if (!empty($this->proxy_list)) {
            foreach ($this->proxy_list as $key=>$proxy) {
                if (strpos($proxy, $remove_proxy) !== false) {
                    unset($this->proxy_list[$key]);
                    return true;
                }
            }
        }
        return false;
    }
}