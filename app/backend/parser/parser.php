<?php
/**
 * @package Backend
 * @class   parser
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Parser
 */

namespace app\backend\parser;

abstract class parser
{
    protected $log = false;

    public function set_log($flag) {
        $this->log = $flag;
        if ($this->log) $this->log('Start');
        return $this;
    }

    /**
     * @return string
     */
    abstract protected function get_log_filename();

    protected function log($str, $date = true, $date_format = 'd.m.Y H:i:s') {
        return file_put_contents(
            PARSER_LOGS_DIR . '/' . $this->get_log_filename(),
            $date ? $str . " [" . date($date_format) . "]\n" : $str . "\n", FILE_APPEND
        );
    }

    protected function clear_log() {
        return file_put_contents(PARSER_LOGS_DIR . '/' . $this->get_log_filename(), '');
    }

    protected function arr2php(array $arr) {
        return file_put_contents(PARSER_LOGS_DIR . '/' . $this->get_log_filename(), '<?php' . PHP_EOL . 'return ' . var_export($arr, true) . ';');
    }
}