{{ param $page_title = 'Sorry' }}
{{ extends _page }}
{{ block page_content }}
<h1 class="pf title">{{ $page_title }}</h1>
<p>{{ $app->t('You do not have enough privileges to access this page', 'backend') }}</p>
{{ /block }}