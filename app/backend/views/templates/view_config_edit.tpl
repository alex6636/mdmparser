{{ param $page_title = 'Configuration' }}
{{ param $nav_active = 'config' }}
{{ extends _page_form }}
{{ block form_content }}
<div class="item">
    <label for="template_cache_lifetime" class="name"><span>{{ $app->t('Cache lifetime', 'backend') }}</span></label>
    <input id="template_cache_lifetime" type="text" name="template[cache_lifetime]" value="{{ $config->get(array('template'=>'cache_lifetime')) }}"/>
    <label for="template_cache_lifetime" class="desc"><span>{{ $app->t('In seconds', 'backend') }}</span></label>
</div>
<!--
<div class="item">
    <label for="lang_default" class="name"><span>{ { $app->t('Default language', 'backend') } }</span></label>
    { { param $allowed_langs = $config->get(array('i18n'=>'allowed_langs')) } }
    { { if !empty($allowed_langs) } }
    <select id="lang_default" name="i18n[default]">
    { { each $allowed_langs as $lang } }
    <option value="{ { $lang } }" { { if $config->get(array('i18n'=>'default')) == $lang } }selected{ { /if } }>{ { $lang } }</option>
    { { /each } }
    </select>
    { { /if } }
</div>
<div class="item">
    <label class="name"><span>{ { $app->t('Autodetection of language', 'backend') } }</span></label>
    <label>
        <input type="radio" name="i18n[auto_detect]" value="1" { { if $config->get(array('i18n'=>'auto_detect')) } }checked{ { /if } }/>
        { { $app->t('yes', 'backend') } }
    </label>
    <label style="margin-left:5px">
        <input type="radio" name="i18n[auto_detect]" value="0" { { if !$config->get(array('i18n'=>'auto_detect')) } }checked{ { /if } }/>
        { { $app->t('no', 'backend') } }
    </label>
</div>
-->
{{ /block }}
{{ block form_buttons }}
<div class="item">
    <button type="submit" class="button" form="form"><span>{{ $app->t('Save', 'backend') }}</span></button>
</div>
{{ /block }}