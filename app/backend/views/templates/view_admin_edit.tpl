{{ param $page_title = 'Editing administrator' }}
{{ extends _page_form }}
{{ block form_content }}
    {{ param $password_not_mandatory = true }}
    {{ extends _admin_form }}
{{ /block }}