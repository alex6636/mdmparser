{{ param $page_title = 'Sign in' }}
{{ extends _auth_page }}
{{ block form }}
<div class="form login">
    <form method="post" id="form">
        <div class="title">{{ $app->t('Sign in', 'backend') }}</div>
        <div class="item">
            <input{{ if !empty($error) }} class="err"{{ /if }} type="text" name="login" id="login" value="" placeholder="{{ $app->t('Email or Login', 'backend') }}"/>
        </div>
        <div class="item">
            <input{{ if !empty($error) }} class="err"{{ /if }} type="password" name="password" id="password" value="" placeholder="Password"/>
        </div>
        {{ if isset($captcha) }}
        <div class="item">
            {{ include _captcha }}
        </div>
        <div class="item">
            <input type="text" name="captcha" id="captcha" value="" placeholder="Code"/>
        </div>
        {{ /if }}
        {{ include _auth_message }}
        <div class="item">
            <input type="checkbox" name="remember" id="remember"/>
            <label for="remember">{{ $app->t('Remember Me', 'backend') }}</label>
            <a class="forgot" href="{{ $router->assemble_route('backend', array('controller'=>'auth', 'action'=>'recovery')) }}">{{ $app->t('Forgot password?', 'backend') }}</a>
        </div>
        <div class="item">
            <button form="form"><span>{{ $app->t('Login', 'backend') }}</span></button>
        </div>
    </form>
    <script>//document.getElementById('login').focus();</script>
</div>
{{ /block }}