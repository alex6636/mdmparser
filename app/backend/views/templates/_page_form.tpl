{{ extends _page }}
{{ block page_content }}
<div class="form_box">
    <h1 class="pf title">{{ $app->t($page_title, 'backend') }}</h1>
    {{ include _message }}
    {{ block form_top }}{{ /block }}
    {{ block form_open }}
    <form action="" method="post" id="form">
    {{ /block }}
        <div class="form">
            {{ block form_content }}{{ /block }}
            {{ block form_buttons }}
            <div class="item">
                <button type="submit" class="button" form="form"><span>{{ $app->t('Save', 'backend') }}</span></button>
                <button type="button" onclick="window.location.href='{{ $controller->assemble_url(array('action'=>'index')) }}'" class="button"><span>{{ $app->t('Back to list', 'backend') }}</span></button>
            </div>
            {{ /block }}
        </div>
    </form>
    {{ block form_bottom }}{{ /block }}
</div>
{{ /block }}