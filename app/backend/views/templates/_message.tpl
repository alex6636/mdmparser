{{ if !empty($success) }}
<div class="msg_box">
    <ul>
    {{ if is_array($success) }}
        {{ each $success as $msg }}
        <li class="success">{{ $app->t($msg, 'backend') }}</li>
        {{ /each }}
    {{ else }}
        <li class="success">{{ $app->t($success, 'backend') }}</li>
    {{ /if }}
    </ul>
</div>
{{ elseif !empty($error) }}
<div class="msg_box">
    <ul>
    {{ if is_array($error) }}
        {{ each $error as $msg }}
        <li class="error">{{ $app->t($msg, 'backend') }}</li>
        {{ /each }}
    {{ else }}
        <li class="error">{{ $app->t($error, 'backend') }}</li>
    {{ /if }}
    </ul>
</div>
{{ /if }}
