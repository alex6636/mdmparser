{{ if !empty($success) }}
<div class="success">
    {{ if is_array($success) }}
        {{ each $success as $msg }}
        <p>{{ $app->t($msg, 'backend') }}</p>
        {{ /each }}
    {{ else }}
        <p>{{ $app->t($success, 'backend') }}</p>
    {{ /if }}
</div>
{{ elseif !empty($error) }}
<div class="errors">
    {{ if is_array($error) }}
        <p>{{ $app->t($error[0], 'backend') }}</p>
    {{ else }}
        <p>{{ $app->t($error, 'backend') }}</p>
    {{ /if }}
</div>
{{ /if }}