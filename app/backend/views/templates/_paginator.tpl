<div class="pf paginator">
    <div class="stat">{{ $app->t('Displaying', 'backend') }} {{ $links['from'] }}-{{$links['for']}} {{ $app->t('of', 'backend') }} {{ $links['all'] }}</div>
    {{ if isset($links['nums']) && (count($links['nums']) > 1) }}
    <div class="links">
        {{ if isset($links['prev']) }}
            <a class="prev" title="{{ $links['prev']['num'] }}" href="{{ $links['prev']['link'] }}">&lt;</a>
        {{ /if }}
        {{ each $links['nums'] as $ln }}
        {{ if isset($ln['link']) }}
        <a class="num" title="{{ $ln['num'] }}" href="{{ $ln['link'] }}">{{ $ln['num'] }}</a>
        {{ else }}
        <span class="curr" title="{{ $ln['num'] }}">{{ $ln['num'] }}</span>
        {{ /if }}
        {{ /each }}
        {{ if isset($links['next']) }}
        <a class="next" title="{{ $links['next']['num'] }}" href="{{ $links['next']['link'] }}">&gt;</a>
        {{ /if }}
    </div>
    {{ /if }}
</div>