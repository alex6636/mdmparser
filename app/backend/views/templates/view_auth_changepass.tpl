{{ param $page_title = 'Change password' }}
{{ extends _auth_page }}
{{ block form }}
<div class="form changepass">
    <form method="post" id="form">
        <div class="title">{{ $app->t('Change password', 'backend') }}</div>
        <div class="item">
            <input{{ if !empty($error) }} class="err"{{ /if }} type="password" name="password" id="password" value="" autocomplete="off" placeholder="{{ $app->t('New password', 'backend') }}"/>
        </div>
        <div class="item">
            <input{{ if !empty($error) }} class="err"{{ /if }} type="password" name="confirm_password" id="confirm_password" value="" autocomplete="off" placeholder="{{ $app->t('Confirm password', 'backend') }}"/>
        </div>
        {{ include _auth_message }}
        <div class="item">
            <button form="form"><span>{{ $app->t('Change', 'backend') }}</span></button>
            <a class="sign_in" href="{{ $router->assemble_route('backend', array('controller'=>'auth')) }}">{{ $app->t('Sign in', 'backend') }}</a>
        </div>
    </form>
    <script>//document.getElementById('password').focus();</script>
</div>
{{ /block }}