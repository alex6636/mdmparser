{{ param $page_title = 'Parser' }}
{{ param $nav_active = 'parser' }}
{{ extends _page }}
{{ block scripts }}
<script src="/backend/js/jquery-1.10.2.min.js"></script>
<script src="/backend/js/parser.js"></script>
<script>
$(function() {
    initParserForm();
});
</script>
{{ /block }}
{{ block page_content }}
<div id="parser">
    <form id="parser_form" method="post">
        <div class="form">
            <div class="column keywords">
                <div class="title">Keywords search</div>
                <div class="item">
                    <select id="website" name="website">
                        <option value="">Website for parsing</option>
                        <option value="{{ $router->assemble_route('backend', array('controller'=>'parser_yellowpages', 'action'=>'index')) }}" data-fields="yellowpages">Parsing yellowpages.com</option>
                        <option value="{{ $router->assemble_route('backend', array('controller'=>'parser_yelp', 'action'=>'index')) }}" data-fields="yelp">Parsing yelp.com</option>
                    </select>
                </div>
                <div class="item">
                    <input type="text" id="location" name="location" value="" placeholder="Zipcode or City/State"/>
                </div>
                <div class="item">
                    <input type="text" id="keyword" name="keyword" value="" placeholder="Keywords"/>
                </div>
                <div class="item">
                    <select id="export_format" name="export_format">
                        <option value="">Export to</option>
                        <option value="xls">Export to XLS</option>
                        <option value="csv">Export to CSV</option>
                    </select>
                </div>
            </div>
            <div class="column fields">
                <div class="title">Report fields</div>
                <div class="list">
                    <div class="item">
                        <label><input type="checkbox" id="toggle_fields"/><span>All (all options)</span></label>
                    </div>
                    <div class="list_website"></div>
                </div>
            </div>
            <div class="column results">
                <div class="title">Results</div>
                <div class="item">
                    <button class="button" type="submit"><span>Submit</span></button>
                    <img id="preloader" src="/backend/img/preloader.gif"/>
                </div>
            </div>
        </div>
    </form>
</div>
{{ /block }}