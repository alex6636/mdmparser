{{ param $page_title = 'Password recovery' }}
{{ extends _auth_page }}
{{ block form }}
<div class="form recovery">
    <form method="post" id="form">
        <div class="title">{{ $app->t('Password recovery', 'backend') }}</div>
        <div class="item">
            <input{{ if !empty($error) }} class="err"{{ /if }} type="text" name="email" id="email" value="" placeholder="{{ $app->t('Email', 'backend') }}"/>
        </div>
        {{ include _auth_message }}
        <div class="item">
            <button form="form"><span>{{ $app->t('Send', 'backend') }}</span></button>
            <a style="float:right" href="{{ $router->assemble_route('backend', array('controller'=>'auth')) }}">{{ $app->t('Sign in', 'backend') }}</a>
        </div>
    </form>
    <script>//document.getElementById('email').focus();</script>
</div>
{{ /block }}