<!DOCTYPE html>
<html>
    <head>
        <title>{{ if !empty($page_title) }}{{ $app->t($page_title, 'backend') }} | {{ /if }}{{ $app->t('Backend', 'backend') }}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="/backend/img/favicon.png"/>
        <link rel="stylesheet" href="/backend/css/login.css"/>
        {{ block styles }}{{ /block }}
        {{ block scripts }}{{ /block }}
    </head>
    <body class="{{ !empty($body_class) ? ' ' . $body_class : '' }}">
        <div id="simple_page">
            <div id="main">
                <div id="inner">
                    <div id="logo"><img src="/backend/img/logo.png"/></div>