{{ extends _page }}
{{ block page_content }}
<div class="list_box">
    {{ block list_top }}
    <h1 class="pf title">{{ $app->t($page_title, 'backend') }}</h1>
    {{ include _message }}
    {{ /block }}
    {{ if !empty($rows) }}
    <div class="list">
        <table class="list_tab">
            {{ block list_head }}{{ /block }}
            {{ param $i = 0 }}
            {{ each $rows as $row }}
                {{ block list_row }}{{ /block }}
                {{ param $i ++ }}
            {{ /each }}
        </table>
        {{ include _paginator }}
    </div>
    {{ else }}
        {{ block list_empty }}<div style="margin-bottom: 20px">{{ $app->t('There are no records in the list', 'backend') }}</div>{{ /block }}
    {{ /if }}
    {{ block list_bottom }}
    <div class="buttons">
        <button type="button" class="button" onclick="window.location.href='{{ $controller->assemble_url(array('action'=>'add')) }}'"><span>{{ $app->t('Add', 'backend') }}</span></button>
    </div>
    {{ /block }}
</div>
{{ /block }}