{{ param $app = \lib\pf\pf::get_app() }}
{{ param $config = $app->get_config() }}
{{ param $controller = $app->get_controller() }}
{{ param $request = $app->get_request() }}
{{ param $router = $app->get_router() }}
{{ param $i18n = $app->get_i18n() }}