{{ include _params }}
{{ extends _header }}
<div id="main">
    <div id="header">
        <div id="logo"><img alt="" src="/backend/img/logo.png"></div>
        {{ nofilter new \app\backend\helpers\helper_widget_topnav('topnav') }}
    </div>
    <div id="page">
        {{ block page_content }}{{ /block }}
    </div>
</div>
{{ extends _footer }}