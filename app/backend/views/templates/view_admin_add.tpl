{{ param $page_title = 'Adding administrator' }}
{{ param $nav_active = 'admin_add' }}
{{ extends _page_form }}
{{ block form_content }}
    {{ extends _admin_form }}
{{ /block }}