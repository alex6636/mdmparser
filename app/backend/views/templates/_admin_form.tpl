<div class="item">
    <label for="email" class="name"><span>Email</span></label>
    <input type="text" id="email" name="email" autocomplete="off" value="{{ if !empty($email) }}{{ $email }}{{ /if }}"/>
</div>
<div class="item">
    <label for="login" class="name"><span>{{ $app->t('Login', 'backend') }}</span></label>
    <input type="text" id="login" name="login" autocomplete="off" value="{{ if !empty($login) }}{{ $login }}{{ /if }}"/>
    <label for="login" class="desc"><span>{{ $app->t('Login length must be 3 to 20 characters', 'backend') }}</span></label>
</div>
<div class="item">
    <label for="password" class="name"><span>{{ $app->t('Password', 'backend') }}</span></label>
    <input type="password" id="password" name="password" autocomplete="off"/>
    <label for="password" class="desc"><span>{{ $app->t('Password length must be minimum 5 characters', 'backend') }}{{ if isset($password_not_mandatory) }}. {{ $app->t('Leave password blank if don\'t want to change', 'backend') }}{{ /if }}</span></label>
</div>
<div class="item">
    <label for="role" class="name">
        <span>{{ $app->t('superadmin', 'backend') }}</span>
        <input type="checkbox" id="role" name="role"{{ if !empty($role) }} checked="checked"{{ /if }}/>
    </label>
    <label for="role" class="desc"><span>{{ $app->t('Allows to edit the other administrators', 'backend') }}</span></label>
</div>