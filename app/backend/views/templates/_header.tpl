<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>{{ if !empty($page_title) }}{{ $app->t($page_title, 'backend') }} | {{ /if }}{{ $app->t('Backend', 'backend') }}</title>
        <link rel="icon" type="image/png" href="/backend/img/favicon.png"/>
        <link rel="stylesheet" href="/backend/css/backend.css"/>
        {{ block styles }}{{ /block }}
        {{ block scripts }}{{ /block }}
    </head>
    <body class="{{ !empty($body_class) ? ' ' . $body_class : '' }}">