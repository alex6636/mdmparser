{{ param $page_title = 'Administrators' }}
{{ param $nav_active = 'admin_list' }}
{{ extends _page_list }}
{{ block list_head }}
<tr class="head odd">
    <td width="50%">{{ $app->t('Login', 'backend') }}</td>
    <td width="50%">Email</td>
    <td>{{ $app->t('Privilege', 'backend') }}</td>
    <td>{{ $app->t('Last visit', 'backend') }}</td>
    <td class="pf actions">{{ $app->t('Actions', 'backend') }}</td>
</tr>
{{ /block }}
{{ block list_row }}
{{ param $row_class = (($i % 2) === 0) ? 'even' : 'odd' }}
<tr class="{{ $row_class }}">
    <td>{{ $row['login'] }}</td>
    <td>{{ $row['email'] }}</td>
    <td>{{ if $row['role'] }}<span class="pf">{{ $app->t('superadmin', 'backend') }}</span>{{ else }}-{{/if}}</td>
    <td nowrap>{{ if $row['last_visit'] }}{{ date('d.m.Y H:i', $row['last_visit']) }}{{ else }}-{{ /if }}</td>
    <td class="pf actions">
        <a href="{{ $controller->assemble_url(array('action'=>'edit', 'param1'=>$row['id'])) }}">{{ $app->t('Edit', 'backend') }}</a>
        <a href="{{ $controller->assemble_url(array('action'=>'delete', 'param1'=>$row['id'])) }}">{{ $app->t('Delete', 'backend') }}</a>
    </td>
</tr>
{{ /block }}