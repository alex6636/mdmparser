<?php
namespace app\controllers;
use lib\pf\controllers\controller_default;

class controller_index extends controller_default
{
    public function action_index() {
        $this->request->redirect(
            $this->router->assemble_route('backend')
        );
    }
}