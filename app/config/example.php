<?php
return array(
    'db'=>array(
        'pdo'=>array(
            'dsn'=>'mysql:dbname=pf_db;host=localhost;charset=utf8',
            'username'=>'root',
            'password'=>'',
        ),
    ),
    'template'=>array(
        'tpl_dir'=>PF_APP_DIR . '/views/templates',
        'cache_lifetime'=>0,
        'xss_filter'=>true,
        'drop_blank'=>false,
        'precompile'=>false,
        'short_tags'=>false,
        'ld'=>'{{',
        'rd'=>'}}',
        'tpl_ext'=>'.tpl',
    ),
    'pagination'=>array(
        'num_rows' =>20,
        'num_links'=>3,
    ),
    'i18n'=>array(
        'dirs'=>array(
            PF_APP_DIR . '/backend/i18n',
        ),
        'default'=>'en',
        'allowed_langs'=>array('en', 'ru'),
        'auto_detect'=>false,
    ),
);