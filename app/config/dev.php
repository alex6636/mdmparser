<?php
return array(
    'db'=>array(
        'pdo'=>array(
            'dsn'=>'mysql:dbname=parser;host=localhost;charset=utf8',
            'username'=>'root',
            'password'=>'',
        ),
    ),
    'template'=>array(
        'tpl_dir'=>PF_APP_DIR . '/views/templates',
        'cache_lifetime'=>0,
        'xss_filter'=>true,
        'drop_blank'=>false,
        'precompile'=>false,
        'short_tags'=>false,
        'ld'=>'{{',
        'rd'=>'}}',
        'tpl_ext'=>'.tpl',
    ),
    'pagination'=>array(
        'num_rows' =>20,
        'num_links'=>3,
    ),
    'parser'=>array(
        'curl'=>array(
            'type'=>'-proxy',
            //'proxy_list'=>PF_APP_DIR . '/backend/parser/proxy_list.txt',
            'proxy_list'=>PF_APP_DIR . '/backend/parser/test_proxy_list.txt',
        ),
        'yellowpages'=>array(
            'limit'=>-1,
            'log'=>true,
        ),
        'yelp'=>array(
            'limit'=>-1,
            'log'=>true,
        ),
    ),
);