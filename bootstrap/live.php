<?php
/**
 * Bootstrap
 */

/* PHP */
error_reporting(0);
ini_set('display_errors', 0);
ini_set('display_startup_errors', 'On');
ini_set('log_errors', 'Off');
ini_set('memory_limit','2048M');
ini_set('max_execution_time', 9999);
ini_set('xdebug.max_nesting_level', 300);
setlocale(LC_ALL, 'US');
date_default_timezone_set('America/Los_Angeles');
/* PF */
defined('PF_DEBUG')    || define('PF_DEBUG', false);  // PF debug
defined('PF_LOGS')     || define('PF_LOGS',  true); // PF logs
defined('PF_ENCODING') || define('PF_ENCODING', 'utf-8'); // PF encoding
define('PF_DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT'] . '/..');
define('PF_APP_DIR',       PF_DOCUMENT_ROOT . '/app');
define('PF_LIB_DIR',       PF_DOCUMENT_ROOT . '/lib');
define('PF_CONFIG_FILE_DEFAULT_EXT', '.php'); // by default (.php|.ini|.xml)
define('PF_TPL_FILE_EXT',            '.tpl');
define('PF_CACHE_FILE_EXT',          '.cache');
if (PF_DEBUG) {
    define('PF_LIB_TRACER_CODE_LINES',      10); // number of rows displayed trace code
    define('PF_LIB_TRACER_CODE_HL_STRING',  '#006600'); // highlight color lines
    define('PF_LIB_TRACER_CODE_HL_COMMENT', '#999999'); // highlight color comments
    define('PF_LIB_TRACER_CODE_HL_KEYWORD', '#0000bb'); // highlight color keywords
    define('PF_LIB_TRACER_CODE_HL_DEFAULT', '#000000'); // highlight color default
    define('PF_LIB_TRACER_CODE_HL_HTML',    '#000000'); // highlight color HTML
}
if (PF_LOGS) {
    define('PF_LOGS_DIR',             PF_DOCUMENT_ROOT . '/logs');
    define('PF_LOGS_EXCEPTIONS_PATH', PF_LOGS_DIR . '/exception.txt');
    define('PF_LOGS_ERRORS_PATH',     PF_LOGS_DIR . '/error.txt');
}
define('SIMPLE_HTML_DOM', PF_LIB_DIR . '/simplehtmldom_1_5/simple_html_dom.php');
define('PHPEXCEL_DIR', PF_LIB_DIR . '/PHPExcel/Classes/');
define('PARSER_FILES_DIR', PF_DOCUMENT_ROOT . '/files');
define('PARSER_FILES_YELLOWPAGES_DIR', PARSER_FILES_DIR . '/yellowpages');
define('PARSER_FILES_YELP_DIR', PARSER_FILES_DIR . '/yelp');
define('PARSER_LOGS_DIR', PF_DOCUMENT_ROOT . '/parser_logs');