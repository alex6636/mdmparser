<?php
/**
 * @package pFramework
 * @class   validator
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    June, 2014
 * @version $Id
 *
 * Validator
 */

namespace lib\pf\validators;
use lib\pf\exceptions\exception_argument;

class validator extends validator_static implements validator_standard
{
    protected $errors = array();

    /**
     * Checking data for validity
     *
     * @param mixed $data
     * @param string $error error text is placed in the $errors resulting from non condition
     * @param mixed $condition as
     * string : required/in/number/url/email/file/length/regex/captcha
     * string the name of the callback function or
     * array('class_name', 'method_name'), as array('str', 'is_email') or
     * array(object, 'method_name')
     * callback function receives the 2 parameters ($data, $params)
     * @param mixed $params as
     * array(5, 10) for condition 'length'
     * string pattern for condition 'regex'
     * string key for captcha
     * if NULL parameters will not be passed to the validation method
     * @throws exception_argument if $condition is invalid
     * @return bool
     */
    public function check($data, $error, $condition, $params = NULL) {
        if (is_string($condition)) {
            // built in class method
            $callback = array($this, $condition);
            if (!is_callable($callback)) {
                // believe that this is an external function
                $callback = $condition;
            }
        } elseif ($condition) {
            // function to be called in the context of a class (object) or closure
            $callback = $condition;
        }
        if (isset($callback) && is_callable($callback)) {
            // validation method
            $valid = ($params === NULL) ?
                     call_user_func($callback, $data) :
                     call_user_func_array($callback, array_merge(array($data), (array) $params));
            if ($valid) {
                return true;
            }
            $this->set_error($error);
            return false;
        }
        throw new exception_argument('Condition {' . var_export($condition, true) . '} is invalid');
    }

    /**
     * Getting errors
     *
     * @param string $separator separator for error if meed to get them as a string
     * @return mixed array if $separator is null, or string
     */
    public function get_errors($separator = NULL) {
        return ($separator === NULL) ? $this->errors : implode($separator, $this->errors);
    }

    /**
     * Setting error
     *
     * @param string $error
     */
    public function set_error($error) {
        $this->errors[] = $error;
    }

    /**
     * Reset errors
     */
    public function reset_errors() {
        $this->errors = array();
    }
}