<?php
/**
 * @package   pFramework
 * @interface standard
 * @author    Alex Sergey (createtruesite@gmail.com)
 * @date      August, 2013
 * @version   $Id
 *
 * Standard validator interface
 */

namespace lib\pf\validators;

interface validator_standard
{
    /**
     * Checking data for validity
     *
     * @param mixed $data
     * @param string $error
     * @param mixed $condition
     * @param mixed $params
     * @return bool
     */
    public function check($data, $error, $condition, $params = NULL);

    /**
     * Getting errors
     *
     * @param string $separator separator for error if meed to get them as a string
     * @return mixed array if $separator is null, or string
     */
    public function get_errors($separator = NULL);

    /**
     * Setting error
     *
     * @param string $error
     */
    public function set_error($error);
}