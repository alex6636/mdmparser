<?php
/**
 * @package pFramework
 * @class   validator_static
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * Validator with static methods
 */

namespace lib\pf\validators;
use lib\pf\exceptions\exception_argument;

class validator_static
{
    /**
     * Checking email
     *
     * @param mixed $data
     * @return bool
     */
    public static function email($data) {
        return filter_var($data, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Checking URL
     *
     * @param mixed $data
     * @return bool
     */
    public static function url($data) {
        return filter_var($data, FILTER_VALIDATE_URL);
    }

    /**
     * Checking for filling
     *
     * @param mixed $data
     * @return bool
     */
    public static function required($data) {
        return empty($data) ? false : true;
    }

    /**
     * Checking entry $data in $params
     *
     * @param mixed $data
     * @param mixed $haystack string or array
     * @return bool
     */
    public static function in($data, $haystack) {
        return is_array($haystack) ? in_array($data, $haystack) : (strpos($haystack, $data) !== false);
    }

    /**
     * Checking the length of the string to the specified range
     *
     * @param mixed $data
     * @param int $min
     * @param mixed $max int maximum or an empty string if the maximum is not considered
     * @throws exception_argument if $min > $max
     * @return bool
     */
    public static function length($data, $min = 1, $max = '') {
        if (!empty($max) && ($max < $min)) {
            throw new exception_argument('The lower limit must be less, than the upper limit');
        }
        return (bool) preg_match('/^.{' . $min . ',' . $max . '}$/ui', $data);
    }

    /**
     * Check for a match with the regular expression pattern
     *
     * @param mixed $data
     * @param string $pattern
     * @return bool
     */
    public static function regex($data, $pattern) {
        return (bool) preg_match($pattern, $data);
    }

    /**
     * Checking UIN
     *
     * @param mixed $data
     * @return bool
     */
    public static function uin($data) {
        return (bool) preg_match('/^\d{5,9}$/ui', $data);
    }

    /**
     * Checking skype
     *
     * @param mixed $data
     * @return bool
     */
    public static function skype($data) {
        return (bool) preg_match('/^[a-z]{1}\S{6,32}$/ui', $data);
    }

    /**
     * Checking captcha
     *
     * @param mixed $data
     * @param string $captcha_key captcha key in the $_SESSION array
     * @return bool
     */
    public static function captcha($data, $captcha_key = 'captcha') {
        return (array_key_exists($captcha_key, $_SESSION) && ($_SESSION[$captcha_key] == $data));
    }
}