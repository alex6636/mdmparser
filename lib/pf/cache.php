<?php
/**
 * @package pFramework
 * @class   cache
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * Cache
 */

namespace lib\pf;
use lib\pf\exceptions\exception_argument;

class cache
{
    protected
        $dir,
        $last_key,
        $cache_ext = PF_CACHE_FILE_EXT;

    /**
     * Constructor
     *
     * @param string $dir
     */
    public function __construct($dir) {
        $this->set_dir($dir);
    }

    /**
     * Setting cache directory
     *
     * @param string $dir
     * @return cache
     */
    public function set_dir($dir) {
        file::create_writable_dir($dir);
        $this->dir = str::append_last_slash($dir);
        return $this;
    }

    /**
     * Getting cache directory
     *
     * @param string
     */
    public function get_dir() {
        return $this->dir;
    }

    /**
     * Getting the extension of cache files
     *
     * @return string
     */
    public function get_cache_ext() {
        return $this->cache_ext;
    }

    /**
     * Setting the extension of cache files
     *
     * @param string $cache_ext
     * @return cache
     */
    public function set_cache_ext($cache_ext) {
        $this->cache_ext = $cache_ext;
        return $this;
    }

    /**
     * Checking the existence of the cache
     *
     * @param string $key
     * @param int $lifetime seconds
     * @return mixed path to cache file or false
     */
    public function is_cached($key, $lifetime = 3600) {
        $cache_file_path = $this->get_cache_file_path($key);
        if (file_exists($cache_file_path)) {
            $filemtime = filemtime($cache_file_path);
            if (time() < ($filemtime + $lifetime)) {
                return $cache_file_path;
            }
        }
        return false;
    }

    /**
     * Getting cache
     *
     * @param string $key
     * @param int $lifetime seconds
     * @return mixed cache contents or false
     * if the cache does not exist or has expired
     */
    public function get($key, $lifetime = 3600) {
        $this->last_key = $key;
        if ($cache_file_path = $this->is_cached($key, $lifetime)) {
            return file::get_contents($cache_file_path);
        }
        return false;
    }

    /**
     * Saving cache
     *
     * @param mixed $data
     * @param string $key
     * @throws exception_argument if the data is not a string
     * @return int number of bytes
     */
    public function set($data, $key = NULL) {
        if (!is_string($data)) {
            throw new exception_argument(var_export($data, true), 4);
        }
        if ($key === NULL) {
            $key = $this->last_key;
        }
        return file::put_contents($this->get_cache_file_path($key), $data);
    }

    /**
     * Getting path to the cache file
     *
     * @param string $key
     * @return string
     */
    protected function get_cache_file_path($key) {
        return $this->dir . md5($key) . $this->cache_ext;
    }

    /**
     * Clearing cache
     *
     * @param string $key
     * @return bool
     */
    public function clear($key = NULL) {
        if ($key === NULL) {
            $key = $this->last_key;
        }
        $cache_file_path = $this->get_cache_file_path($key);
        if (is_file($cache_file_path)) {
            return @unlink($cache_file_path);
        }
        return false;
    }
}