<?php
/**
 * @package pFramework
 * @class   parser_url
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    August, 2013
 * @version $Id
 *
 * URL parser
 */

namespace lib\pf;

class parser_url
{
    protected
        $url,
        $parsed_url = array(),
        $path,
        $parts      = array(),
        $query;

    /**
     * Constructor
     *
     * @param string $url
     */
    public function __construct($url) {
        $this->set_url($url)
             ->set_parsed_url()
             ->set_path()
             ->set_parts()
             ->set_query();
    }

    /**
     * Getting the value of the next part of URL after $part
     *
     * @param string $part URL part
     * @return mixed URL part value or false
     */
    public function get_next_part($part) {
        $parts = $this->parts;
        if (($search = array_search($part, $parts)) !== false) {
            return arr::get($parts, ++ $search, false);
        }
        return false;
    }

    /**
     * Getting the value of the previous part of URL after $part
     *
     * @param string $part URL part
     * @return mixed part value or false
     */
    public function get_prev_part($part) {
        $parts = $this->parts;
        if (($search = array_search($part, $parts)) !== false) {
            return arr::get($parts, -- $search, false);
        }
        return false;
    }

    /**
     * Getting parsed URL parameters
     *
     * @return array
     */
    public function get_parsed_url() {
        return $this->parsed_url;
    }

    /**
     * URL path
     *
     * @return string
     */
    public function get_path() {
        return $this->path;
    }

    /**
     * URL parts
     *
     * @return array
     */
    public function get_parts() {
        return $this->parts;
    }

    /**
     * URL query
     *
     * @return string
     */
    public function get_query() {
        return $this->query;
    }

    /**
     * Setting URL
     *
     * @param string $url
     * @return parser_url
     */
    protected function set_url($url) {
        $this->url = $url;
        return $this;
    }

    /**
     * Setting parsed URL
     *
     * @return parser_url
     */
    protected function set_parsed_url() {
        $this->parsed_url = parse_url($this->url);
        return $this;
    }

    /**
     * Setting URL path
     *
     * @return parser_url
     */
    protected function set_path() {
        if ($path = arr::get($this->parsed_url, 'path')) {
            $path = str::trim_slashes($path);
        }
        $this->path = $path;
        return $this;
    }

    /**
     * Setting URL parts
     *
     * @return parser_url
     */
    protected function set_parts() {
        if (!empty($this->path)) {
            $this->parts = explode('/', $this->path);
        }
        return $this;
    }

    /**
     * Setting URL query
     *
     * @return parser_url
     */
    protected function set_query() {
        $this->query = arr::get($this->parsed_url, 'query');
        return $this;
    }
}