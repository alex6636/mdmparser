<?php
/**
 * @package pFramework
 * @class   loader_base
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    August, 2013
 * @version $Id
 *
 * Base class loader
 */

namespace lib\pf\loaders;

abstract class loader_base
{
    protected static $classes = array(); // loaded classes

    /**
     * Register class autoloader
     */
    abstract public function register();

    /**
     * Unregister class autoloader
     */
    abstract public function unregister();

    /**
     * Class autoload
     *
     * @param string $class class name
     * @return bool result
     */
    abstract public function autoload($class);

    /**
     * Autoload class with debugging
     *
     * @param string $class class name
     */
    public function autoload_debug($class) {
        $this->autoload($class);
        $this->add_class($class);
    }

    /**
     * Getting loaded classes
     *
     * @return array
     */
    public function get_classes() {
        return static::$classes;
    }

    /**
     * Getting name of autoload method
     *
     * @return string
     */
    protected function get_autoload_method() {
        return PF_DEBUG ? 'autoload_debug' : 'autoload';
    }

    /**
     * Adding a class to loaded
     *
     * @param string $class class name
     */
    protected function add_class($class) {
        static::$classes[] = $class;
    }
}