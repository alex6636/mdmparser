<?php
/**
 * @package pFramework
 * @class   loader_namespace
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Namespace loader
 */

namespace lib\pf\loaders;

class loader_namespace extends loader_base
{
    protected $use_include_path = false; // use include_path

    /**
     * Register class autoloader
     */
    public function register() {
        spl_autoload_register(array($this, $this->get_autoload_method()));
    }

    /**
     * Unregister class autoloader
     */
    public function unregister() {
        spl_autoload_unregister(array($this, $this->get_autoload_method()));
    }

    /**
     * Class autoload
     *
     * @param string $class class name
     * @return bool result
     */
    public function autoload($class) {
        // path to class file
        $class_path = str_replace('\\', '/', $class) . '.php';
        // looking on PF_DOCUMENT_ROOT
        if (file_exists(PF_DOCUMENT_ROOT . '/' . $class_path)) {
            $class_path = PF_DOCUMENT_ROOT . '/' . $class_path;
        } elseif ($this->use_include_path) {
            // looking on include_path
            $class_path = stream_resolve_include_path($class_path);
        } else {
            $class_path = false;
        }
        if ($class_path) {
            include $class_path;
            if (
                class_exists($class, false) ||
                interface_exists($class, false) ||
                (function_exists('trait_exists') && trait_exists($class, false))
            ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Using include_path
     *
     * @param bool $flag flag
     * @return loader_namespace
     */
    public function use_include_path($flag) {
        $this->use_include_path = $flag;
        return $this;
    }
}