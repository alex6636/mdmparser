<?php
/**
 * @package pFramework
 * @class   controller_default
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    November, 2013
 * @version $Id
 *
 * Controller to create a default view to caused action
 */

namespace lib\pf\controllers;
use lib\pf\app;
use lib\pf\event;
use lib\pf\template\template_factory;

abstract class controller_default extends controller
{
    protected $default_view; // default view

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        $this->default_view = template_factory::make($this->get_default_view_config());
        if (!event::has(app::EVENT_AFTER_CALL_CONTROLLER)) {
            // displayed view after calling controller
            event::on(app::EVENT_AFTER_CALL_CONTROLLER, array($this, 'display_default_view'));
        }
    }

    /**
     * Default view name
     * controller_test::action_index => view_test_index
     *
     * @return string
     */
    public function get_default_view_name() {
        return 'view_' . $this->request->get_route()->get_controller() . '_' . $this->request->get_route()->get_action();
    }

    /**
     * Default view configuration
     *
     * @return array
     */
    public function get_default_view_config() {
        return (array) $this->app->get_config()->get('template');
    }

    /**
     * Getting default view
     *
     * @return mixed template template object or NULL, if the view was not created
     */
    public function get_default_view() {
        return $this->default_view;
    }

    /**
     * Display default view
     *
     * @return controller_default
     */
    public function display_default_view() {
        $default_view_name = $this->get_default_view_name();
        if ($this->default_view->is_exists($default_view_name)) {
            $this->default_view->display($default_view_name);
        }
        return $this;
    }
}