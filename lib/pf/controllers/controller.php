<?php
/**
 * @package pFramework
 * @class   controller
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    November, 2013
 * @version $Id
 *
 * Controller
 */

namespace lib\pf\controllers;
use lib\pf\exceptions\exception_argument;
use lib\pf\exceptions\exception_runtime;
use lib\pf\pf;
use lib\pf\app;
use lib\pf\str;
use lib\pf\request;
use lib\pf\routers\router;

abstract class controller
{
   /** @var app $app application */
    protected $app;

   /** @var request $request request */
    protected $request;

   /** @var router $router router */
    protected $router;

    /**
     * Constructor
     */
    public function __construct() {
        $app = pf::get_app();
        if (!$app instanceof app) {
            throw new exception_argument('app', 7);
        }
        $this->set_app($app)
             ->set_request($app->get_request())
             ->set_router($app->get_router());
    }

    /**
     * Getting application
     *
     * @return app
     */
    public function get_app() {
        return $this->app;
    }

    /**
     * Setting application
     *
     * @param app $app application
     * @return controller
     */
    public function set_app(app $app) {
        $this->app = $app;
        return $this;
    }

    /**
     * Getting request
     *
     * @return request
     */
    public function get_request() {
        return $this->request;
    }

    /**
     * Setting request
     *
     * @param request $request request
     * @return controller
     */
    public function set_request(request $request) {
        $this->request = $request;
        return $this;
    }

    /**
     * Getting router
     *
     * @return router
     */
    public function get_router() {
        return $this->router;
    }

    /**
     * Setting router
     *
     * @param router $router router
     * @return controller
     */
    public function set_router(router $router) {
        $this->router = $router;
        return $this;
    }

    /**
     * Forwards to the current controller action
     *
     * @param string $action name of action
     * @param array $params parameters
     * @throws exception_runtime if not possible to call action
     * @return mixed result of calling or false
     */
    public function forward($action, array $params = NULL) {
        // cut out a prefix action_
        $action = str::ltrim_substr($action, 'action_');
        // change request action
        $this->request->get_route()->set_action($action);
        $method = 'action_' . $action;
        if (!is_callable(array($this, $method))) {
            throw new exception_runtime(get_called_class() . '::' . $method, 3);
        }
        return call_user_func_array(array($this, $method), (array) $params);
    }
}