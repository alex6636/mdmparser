<?php
/**
 * @package pFramework
 * @class   view_factory
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * View, Factory
 */

namespace lib\pf\view;

class view_factory
{
    /**
     * Creating the view object
     *
     * @param array $config
     * array(
     *     ['dir'            =>mixed string|array
     *     [,'filter'        =>mixed string|array,]
     *     [,'drop_blank'    =>bool (false by default)]
     * )
     * @return view $view
     */
    public static function make(array $config) {
        $config = $config + array(
            'dir'       =>NULL,
            'filter'    =>NULL,
            'drop_blank'=>false
        );
        $view = new view;
        if (!empty($config['dir'])) {
            if (is_array($config['dir'])) {
                $view->set_dirs($config['dir']);
            } else {
                $view->set_dir($config['dir']);
            }
        }
        // filters
        if (!empty($config['filter'])) {
            if (is_array($config['filter'])) {
                $view->set_filters($config['filter']);
            } else {
                $view->set_filter($config['filter']);
            }
        }
        $view->set_drop_blank($config['drop_blank']);
        return $view;
    }
}