<?php
/**
 * @package pFramework
 * @class   view
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * View
 */

namespace lib\pf\view;
use lib\pf\arr;
use lib\pf\str;
use lib\pf\file;
use lib\pf\exceptions\exception_file;
use lib\pf\exceptions\exception_runtime;

class view
{
    const
        // Processing method when adding a new parameter,
        // if the parameter with the same name already exists
        SET_PARAM_SKIP      = 0,
        SET_PARAM_OVERWRITE = 1;

    protected
        $params     = array(),
        $nofilters  = array(),
        $filters    = array(),
        $dirs       = array(),
        $drop_blank = false,
        $file_ext   = '.php';

    /**
     * Getting directories
     *
     * @return array
     */
    public function get_dirs() {
        return $this->dirs;
    }

    /**
     * Setting directory
     *
     * @param string $dir
     * @throws exception_file if directory is invalid
     * @return view
     */
    public function set_dir($dir) {
        $dir = str::append_last_slash($dir);
        if (!in_array($dir, $this->dirs)) {
            if (!is_dir($dir)) {
                throw new exception_file($dir, 13);
            }
            $this->dirs[] = $dir;
        }
        return $this;
    }

    /**
     * Setting directories
     *
     * @param array $dirs
     * @return view
     */
    public function set_dirs(array $dirs) {
        foreach ($dirs as $dir) {
            $this->set_dir($dir);
        }
        return $this;
    }

    /**
     * Getting filters
     *
     * @return array
     */
    public function get_filters() {
        return $this->filters;
    }

    /**
     * Setting filter
     *
     * @param mixed $filter callback function as
     * string 'htmlspecialchars' or array('\lib\pf\str', 'drop_blank') etc.
     * @throws exception_runtime if can't call the filter-function
     * @return view
     */
    public function set_filter($filter) {
        if (!in_array($filter, $this->filters)) {
            if (!is_callable($filter)) {
                throw new exception_runtime(var_export($filter, true), 3);
            }
            $this->filters[] = $filter;
        }
        return $this;
    }

    /**
     * Setting filters
     *
     * @param array $filters
     * @return view
     */
    public function set_filters(array $filters) {
        foreach ($filters as $filter) {
            $this->set_filter($filter);
        }
        return $this;
    }

    /**
     * Getting flag removing spaces, tabs, and hyphenation
     *
     * @return bool
     */
    public function get_drop_blank() {
        return $this->drop_blank;
    }

    /**
     * Setting flag removing spaces, tabs, and hyphenation
     *
     * @param bool $flag
     * @return view
     */
    public function set_drop_blank($flag) {
        $this->drop_blank = $flag;
        return $this;
    }

    /**
     * Getting extension view files
     *
     * @return string
     */
    public function get_file_ext() {
        return $this->file_ext;
    }

    /**
     * Setting extension view files
     *
     * @param string $file_ext
     * @return view
     */
    public function set_file_ext($file_ext) {
        $this->file_ext = $file_ext;
        return $this;
    }

    /**
     * Getting parameters
     *
     * @return array
     */
    public function get_params() {
        return $this->params;
    }

    /**
     * Getting parameter
     *
     * @param string $name
     * @return mixed
     */
    public function get_param($name) {
        return arr::get($this->params, $name);
    }

    /**
     * Getting parameter via magic method __get
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name) {
        return $this->get_param($name);
    }

    /**
     * Setting parameter
     *
     * @param string $name
     * @param mixed $value
     * @param bool $nofilter
     * @param int $type
     * @return view
     */
    public function set_param($name, $value, $nofilter = false, $type = self::SET_PARAM_OVERWRITE) {
        if (!$exists_param = array_key_exists($name, $this->params)) {
            $this->params[$name] = $value;
            if ($nofilter) {
                $this->nofilters[] = $name;
            }
        } else {
            if ($type === self::SET_PARAM_OVERWRITE) {
                $this->params[$name] = $value;
                if ($nofilter) {
                    if (!in_array($name, $this->nofilters)) {
                        $this->nofilters[] = $name;
                    }
                } else {
                    if (($nofilter_key = array_search($name, $this->nofilters)) !== false) {
                        unset($this->nofilters[$nofilter_key]);
                    }
                }
            }
        }
        return $this;
    }

    /**
     * Setting parameter via magic method __set
     *
     * $view->param_name = 'value';
     *
     * @param string $name
     * @param mixed $value
     * @return view
     */
    public function __set($name, $value) {
        $this->set_param($name, $value);
        return $this;
    }

    /**
     * Extracting parameters from array
     *
     * @param array $params
     * array('name1'=>'value1', 'name2'=>'value2'[,...])
     * @param bool $nofilter
     * @param int $type
     * @return view
     */
    public function extract(array $params = NULL, $nofilter = false, $type = self::SET_PARAM_OVERWRITE) {
        if ($params !== NULL) {
            foreach ($params as $name=>$value) {
                $this->set_param($name, $value, $nofilter, $type);
            }
        }
        return $this;
    }

    /**
     * Check the existence of the parameter via magic method __isset
     *
     * isset($view->foo);
     *
     * @param string $name
     * @return bool
     */
    public function __isset($name) {
        return array_key_exists($name, $this->params);
    }

    /**
     * Delete parameter via magic method __unset
     *
     * unset($view->foo);
     *
     * @param string $name
     * @return view
     */
    public function __unset($name) {
        if (isset($this->params)) {
            unset($this->params[$name]);
        }
        return $this;
    }

    /**
     * Display view
     *
     * @param string $file
     * @throws exception_file if view file is not exists
     */
    public function display($file) {
        if (!$file = $this->is_exists($file)) {
            throw new exception_file($file, 1);
        }
        if (!empty($this->params)) {
            $params = $this->filter_params($this->params);
            extract($params);
        }
        ob_start();
        include $file;
        $result = ob_get_clean();
        if ($this->drop_blank) {
            $result = str::reduce_spaces(str::drop_blank($result));
        }
        echo $result;
    }

    /**
     * View content in a variable
     *
     * @param string $file
     * @return string
     */
    public function fetch($file) {
        ob_start();
        $this->display($file);
        return ob_get_clean();
    }

    /**
     * Checking the existence of the file type
     *
     * @param string $file path to the view file (extension is not required)
     * @return mixed full path to the view file false
     */
    public function is_exists($file) {
        $file = str::append_extension($file, $this->file_ext);
        if ($path = file::full_path($file)) {
            return $path;
        }
        if (!empty($this->dirs)) {
            foreach ($this->dirs as $dir) {
                if ($path = file::full_path($dir . $file)) {
                    return $path;
                }
            }
        }
        return false;
    }

    /**
     * Filtering parameters
     *
     * @param array $params
     * @return array $params
     */
     public function filter_params($params) {
        if (!empty($this->filters)) {
            $filtered = array();
            foreach ($params as $name=>$value) {
                // check whether it is possible to filter parameter
                if (!in_array($name, $this->nofilters)) {
                    $filtered[$name] = $value;
                }
            }
            if (!empty($filtered)) {
                $filtered = (count($this->filters) === 1) ?
                            arr::map(current($this->filters), $filtered) :
                            arr::map_some_callbacks($this->filters, $filtered);
                $params = $filtered + $params;
            }
        }
        return $params;
    }
}