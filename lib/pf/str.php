<?php
/**
 * @package pFramework
 * @class   str
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * String
 */

namespace lib\pf;

class str
{
    /**
     * Removing tabs and line breaks
     *
     * @param string $str
     * @return string
     */
    public static function drop_blank($str) {
        return preg_replace("/[\t\r\n]/u", '', $str);
    }

    /**
     * Remove duplicate spaces
     *
     * @param string $str
     * @return string
     */
    public static function reduce_spaces($str) {
        return preg_replace('/[\s]+/u', ' ', $str);
    }

    /**
     * Insert the first slash
     *
     * @param string $str
     * @param string $slash
     * @return string
     */
    public static function append_first_slash($str, $slash = '/') {
        return $slash . static::ltrim_slashes($str);
    }

    /**
     * Insert the last slash
     *
     * @param string $str
     * @param string $slash
     * @return string
     */
    public static function append_last_slash($str, $slash = '/') {
        return static::rtrim_slashes($str) . $slash;
    }

    /**
     * Reducing repetitive slashes
     *
     * @param string $str
     * @param string $slash
     * @return string
     */
    public static function reduce_slashes($str, $slash = '/') {
        return preg_replace('/(?<!:)[\/\\\]+/u', $slash, $str);
    }

    /**
     * Removing direct and backslashes in the beginning of the line
     *
     * @param string $str
     * @return string
     */
    public static function ltrim_slashes($str) {
        return ltrim($str, '/\\');
    }

    /**
     * Removing direct and backslashes in the end of the line
     *
     * @param string $str
     * @return string
     */
    public static function rtrim_slashes($str) {
        return rtrim($str, '/\\');
    }

    /**
     * Removing direct and backslashes from the beginning and end of the line
     *
     * @param string $str
     * @return string
     */
    public static function trim_slashes($str) {
        return static::ltrim_slashes(static::rtrim_slashes($str));
    }

    /**
     * Deleting a substring in a string beginning
     *
     * @param string $str
     * @param string $substr
     * @return string
     */
    public static function ltrim_substr($str, $substr) {
        return preg_replace('/^' . preg_quote($substr) . '/ui', '', $str);
    }

    /**
     * Removing direct and backslashes at end of line
     *
     * @param string $str
     * @param string $substr
     * @return string
     */
    public static function rtrim_substr($str, $substr) {
        return preg_replace('/' . preg_quote($substr) . '$/ui', '', $str);
    }

    /**
     * Removing substring at the beginning and end of the line
     *
     * @param string $str
     * @param string $substr
     * @return string
     */
    public static function trim_substr($str, $substr) {
        return static::ltrim_substr(static::rtrim_substr($str, $substr), $substr);
    }

    /**
     * Split a string into words by a space
     *
     * @param string $str
     * @return array
     */
    public static function split_words($str) {
        return preg_split('/[\s]+/u', trim($str));
    }

    /**
     * Getting extension
     *
     * str::extension('filename.txt') => '.txt'
     *
     * @param string $str
     * @return string
     */
    public static function extension($str) {
        return strtolower((string) strrchr($str, '.'));
    }

    /**
     * Inserting extension
     *
     * @param string $str
     * @param string $extension
     * @return string
     */
    public static function append_extension($str, $extension) {
        return static::remove_extension($str, $extension) . $extension;
    }

    /**
     * Deleting extension
     *
     * str::remove_extension('filename.txt') => 'filename'
     *
     * @param string $str
     * @param string $extension (not necessarily)
     * @return string
     */
    public static function remove_extension($str, $extension = NULL) {
        return preg_replace('/' . preg_quote($extension ?: static::extension($str)) . '$/ui', '', $str);
    }

    /**
     * Trim lines for a given number of words
     *
     * @param string $str
     * @param int $limit
     * @param string $delim
     * @param string $inf
     * @return string $str
     */
    public static function limit_words($str, $limit = 10, $delim = ' ', $inf = '...') {
        $arr_words = static::split_words($str);
        $count_words = count($arr_words);
        if ($count_words > $limit) {
            $str = implode($delim, array_slice($arr_words, 0, $limit)) . $inf;
        }
        return $str;
    }

    /**
     * Trim lines for a given number of characters
     *
     * @param string $str
     * @param int $limit
     * @param string $inf
     * @return string $str
     */
    public static function limit_chars($str, $limit = 10, $inf = '...') {
        return (strlen($str) > $limit) ? substr($str, 0, $limit) . $inf : $str;
    }

    /**
     * Replacement basename of the string
     *
     * @param string $str
     * @param string $replacement
     * @return string
     */
    public static function replace_basename($str, $replacement) {
        return substr_replace($str, $replacement, strrpos($str, basename($str)));
    }

    /**
     * Getting SEO URL
     *
     * @param string $str
     * @return string $seourl
     */
    public static function get_seourl($str) {
        $seourl = strtolower($str);
        $seourl = preg_replace('/\s+/u', '-', $seourl);
        $seourl = preg_replace('/[^\w-]+/u', '', $seourl);
        return $seourl;
    }
}