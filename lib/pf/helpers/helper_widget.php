<?php
/**
 * @package pFramework
 * @class   helper_widget
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    October, 2013
 * @version $Id
 *
 * Helper-widget
 */

namespace lib\pf\helpers;
use lib\pf\pf;
use lib\pf\template\template;
use lib\pf\template\template_factory;
use lib\pf\str;

abstract class helper_widget extends helper
{
    protected
        $view,                  // view object
        $view_name,             // view name
        $view_params = array(); // view parameters

    /**
     * Constructor
     *
     * @param string $view_name view name
     * @param array $view_params view parameters
     */
    public function __construct($view_name, array $view_params = NULL) {
        parent::__construct();
        $this->set_view_name($view_name)
             ->set_view_params((array) $view_params);
        $this->view = template_factory::make($this->get_view_config());
    }

    /**
     * Display widget
     *
     * @return string
     */
    public function __toString() {
        // catching exceptions
        try {
            return $this->render();
        } catch (\exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * Render widget
     *
     * @return string
     */
    public function render() {
        return $this->view->extract($this->view_params)
                          ->fetch($this->view_name);
    }

    /**
     * Getting view object
     *
     * @return template
     */
    public function get_view() {
        return $this->view;
    }

    /**
     * Getting view name
     *
     * @return string
     */
    public function get_view_name() {
        return $this->view_name;
    }

    /**
     * Setting view name
     *
     * @param string $view_name view name
     * @return helper_widget
     */
    public function set_view_name($view_name) {
        $this->view_name = 'widget_' . str::ltrim_substr($view_name, 'widget_');
        return $this;
    }

    /**
     * Getting view parameters
     *
     * @return array
     */
    public function get_view_params() {
        return $this->view_params;
    }

    /**
     * Setting view parameters
     *
     * @param array $view_params view parameters
     * @return helper_widget
     */
    public function set_view_params(array $view_params) {
        $this->view_params = $view_params;
        return $this;
    }

    /**
     * Getting view configuration
     *
     * @return array
     */
    public function get_view_config() {
        return (array) pf::get_app()->get_config()->get('template');
    }
}