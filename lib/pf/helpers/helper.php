<?php
/**
 * @package pFramework
 * @class   helper
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * Helper
 */

namespace lib\pf\helpers;
use lib\pf\pf;
use lib\pf\controllers\controller;
use lib\pf\exceptions\exception_runtime;

abstract class helper
{
    protected $controller; // controller

    /**
     * Constructor
     *
     * @throws exception_runtime if controller is not initialized
     */
    public function __construct() {
        if (!$controller = pf::get_app()->get_controller()) {
            throw new exception_runtime('Application controller is not initialized');
        }
        $this->set_controller($controller);
    }

    /**
     * Getting controller
     *
     * @return controller
     */
    public function get_controller() {
        return $this->controller;
    }

    /**
     * Setting controller
     *
     * @param controller $controller controller
     * @return helper
     */
    public function set_controller(controller $controller) {
        $this->controller = $controller;
        return $this;
    }
}