<?php
/**
 * @package pFramework
 * @class   cookie
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    August, 2013
 * @version $Id
 *
 * Cookie
 */

namespace lib\pf;

class cookie
{
    /**
     * Setting cookie
     *
     * @param string $name
     * @param string $value
     * @param string $path
     * the path on the server in which the cookie will be available on
     * @param int $expire_days lifetime, days
     * @param string $server sever name
     * @param bool $secure
     * @return bool
     */
    public static function set(
                               $name,
                               $value       = '',
                               $path        = '/',
                               $expire_days = 30,
                               $server      = NULL,
                               $secure      = false
                              )
    {
        return setcookie(
            $name,
            $value,
            time() + 3600 * 24 * $expire_days,
            $path,
            $server ?: $_SERVER['SERVER_NAME'],
            $secure
        );
    }

    /**
     * Getting cookie
     *
     * @param string $name
     * @param string $default the default value for a non-existent cookie
     * @return mixed cookie value or default value
     */
    public static function get($name, $default = NULL) {
        return arr::get($_COOKIE, $name, $default);
    }

    /**
     * Deleting cookie
     *
     * @param string $name
     * @param string $path
     * the path on the server in which the cookie will be available on
     * @param string $server
     * @param bool $secure
     * @return bool
     */
    public static function delete($name, $path = '/', $server = NULL, $secure = false) {
        if (array_key_exists($name, $_COOKIE)) {
            return setcookie(
                $name,
                '',
                time() - 1,
                $path,
                $server ?: $_SERVER['SERVER_NAME'],
                $secure
            );
        }
        return false;
    }
}