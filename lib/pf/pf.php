<?php
/**
 * @package pFramework
 * @class   pf
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * PF (pFramework)
 */

namespace lib\pf;
use \Exception;
use lib\pf\exceptions\error_shutdown_handler;
use lib\pf\loaders\loader_base;
use lib\pf\loaders\loader_namespace;

class pf
{
    const
        VERSION         = '3.0.0',
        PHP_VERSION_MIN = '5.3.0';

    protected static
        /** @var loader_namespace $loader */
        $loader,
        /** @var app $loader */
        $app;

    /**
     * Connecting the boot file
     *
     * @param string $file_path
     * @throws Exception if below the minimum PHP-version
     * @throws Exception if cannot find bootstap-file
     */
    public static function bootstrap($file_path) {
        if (!static::check_php()) {
            throw new Exception('PHP Version must be newer than ' . self::PHP_VERSION_MIN .
                                ' [' . (static::get_title()) . ']');
        }
        if (!is_file($file_path)) {
            throw new Exception('Boot file ' . $file_path . ' not found' .
                                ' [' . (static::get_title()) . ']');
        }
        require $file_path;
        pf::register_loader();
        pf::register_error_handlers();
    }

    /**
     * Checking the PHP-version
     *
     * @return bool
     */
    public static function check_php() {
        return version_compare(PHP_VERSION, self::PHP_VERSION_MIN, '>=') ? true : false;
    }

    /**
     * Register loader
     */
    public static function register_loader() {
        require_once __DIR__ . '/loaders/loader_base.php';
        require_once __DIR__ . '/loaders/loader_namespace.php';
        $loader = new loader_namespace;
        $loader->register();
        static::$loader = $loader;
    }

    /**
     * Unable to register loader
     */
    public static function unregister_loader() {
        /** @var loader_namespace $loader */
        $loader = static::$loader;
        if ($loader !== NULL) {
            $loader->unregister();
        }
    }

    /**
     * Getting loader
     *
     * @return loader_base the loader object or NULL,
     * if loader object was not initialized
     */
    public static function get_loader() {
        return static::$loader;
    }

    /**
     * Register error handlers
     */
    public static function register_error_handlers() {
        set_exception_handler(array('\lib\pf\exceptions\exception_handler', 'handler'));
        set_error_handler(array('\lib\pf\exceptions\error_handler', 'handler'), error_reporting());
        register_shutdown_function(array('\lib\pf\exceptions\error_shutdown_handler', 'shutdown_handler'));
        error_shutdown_handler::handle(true);
    }

    /**
     * Restore error handlers
     */
    public static function restore_error_handlers() {
        restore_exception_handler();
        restore_error_handler();
        error_shutdown_handler::handle(false);
    }

    /**
     * Creating an application
     *
     * @param string $config_file_path
     * @return app $app
     */
    public static function create_app($config_file_path) {
        $app = new app($config_file_path);
        static::set_app($app);
        return $app;
    }

    /**
     * Getting application
     *
     * @return app or NULL if application was not initialized
     */
    public static function get_app() {
        return static::$app;
    }

    /**
     * Setting application
     *
     * @param app $app
     */
    public static function set_app(app $app) {
        static::$app = $app;
    }

    /**
     * Getting version of framework
     *
     * @return string
     */
    public static function get_version() {
        return self::VERSION;
    }

    /**
     * Title of the Framework
     *
     * @return string
     */
    public static function get_title() {
        return 'pFramework ' . static::get_version();
    }

    /**
     * Powered text
     *
     * @return string
     */
    public static function powered() {
        return '<a class="powered" href="#">Powered by ' . static::get_title() . '</a>';
    }
}