<?php
/**
 * @package pFramework
 * @class   table_row
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    December, 2013
 * @version $Id
 *
 * Working with database table row
 */

namespace lib\pf\db;
use lib\pf\db\drivers\driver_pdo;
use lib\pf\arr;

abstract class table_row
{
   /** @var driver_pdo $db_driver PDO driver */
    protected $db_driver;

    /**
     * Constructor
     *
     * @param driver_pdo $db_driver
     */
    public function __construct(driver_pdo $db_driver) {
        $this->set_db_driver($db_driver);
        // check schema of the table
        if ($query = $this->get_table_schema()) {
            // create table
            $db_driver->query($query);
        }
    }

    /**
     * Table name
     *
     * @return string
     */
    public abstract function get_table();

    /**
     * Primary key name
     *
     * @return string
     */
    public function get_table_pk() {
        return 'id';
    }

    /**
     * Getting table schema
     *
     * @return mixed string SQL-quesry with table schema
     * or NULL, if the table has already been created
     */
    public function get_table_schema() {
        return NULL;
    }

    /**
     * Getting driver
     *
     * @return driver_pdo
     */
    public function get_db_driver() {
        return $this->db_driver;
    }

    /**
     * Setting driver
     *
     * @param driver_pdo $db_driver
     * @return driver_pdo
     */
    public function set_db_driver(driver_pdo $db_driver) {
        $this->db_driver = $db_driver;
        return $this;
    }

    /**
     * Getting all the rows in the table
     *
     * @param string $fields SQL-fields
     * @return array multidimensional array of data rows in the table
     * with subarrays in PDO::ATTR_DEFAULT_FETCH_MODE format
     */
    public function get_all($fields = '*') {
        return $this->get_all_order_by_field($this->get_table_pk(), $fields);
    }

    /**
     * Getting all the rows in the table by field
     *
     * @param string $field field name
     * @param mixed $value field value
     * @param string $fields SQL-fields
     * @return array multidimensional array of data rows in the table
     * with subarrays in PDO::ATTR_DEFAULT_FETCH_MODE format
     */
    public function get_all_by_field($field, $value, $fields = '*') {
        $query = 'SELECT '   . $fields . ' FROM ' . $this->get_table() . PHP_EOL .
                 'WHERE '  . $field . '=?' . PHP_EOL .
                 'ORDER BY ' . $this->get_table_pk();
        return $this->db_driver->query($query, $value)->fetchAll();
    }

    /**
     * Getting all the rows in the table order by field
     *
     * @param mixed $field name of the field on which to sort the records
     * or an array of fields as array('name DESC', 'id')
     * @param string $fields received fields
     * @return array multidimensional array of data rows in the table
     * with subarrays in PDO::ATTR_DEFAULT_FETCH_MODE format
     */
    public function get_all_order_by_field($field, $fields = '*') {
        $query = 'SELECT '   . $fields . ' FROM ' . $this->get_table() . PHP_EOL .
                 'ORDER BY ' . (is_array($field) ? implode(', ', $field) : $field);
        return $this->db_driver->query($query)->fetchAll();
    }

    /**
     * Getting table row by primary key value
     *
     * @param mixed $id primary key value
     * @param string $fields SQL-fields
     * @return mixed data array of table row
     * in PDO::ATTR_DEFAULT_FETCH_MODE format or false
     */
    public function get_row_by_id($id, $fields = '*') {
        return $this->get_row_by_field($this->get_table_pk(), $id, $fields);
    }

    /**
     * Getting table row by field value
     *
     * @param string $field field name
     * @param mixed $value field value
     * @param string $fields SQL-fields
     * @return mixed data array of table row
     * in PDO::ATTR_DEFAULT_FETCH_MODE format or false
     */
    public function get_row_by_field($field, $value, $fields = '*') {
        $query = 'SELECT ' . $fields . ' FROM ' . $this->get_table() . PHP_EOL .
                 'WHERE '  . $field . '=?' . PHP_EOL .
                 'ORDER BY ' . $this->get_table_pk();
        return $this->db_driver->query($query, $value)->fetch();
    }

    /**
     * Getting table row by primary key with ignoring rowset id's
     *
     * @param mixed $value primary key value
     * @param mixed $ignore_id int or array ignoring rowset id's
     * @param string $fields SQL-fields
     * @return mixed data array of table row
     * in PDO::ATTR_DEFAULT_FETCH_MODE format or false
     */
    public function get_row_by_id_ignore_id($value, $ignore_id, $fields = '*') {
        return $this->get_row_by_field_ignore_id($this->get_table_pk(), $value, $ignore_id, $fields);
    }

    /**
     * Getting table row by field value with ignoring rowset id's
     *
     * @param string $field field name
     * @param mixed $value field value
     * @param mixed $ignore_id int or array ignoring rowset id's
     * @param string $fields SQL-fields
     * @return mixed data array of table row
     * in PDO::ATTR_DEFAULT_FETCH_MODE format or false
     */
    public function get_row_by_field_ignore_id($field, $value, $ignore_id, $fields = '*') {
        $ignore_id = (array) $ignore_id;
        $query = 'SELECT ' . $fields . ' FROM ' . $this->get_table() . PHP_EOL .
                 'WHERE '  . $field . '=?' . PHP_EOL .
                 'AND ' . $this->get_table_pk() . ' NOT IN'  . PHP_EOL .
                 '(' . implode(',', array_fill(0, count($ignore_id), '?')) . ')' . PHP_EOL .
                 'ORDER BY ' . $this->get_table_pk();
        return $this->db_driver->query($query, array_merge((array) $value, $ignore_id))->fetch();
    }

    /**
     * Getting a column primary key values ​​of the table
     *
     * @return array primary key column of the table
     */
    public function get_column_id() {
        return $this->get_column($this->get_table_pk());
    }

    /**
     * Getting a column values ​​of the table
     *
     * @param string $field field name
     * @return array column of the table
     */
    public function get_column($field) {
        $query = 'SELECT ' . $field . ' FROM ' . $this->get_table() . PHP_EOL .
                 'ORDER BY ' . $this->get_table_pk();
        return arr::column($this->db_driver->query($query)->fetchAll() , $field);
    }

    /**
     * Getting the cell of table row by primary key value
     *
     * @param string $field field (cell) name
     * @param mixed $id primary key value
     * @return mixed value of the cell or false
     */
    public function get_cell_by_id($field, $id) {
        return $this->get_cell_by_field($field, $this->get_table_pk(), $id);
    }

    /**
     * Getting the cell of table row by field value
     *
     * @param string $cell_field field (cell) name
     * @param string $field field name
     * @param mixed $value field value
     * @return mixed value of the cell or false
     */
    public function get_cell_by_field($cell_field, $field, $value) {
        $query = 'SELECT ' . $cell_field . ' FROM ' . $this->get_table() . PHP_EOL .
                 'WHERE '  . $field . '=?' . PHP_EOL .
                 'ORDER BY ' . $this->get_table_pk();
        return $this->db_driver->query($query, $value)->fetchColumn();
    }

    /**
     * Getting a set of values ​​of the field using SQL-functions: COUNT, MIN, MAX, SUM, AVG
     *
     * @param string $function SQL-function name
     * @param string $field field name
     * @return mixed result
     */
    public function get_function_by_field($function, $field) {
        $query = 'SELECT ' . strtoupper($function) . '(' . $field . ') FROM ' . $this->get_table();
        return $this->db_driver->query($query)->fetchColumn();
    }

    /**
     * Adding row
     *
     * @param array $arr associative array in the form of
     * array('field_name'=>'field_value'[,'field_name2'=>'field_value2',[...]])
     * @return string last inserted id
     */
    public function add_row(array $arr) {
        $query  = 'INSERT INTO ' . $this->get_table();
        // fields
        $query .= '(' . implode(',', array_keys($arr)) . ')' . PHP_EOL;
        // values
        $query .= 'VALUES (' . implode(',', array_fill(0, count($arr), '?')) . ')';
        $this->db_driver->query($query, array_values($arr));
        return $this->db_driver->connect()->lastInsertId();
    }

    /**
     * Editing row by primary key value
     *
     * @param array $arr associative array in the form of
     * array('field_name'=>'field_value'[,'field_name2'=>'field_value2',[...]])
     * @param mixed $id primary key value
     * @return int number of updated rows
    */
    public function edit_row_by_id(array $arr, $id) {
        return $this->edit_row_by_field($arr, $this->get_table_pk(), $id);
    }

    /**
     * Editing row by field value
     *
     * @param array $arr associative array in the form of
     * array('field_name'=>'field_value'[,'field_name2'=>'field_value2',[...]])
     * @param string $field field name
     * @param mixed $value field value
     * @return int number of updated rows
    */
    public function edit_row_by_field(array $arr, $field, $value) {
        $query = 'UPDATE ' . $this->get_table() . ' ';
        $query .= 'SET ' . implode(',' . PHP_EOL, array_map(
            function($field) {
                return $field . '=?';
            }, array_keys($arr)
        )) . PHP_EOL;
        $query .= 'WHERE ' . $field . '=?';
        return (int) $this->db_driver->query($query, array_merge(array_values($arr), (array) $value))->rowCount();
    }

    /**
     * Delete row by primary key
     *
     * @param mixed $id primary key value
     * @return int number of deleted rows
     */
    public function delete_row_by_id($id) {
        return $this->delete_row_by_field($this->get_table_pk(), $id);
    }

    /**
     * Delete row by field value
     *
     * @param string $field field name
     * @param mixed $value field value
     * @return int number of deleted rows
     */
    public function delete_row_by_field($field, $value) {
        $query = 'DELETE FROM ' . $this->get_table() . PHP_EOL .
                 'WHERE ' . $field . '=?';
        return (int) $this->db_driver->query($query, $value)->rowCount();
    }
}