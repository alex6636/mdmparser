<?php
/**
 * @package pFramework
 * @class   driver_pdo
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    June, 2014
 * @version $Id
 *
 * Driver to work with the PDO
 */

namespace lib\pf\db\drivers;
use lib\pf\exceptions\exception_runtime;
use lib\pf\profiler;
use \PDO;
use \PDOStatement;

class driver_pdo extends driver
{
    public static
        $err_mode   = PDO::ERRMODE_EXCEPTION, // error processing level
        $fetch_mode = PDO::FETCH_ASSOC;       // data type

   /** @var PDO $connection connection object */
    protected $connection = NULL;

    /**
     * Constructor
     *
     * @param array $config configuration parameters
     * @throws exception_runtime if PDO extension in not installed
     */
    public function __construct(array $config) {
        if (!extension_loaded('PDO')) {
            throw new exception_runtime('PDO', 5);
        }
        parent::__construct($config);
    }

    /**
     * Connection to the database
     *
     * @return PDO $this->connection
     */
    public function connect() {
        if ($this->connection !== NULL) {
            // already connected
            return $this->connection;
        }
        $config = $this->config + array(
            'dsn'           =>'',
            'username'      =>'',
            'password'      =>'',
            'driver_options'=>array()
        );
        // creating a new connection
        $this->connection = new PDO(
            $config['dsn'],
            $config['username'],
            $config['password'],
            $config['driver_options']
        );
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, static::$err_mode);
        $this->connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, static::$fetch_mode);
        return $this->connection;
    }

    /**
     * Disconnect from database
     */
    public function disconnect() {
        $this->connection = NULL;
    }

    /**
     * Query
     *
     * @param string $query query text
     * @param mixed $params parameters
     * @return PDOStatement $sth query result
     */
    public function query($query, $params = NULL) {
        if (PF_DEBUG) {
            // start profiling
            $benchmark = profiler::start('query', $this->interpolate_query($query, $params));
        }
        if (empty($params)) {
            $sth = $this->connect()->query($query);
        } else {
            $sth = $this->connect()->prepare($query);
            $sth->execute((array) $params);
        }
        if (isset($benchmark)) {
            // stop profiling
            profiler::stop($benchmark);
        }
        return $sth;
    }
}