<?php
/**
 * @package pFramework
 * @class   driver
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    June, 2014
 * @version $Id
 *
 * Driver to work with the database
 */

namespace lib\pf\db\drivers;

abstract class driver
{
    protected $config = array(); // configuration array

    /**
     * Constructor
     *
     * @param array $config configuration parameters
     */
    public function __construct(array $config) {
        $this->config = $config;
    }

    /**
     * Connect to the database
     *
     * @return object connection
     */
    abstract public function connect();

    /**
     * Disconnecting from the database
     */
    abstract public function disconnect();

    /**
     * Query
     *
     * @param string $query query text
     * @param mixed $params parameters
     * @return mixed query result
     */
    abstract public function query($query, $params = NULL);

    /**
     * Replaces placeholders for parameters
     *
     * @param string $query
     * @param mixed $params parameters
     * @return string
     */
    public function interpolate_query($query, $params = NULL) {
        if (empty($params)) {
            return $query;
        }
        $params = (array) $params;
        $keys = array();
        // build a regular expression for each parameter
        foreach ($params as $key=>$value) {
            $keys[] = is_string($key) ?
                      '/' . ((strpos($key, ':') === 0) ? '' : ':') . $key . '/' :
                      '/[?]/';
        }
        return preg_replace($keys, $params, $query, 1, $count);
    }
}