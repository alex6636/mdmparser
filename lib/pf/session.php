<?php
/**
 * @package pFramework
 * @class   session
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Session (Singleton)
 */

namespace lib\pf;

class session
{
    /** @var session $instance */
    private static $instance;

    /**
     * Getting session object
     *
     * @return session
     */
    public static function get_instance() {
        if (!static::$instance) {
            static::$instance = new static;
        }
        static::$instance->start();
        return static::$instance;
    }

    /**
     * Start session
     *
     * @return bool
     */
    public function start() {
        if (!$this->is_started()) {
            return session_start();
        }
        return true;
    }

    /**
     * Destroy session
     *
     * @return bool
     */
    public function destroy() {
        if ($this->is_started()) {
            return session_destroy();
        }
        return true;
    }

    /**
     * Checking session is started
     *
     * @return bool
     */
    public function is_started() {
        return (bool) session_id();
    }

    /**
     * Getting value of parameter
     *
     * @param string $name
     * @param string $default the default value for a non-existent element
     * @return mixed parameter value or NULL,
     * if parameter is not exists
     */
    public function get($name, $default = NULL) {
        return arr::get($_SESSION, $name, $default);
    }

    /**
     * Getting value of parameter by magic method __get
     *
     * session::get_instance()->foo = 'bar';
     *
     * @param string $name
     * @return mixed parameter value or NULL,
     * if parameter is not exists
     */
    public function __get($name) {
        return $this->get($name);
    }

    /**
     * Setting value of parameter
     *
     * @param string $name
     * @param string $value
     * @return session
     */
    public function set($name, $value = NULL) {
        $_SESSION[$name] = $value;
        return $this;
    }

    /**
     * Setting value of parameter by magic method __get
     *
     * echo session::get_instance()->foo;
     *
     * @param string $name
     * @param string $value
     * @return session
     */
    public function __set($name, $value = NULL) {
        return $this->set($name, $value);
    }

    /**
     * Checking the existence of the parameter session by magic method __isset
     *
     * isset(session::get_instance()->foo);
     *
     * @param string $name
     * @return bool
     */
    public function __isset($name) {
        return isset($name, $_SESSION);
    }

    /**
     * Deleting sessiong parameter by magic method __unset
     *
     * unset(session::get_instance()->foo);
     *
     * @param string $name
     */
    public function __unset($name) {
        if (isset($_SESSION[$name])) {
            unset($_SESSION[$name]);
        }
    }

    /**
     * Private constructor
     */
    private function __construct() {}

    /**
     * Cloning ban
     */
    private function __clone() {}
}