<?php
/**
 * @package pFramework
 * @class   paginator
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * Base pagination
 */

namespace lib\pf\paginators;
use lib\pf\arr;

abstract class paginator
{
    protected
        $cur_page_num,
        $num_rows      = 20,
        $num_links     = 3,
        $links_as_html = false,
        $url_key       = 'page',
        $script_url;

    /**
     * Constructor
     *
     * @param int $cur_page_num
     */
    public function __construct($cur_page_num = NULL) {
        $cur_page_num = ($cur_page_num !== NULL) ? $cur_page_num : arr::get($_GET, $this->url_key, 1);
        $this->set_cur_page_num($cur_page_num)
             ->set_script_url($_SERVER['REQUEST_URI']);
    }

    /**
     * Pagination result
     *
     * @return array
     * array(
     *     array $rows,
     *     mixed $links // array or string, depending on a flag $this->links_as_html
     * )
     */
    public function get_pagination() {
        return array(
            $this->get_rows(),
            $this->links_as_html ? $this->get_links_html() : $this->get_links()
        );
    }

    /**
     * Getting current page number
     *
     * @return int
     */
    public function get_cur_page_num() {
        return $this->cur_page_num;
    }

    /**
     * Setting current page number
     *
     * @param int $cur_page_num
     * @return paginator
     */
    public function set_cur_page_num($cur_page_num) {
        $cur_page_num = (int) $cur_page_num;
        $this->cur_page_num = ($cur_page_num > 0) ? $cur_page_num : 1;
        return $this;
    }

    /**
     * Getting rows number on page
     *
     * @return int $this->num_rows
     */
    public function get_num_rows() {
        return $this->num_rows;
    }

    /**
     * Setting rows number on page
     *
     * @param int $num_rows
     * @return paginator
     */
    public  function set_num_rows($num_rows) {
        $this->num_rows = $num_rows;
        return $this;
    }

    /**
     * Getting links HTML-flag
     *
     * @return bool
     */
    public function get_links_as_html() {
        return $this->links_as_html;
    }

    /**
     * Setting links HTML-flag
     *
     * @param bool $links_as_html
     * @return paginator
     */
    public function set_links_as_html($links_as_html) {
        $this->links_as_html = $links_as_html;
        return $this;
    }

    /**
     * Getting script URL
     *
     * @return string
     */
    public function get_script_url() {
        return $this->script_url;
    }

    /**
     * Setting script URL
     *
     * @param string $script_url
     * @return paginator
     */
    public  function set_script_url($script_url) {
        $this->script_url = $script_url;
        return $this;
    }

    /**
     * Getting number of links on the left and right of the current
     *
     * @return int
     */
    public function get_num_links() {
        return $this->num_links;
    }

    /**
     * Setting number of links on the left and right of the current
     *
     * @param int $num_links
     * @return paginator
     */
    public  function set_num_links($num_links) {
        $this->num_links = $num_links;
        return $this;
    }

    /**
     * Getting ULR-key of parameter number on current page
     *
     * @return string
     */
    public function get_url_key() {
        return $this->url_key;
    }

    /**
     * Setting ULR-key of parameter number on current page
     *
     * @param string $url_key
     * @return paginator
     */
    public function set_url_key($url_key) {
        $this->url_key = $url_key;
        return $this;
    }

    /**
     * Pagination links
     *
     * @return array
     * array(
     *     'prev'=>array('num'=>'...', 'link'=>'...'),
     *     'start'=>array(...), 'nums'=>array(...), 'end'=>array(), 'next'=>array(...),
     *     'from'=int '...', from = int '...', for = int '...'
     * )
     */
    protected function get_links() {
        if (!$num_rows_all = $this->get_num_rows_total()) {
            return array();
        }
        $links = array();
        $num_rows  = $this->num_rows;
        $num_pages = ceil($num_rows_all / $this->num_rows);
        $cur_page_num = $this->cur_page_num;
        $script_url = $this->prepare_script_url($this->script_url);
        // previous page
        if ($cur_page_num > 1) {
            $links['prev'] = array(
               'num' =>($cur_page_num - 1),
               'link'=>$script_url . $this->url_key . '=' . ($cur_page_num - 1)
            );
        }
        // links to pages
        // check the links on the left
        if (($cur_page_num - $this->num_links) > 2) {
            // first page
            $links['start'] = array(
                'num' =>1,
                'link'=>$script_url . $this->url_key . '=1'
            );
            // if has links of the left
            for($i = $cur_page_num - $this->num_links; $i < $cur_page_num; $i ++) {
                $links['nums'][$i]['num']  = $i;
                $links['nums'][$i]['link'] = $script_url . $this->url_key . '=' . $i;
            }
        } else {
            // if not has links of the left
            for ($i = 1; $i < $cur_page_num; $i ++) {
                $links['nums'][$i]['num']  = $i;
                $links['nums'][$i]['link'] = $script_url . $this->url_key . '=' . $i;
            }
        }
        // check the links on the right
        if (($cur_page_num + $this->num_links) < $num_pages - 1) {
            // if has links of the right
            for($i = $cur_page_num; $i <= $cur_page_num + $this->num_links; $i ++) {
                $links['nums'][$i]['num'] = $i;
                if ($cur_page_num != $i) {
                    $links['nums'][$i]['link'] = $script_url . $this->url_key . '=' . $i;
                }
            }
            // last page
            $links['end'] = array(
                'num' =>$num_pages,
                'link'=>$script_url . $this->url_key . '=' . $num_pages
            );
        } else {
            // if not has links of the right
            for ($i = $cur_page_num; $i <= $num_pages; $i ++) {
                if ($num_pages == $i) {
                    $links['nums'][$i]['num'] = $i;
                    if ($cur_page_num != $i) {
                        $links['nums'][$i]['link'] = $script_url . $this->url_key . '=' . $i;
                    }
                } else{
                    $links['nums'][$i]['num'] = $i;
                    if ($cur_page_num != $i) {
                        $links['nums'][$i]['link'] = $script_url . $this->url_key . '=' . $i;
                    }
                }
            }
        }
        // next page
        if ($cur_page_num < $num_pages) {
            $links['next'] = array(
                'num' =>($cur_page_num + 1),
                'link'=>$script_url . $this->url_key . '=' . ($cur_page_num + 1)
            );
        }
        $links['all'] = $num_rows_all;
        $links['from'] = ($cur_page_num - 1) * $num_rows + 1;
        if ($num_pages != $cur_page_num) {
            $links['for'] = ($cur_page_num - 1) * $num_rows + $num_rows;
        } else {
            $links['for'] = $num_rows_all;
        }
        return $links;
    }

    /**
     * Pagination links in HMTL
     *
     * @return string $html
     */
    protected  function get_links_html() {
        $html = '<div class="paginator">';
        $links = $this->get_links();
        if (!empty($links) && (count($links['nums']) > 1)) {
            if (isset($links['start'])) {
                $html .= '<a href="' . $links['start']['link'] . '">' . $links['start']['num'] . '</a>&nbsp;...&nbsp';
            }
            foreach ($links['nums'] as $link) {
                if (isset($link['link'])) {
                    $html .= '<a href="' . $link['link'] . '">' . $link['num'] . '</a>';
                } else {
                    $html .= '<span>' . $link['num'] . '</span>';
                }
            }
            if (isset($links['end'])) {
                $html .= '&nbsp;...&nbsp;<a href="' . $links['end']['link'].'">' . $links['end']['num'] . '</a>';
            }
        }
        $html .= '</div>';
        return $html;
    }

    /**
     * Getting rows on the page
     *
     * @return array
     */
    abstract protected function get_rows();

    /**
     * Getting total number of rows
     *
     * @return int
     */
    abstract protected function get_num_rows_total();

    /**
     * Getting start index
     *
     * @return int
     */
    protected function get_index_start() {
        return ($this->cur_page_num - 1) * $this->num_rows;
    }

    /**
     * Preparing script URL
     *
     * @param string $script_url
     * @return string $script_url
     */
    protected function prepare_script_url($script_url) {
        // remove the current page number
        $script_url = preg_replace('/(&?' . $this->url_key . '=([^&]+)?)+/ui', '', $script_url);
        // remove the symbol ? at the end
        $script_url = preg_replace('/\?$/ui', '', $script_url);
        $parse_url = parse_url($script_url);
        $script_url .= empty($parse_url['query']) ? '?' : '&';
        return $script_url;
    }
}