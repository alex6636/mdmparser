<?php
/**
 * @package pFramework
 * @class   paginator_arr
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * Array paginator
 */

namespace lib\pf\paginators;

class paginator_arr extends paginator
{
    protected $arr;

    /**
     * Constructor
     *
     * @param array $arr
     * @param int $cur_page_num
     */
    public function __construct(array $arr, $cur_page_num = NULL) {
        parent::__construct($cur_page_num);
        $this->set_arr($arr);
    }

    /**
     * Getting pagination array
     *
     * @return array
     */
    public function get_arr() {
        return $this->arr;
    }

    /**
     * Setting pagination array
     *
     * @param array $arr
     * @return paginator_arr
     */
    public function set_arr(array $arr) {
        $this->arr = $arr;
        return $this;
    }

    /**
     * Getting rows on the page
     *
     * @return array
     */
    protected function get_rows() {
        $num_rows_total = $this->get_num_rows_total();
        $index_start = $this->get_index_start();
        $length = (($index_start + $this->num_rows) <= $num_rows_total) ?
                  $this->num_rows :
                  ($num_rows_total - $index_start);
        return array_slice($this->arr, $index_start, $length);
    }

    /**
     * Getting total number of rows
     *
     * @return int
     */
    protected function get_num_rows_total() {
        return count($this->arr);
    }
}