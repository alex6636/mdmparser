<?php
/**
 * @package pFramework
 * @class   paginator_db
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    December, 2013
 * @version $Id
 *
 * Database paginator
 */

namespace lib\pf\paginators;
use lib\pf\db\drivers\driver_pdo;
use lib\pf\exceptions\exception_argument;
use \PDO;

class paginator_db extends paginator
{
   /** @var driver_pdo $db_driver */
    protected $db_driver;
    protected
        $query,
        $query_params       = NULL,
        $query_total,
        $query_total_params = NULL;

    /**
     * Constructor
     *
     * @param driver_pdo $db_driver database driver
     * @param string $query select query without LIMIT
     * @param mixed $query_params select query parameters
     * @param int $cur_page_num current page number
     */
    public function __construct(driver_pdo $db_driver, $query, $query_params = NULL, $cur_page_num = NULL) {
        parent::__construct($cur_page_num);
        $this->set_db_driver($db_driver)
             ->set_query($query)
             ->set_query_params($query_params)
             ->set_query_total();
    }

    /**
     * Getting database driver
     *
     * @return driver_pdo
     */
    public function get_db_driver() {
        return $this->db_driver;
    }

    /**
     * Setting database driver
     *
     * @param driver_pdo $db_driver
     * @return paginator_db
     */
    public function set_db_driver(driver_pdo $db_driver) {
        $this->db_driver = $db_driver;
        return $this;
    }

    /**
     * Getting select query
     *
     * @return string
     */
    public function get_query() {
        return $this->query;
    }

    /**
     * Setting select query
     *
     * @param string $query
     * @throws exception_argument if the query contains a LIMIT
     * @return paginator_db
     */
    public function set_query($query) {
        if (preg_match('/\s+LIMIT\s+/ui', $query)) {
            throw new exception_argument('Query should not contain LIMIT clauses');
        }
        if ($this->db_driver->connect()->getAttribute(PDO::ATTR_DRIVER_NAME) == 'mysql') {
            // appends SQL_CALC_FOUND_ROWS only if it
            // not used in the query initially
            $query = preg_replace(
                '/^\s*SELECT(?!\s+(SQL_CALC_FOUND_ROWS))/ui',
                'SELECT SQL_CALC_FOUND_ROWS',
                $query
            );
        }
        $this->query = $query;
        return $this;
    }

    /**
     * Getting select query parameters
     *
     * @return mixed
     */
    public function get_query_params() {
        return $this->query_params;
    }

    /**
     * Setting select query parameters
     *
     * @param mixed $params
     * @return paginator_db
     */
    public function set_query_params($params) {
        $this->query_params = $params;
        return $this;
    }

    /**
     * Getting a query for counting total number of records
     *
     * @return string
     */
    public function get_query_total() {
        return $this->query_total;
    }

    /**
     * Setting a query for counting total number of records
     *
     * @param string $query_total
     * If not passed query text, depending on the database driver
     * Will generate a request for Counting the total number of records.
     * For mysql SELECT FOUND_ROWS (), for other drivers
     * COUNT will be appended to the select query. This will work correctly
     * Only if the request has the form of a "SELECT * FROM ...",
     * During enumeration fields request for counting
     * Total number of records need to be initialized manually.
     * @return paginator_db
     */
    public function set_query_total($query_total = NULL) {
        if ($query_total === NULL) {
            $query_total = ($this->db_driver->connect()->getAttribute(PDO::ATTR_DRIVER_NAME) == 'mysql') ?
                           'SELECT FOUND_ROWS()' :
                           preg_replace('/^SELECT\s+\*/ui', 'SELECT COUNT(*)', $this->query);
            $this->set_query_total_params($this->query_params);
        }
        $this->query_total = $query_total;
        return $this;
    }

    /**
     * Getting a query counting parameters
     *
     * @return mixed
     */
    public function get_query_total_params() {
        return $this->query_total_params;
    }

    /**
     * Setting a query counting parameters
     *
     * @param mixed $params
     * @return paginator_db
     */
    public function set_query_total_params($params) {
        $this->query_total_params = $params;
        return $this;
    }

    /**
     * Getting rows on the page
     *
     * @return array
     */
    protected function get_rows() {
        $query = $this->query . PHP_EOL . 'LIMIT ' . $this->get_index_start() . ',' . $this->num_rows;
        return $this->db_driver->query($query, $this->query_params)->fetchAll();
    }

    /**
     * Getting total number of rows
     *
     * @return int
     */
    protected function get_num_rows_total() {
        return $this->db_driver->query($this->query_total, $this->query_total_params)->fetchColumn();
    }
}