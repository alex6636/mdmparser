<?php
/**
 * @package pFramework
 * @class   template
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    June, 2014
 * @version $Id
 *
 * Template
 */

namespace lib\pf\template;
use lib\pf\pf;
use lib\pf\arr;
use lib\pf\exceptions\exception_file;
use lib\pf\str;
use lib\pf\file;

class template
{
    const
        // Processing method when adding a new parameter,
        // if the parameter with the same name already exists
        SET_PARAM_SKIP      = 0,
        SET_PARAM_OVERWRITE = 1;

    protected
        $tpl_dir,
        $compile_dir,
        $cache_dir,
        $compile_dir_def   = 'compile',
        $cache_dir_def     = 'cache',
        $tpl_dir_mtime     = 0,
        $compile_dir_mtime = 0,
        $cache_lifetime    = 0,
        $cache_key,
        $xss_filter        = true,
        $drop_blank        = false,
        $precompile        = true,
        $short_tags        = true,
        $ld,
        $rd,
        $tpl_ext           = PF_TPL_FILE_EXT,
        $cache_ext         = PF_CACHE_FILE_EXT,
        $params            = array(),
        $blocks            = array(),
        $extends_blocks    = true,
        $extends_names     = array(),
        $compiled_files    = array();

    /**
     * Constructor
     *
     * @param string $tpl_dir path to templates directory
     * @param string $compile_dir path to directory with compiled php files
     * @param array $cache_dir path to cache directory
     * @param string $ld left delimeter
     * @param string $rd right delimeter
     */
    public function __construct($tpl_dir, $compile_dir = NULL, $cache_dir = NULL, $ld = '{{', $rd = '}}') {
        $this->set_tpl_dir($tpl_dir)
             ->set_compile_dir($compile_dir)
             ->set_cache_dir($cache_dir)
             ->set_cache_key($_SERVER['REQUEST_URI'])
             ->set_ld($ld)
             ->set_rd($rd)
             ->set_param('this', $this);
    }

    /**
     * Destructor
     */
    public function __destruct() {
        if (!$this->precompile) {
            // remove compiled files
            array_map(function($file) {
                if (is_file($file)) unlink($file);
            }, $this->compiled_files);
        }
    }

    /**
     * Getting all parameters
     *
     * @return array
     */
    public function get_params() {
        return $this->params;
    }

    /**
     * Getting parameter
     *
     * @param string $name
     * @return mixed
     */
    public function get_param($name) {
        return arr::get($this->params, $name);
    }

    /**
     * Getting parameter via magic method __get
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name) {
        return $this->get_param($name);
    }

    /**
     * Setting parameter
     *
     * @param string $name
     * @param mixed $value
     * @param int $type processing method
     * static::SET_PARAM_OVERWRITE by default
     * @return template
     */
    public function set_param($name, $value, $type = self::SET_PARAM_OVERWRITE) {
        if (!array_key_exists($name, $this->params) || ($type === self::SET_PARAM_OVERWRITE)) {
            $this->params[$name] = $value;
        }
        return $this;
    }

    /**
     * Setting parameter via magic method __set
     *
     * $tpl->param_name = 'value';
     *
     * @param string $name
     * @param mixed $value
     * @return template
     */
    public function __set($name, $value) {
        $this->set_param($name, $value);
        return $this;
    }

    /**
     * Extracting parameters from array
     *
     * @param array $params as
     * array('name1'=>'value1', 'name2'=>'value2'[,...])
     * @param int $type processing method
     * static::SET_PARAM_OVERWRITE by default
     * @return template
     */
    public function extract(array $params = NULL, $type = self::SET_PARAM_OVERWRITE) {
        if ($params !== NULL) {
            foreach ($params as $name=>$value) {
                $this->set_param($name, $value, $type);
            }
        }
        return $this;
    }

    /**
     * Check the existence of the parameter via magic method __isset
     *
     * isset($tpl->foo);
     *
     * @param string $name
     * @return bool
     */
    public function __isset($name) {
        return array_key_exists($name, $this->params);
    }

    /**
     * Delete parameter via magic method __unset
     *
     * unset($tpl->foo);
     *
     * @param string $name
     * @return template
     */
    public function __unset($name) {
        if (isset($this->params)) {
            unset($this->params[$name]);
        }
        return $this;
    }

    /**
     * Getting templates directory
     *
     * @return string
     */
    public function get_tpl_dir() {
        return $this->tpl_dir;
    }

    /**
     * Setting templates directory
     *
     * @param string $tpl_dir
     * @throws exception_file if $tpl_dir is not directory
     * @return template
     */
    public function set_tpl_dir($tpl_dir) {
        if (!is_dir($tpl_dir)) {
            throw new exception_file($tpl_dir, 13);
        }
        $this->tpl_dir = str::append_last_slash($tpl_dir);
        $this->tpl_dir_mtime = file::dirmtime($tpl_dir);
        return $this;
    }

    /**
     * Getting compiled files directory
     *
     * @return string
     */
    public function get_compile_dir() {
        return $this->compile_dir;
    }

    /**
     * Setting compiled files directory
     *
     * @param string $compile_dir
     * @return template
     */
    public function set_compile_dir($compile_dir) {
        // if the directory is not specified, use the directory at the same level
        // directory that's templates, called $this->compile_dir_def default
        $compile_dir = ($compile_dir !== NULL) ? $compile_dir : str::replace_basename($this->tpl_dir, $this->compile_dir_def);
        $compile_dir = str::append_last_slash($compile_dir);
        file::create_writable_dir($compile_dir);
        $this->compile_dir = $compile_dir;
        $this->compile_dir_mtime = file::dirmtime($compile_dir);
        return $this;
    }

    /**
     * Getting cache directory
     *
     * @return string
     */
    public function get_cache_dir() {
        return $this->cache_dir;
    }

    /**
     * Setting cache directory
     *
     * @param string $cache_dir
     * @return template
     */
    public function set_cache_dir($cache_dir) {
        // if the directory is not specified, use the directory at the same level
        // directory that's templates, called $this->cache_dir_def default
        $cache_dir = ($cache_dir !== NULL) ? $cache_dir : str::replace_basename($this->tpl_dir, $this->cache_dir_def);
        $cache_dir = str::append_last_slash($cache_dir);
        file::create_writable_dir($cache_dir);
        $this->cache_dir = $cache_dir;
        return $this;
    }

    /**
     * Getting XSS-filter flag
     *
     * @return bool
     */
    public function get_xss_filter() {
        return $this->xss_filter;
    }

    /**
     * Setting XSS-filter flag
     *
     * @param bool $flag
     * @return template
     */
    public function set_xss_filter($flag) {
        $this->xss_filter = $flag;
        return $this;
    }

    /**
     * Getting flag removing spaces, tabs, and hyphenation
     *
     * @return bool
     */
    public function get_drop_blank() {
        return $this->drop_blank;
    }

    /**
     * Setting flag removing spaces, tabs, and hyphenation
     *
     * @param bool $flag
     * @return template
     */
    public function set_drop_blank($flag) {
        $this->drop_blank = $flag;
        return $this;
    }

    /**
     * Getting precompilation mode
     *
     * @return bool
     */
    public function get_precompile() {
        return $this->precompile;
    }

    /**
     * Setting precompilation mode
     *
     * @param bool $flag
     * if false, templates recompiled again
     * each time the page is refreshed
     * @return template
     */
    public function set_precompile($flag) {
        $this->precompile = $flag;
        return $this;
    }

    /**
     * Getting short php tag
     *
     * @return bool
     */
    public function get_short_tags() {
        return $this->short_tags;
    }

    /**
     * Setting short php tag
     *
     * @param bool $flag
     * @return template
     */
    public function set_short_tags($flag) {
        $this->short_tags = $flag;
        return $this;
    }

    /**
     * Getting left delimeter
     *
     * @return string
     */
    public function get_ld() {
        return $this->ld;
    }

    /**
     * Setting left delimeter
     *
     * @param string $ld
     * @return template
     */
    public function set_ld($ld) {
        $this->ld = preg_quote($ld, '/');
        return $this;
    }

    /**
     * Getting right delimeter
     *
     * @return string
     */
    public function get_rd() {
        return $this->rd;
    }

    /**
     * Setting right delimeter
     *
     * @param string $rd
     * @return template
     */
    public function set_rd($rd) {
        $this->rd = preg_quote($rd, '/');
        return $this;
    }

    /**
     * Getting the lifetime of cache files
     *
     * @return int seconds
     */
    public function get_cache_lifetime() {
        return $this->cache_lifetime;
    }

    /**
     * Setting the lifetime of cache files
     *
     * @param int $cache_lifetime seconds
     * @return template
     */
    public function set_cache_lifetime($cache_lifetime) {
        $this->cache_lifetime = $cache_lifetime;
        return $this;
    }

    /**
     * Getting cache key
     *
     * @return string
     */
    public function get_cache_key() {
        return $this->cache_key;
    }

    /**
     * Setting cache key
     *
     * @param string $cache_key
     * @return template
     */
    public function set_cache_key($cache_key) {
        $this->cache_key = $cache_key;
        return $this;
    }

    /**
     * Getting extension template files
     *
     * @return string
     */
    public function get_tpl_ext() {
        return $this->tpl_ext;
    }

    /**
     * Setting extension template files
     *
     * @param string $tpl_ext
     * @return template
     */
    public function set_tpl_ext($tpl_ext) {
        $this->tpl_ext = $tpl_ext;
        return $this;
    }

    /**
     * Getting extension cache files
     *
     * @return string
     */
    public function get_cache_ext() {
        return $this->cache_ext;
    }

    /**
     * Setting extension cache files
     *
     * @param string $cache_ext
     * @return template
     */
    public function set_cache_ext($cache_ext) {
        $this->cache_ext = $cache_ext;
        return $this;
    }

    /**
     * Display pattern on the screen
     *
     * @param string $tpl_name template name, extension is not necessarily
     * @return template
     */
    public function display($tpl_name) {
        if ($this->cache_lifetime > 0) {
            if (!$this->is_cached($tpl_name)) {
                $this->extends_blocks = true;
                $this->compile($tpl_name)
                     ->save_cache_contents($tpl_name);
            }
            $this->include_compile_tpl($this->get_cache_path($tpl_name));
        } else {
            $this->extends_blocks = true;
            $this->compile($tpl_name)
                 ->include_compile_tpl($this->get_compile_path($tpl_name));
        }
        return $this;
    }

    /**
     * Template content in a variable
     *
     * @param string $tpl_name
     * @return string
     */
    public function fetch($tpl_name) {
        ob_start();
        $this->display($tpl_name);
        return ob_get_clean();
    }

    /**
     * Filtering before output to HTML
     *
     * @param string $str
     * @return string
     */
    public function filter($str) {
        return htmlspecialchars($str, ENT_QUOTES, PF_ENCODING);
    }

    /**
     * Checking the existence of the template
     *
     * @param string $tpl_name
     * @return bool
     */
    public function is_exists($tpl_name) {
        return is_file($this->get_tpl_path($tpl_name));
    }

    /**
     * Checking the existence of the cache template
     *
     * @param string $tpl_name template name, extension is not necessarily
     * @return bool
     */
    public function is_cached($tpl_name) {
        $cache_path = $this->get_cache_path($tpl_name);
        if (is_file($cache_path)) {
            if (time() < (filemtime($cache_path) + $this->cache_lifetime)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Clearing the cache template
     *
     * @param string $tpl_name
     */
    public function clear_cache($tpl_name) {
        $cache_path = $this->get_cache_path($tpl_name);
        if (is_file($cache_path)) {
            @unlink($cache_path);
        }
    }

    /**
     * Cleaning the entire cache directory
     */
    public function clear_all_cache() {
        if ($dir_handle = opendir($this->cache_dir)) {
            while (($file = readdir($dir_handle)) !== false) {
                if (str::extension($file) == $this->cache_ext) {
                    @unlink($this->cache_dir . $file);
                }
            }
            closedir($dir_handle);
        }
    }

    /**
     * Getting the file name without the extension template
     *
     * @param string $tpl_name template name, extension is not necessarily
     * @return string
     */
    protected function get_tpl_name_key($tpl_name) {
        return str::remove_extension($tpl_name, $this->tpl_ext);
    }

    /**
     * Getting the name of the template file with the extension
     *
     * @param string $tpl_name template name, extension is not necessarily
     * @return string
     */
    protected function get_tpl_name_full($tpl_name) {
        return str::append_extension($tpl_name, $this->tpl_ext);
    }

    /**
     * Getting the path to the template file
     *
     * @param string $tpl_name
     * @return string
     */
    protected function get_tpl_path($tpl_name) {
        return $this->tpl_dir . $this->get_tpl_name_full($tpl_name);
    }

    /**
     * Getting the path to the compiled template file
     *
     * @param string $tpl_name
     * @return string
     */
    protected function get_compile_path($tpl_name) {
        return $this->compile_dir . $this->get_tpl_name_full(preg_replace('/\.php$/ui', '', $tpl_name)) . '.php';
    }

    /**
     * Getting the path to the file cache
     *
     * @param string $tpl_name
     * @return string
     */
    protected function get_cache_path($tpl_name) {
        return $this->cache_dir . md5($this->get_tpl_name_full($tpl_name) . $this->cache_key) . $this->cache_ext;
    }

    /**
     * Saving the content cache
     *
     * @param string $tpl_name template name, extension is not necessarily
     * @return template
     */
    protected function save_cache_contents($tpl_name) {
        $compile_path = $this->get_compile_path($tpl_name);
        $cache_path = $this->get_cache_path($tpl_name);
        $cache_content = file::get_contents($compile_path);
        $cache_tmp_path = $this->compile_dir . basename($cache_path) . '.php';
        $this->assembly_cache_contents($cache_content, $tpl_name);
        // replace to require non-executable fragments
        // for a temporary file cache, in which they shall not be made
        $cache_content = preg_replace(
            '/<\?php\s+require\s+(\'|")([^"\']+)("|\');\s+\?>/ui',
            '<!--cache::$2;-->',
            $cache_content
        );
        file::put_contents($cache_tmp_path, $cache_content);
        ob_start();
        $this->include_compile_tpl($cache_tmp_path);
        $ob_cache_content = ob_get_clean();
        $ob_cache_content = preg_replace(
            '/<\!--cache::([^;]+);-->/ui',
            '<?php require \'' . $this->compile_dir . '$1\'; ?>',
            $ob_cache_content
        );
        file::put_contents($cache_path, $ob_cache_content);
        @unlink($cache_tmp_path);
        return $this;
    }

    /**
     * Assembly cache
     *
     * During assembly is replaced include all inclusions
     * On their contents.
     *
     * @param string &$content
     * @param string $tpl_name
     * @return template
     */
    protected function assembly_cache_contents(&$content, $tpl_name) {
        $compile_path = $this->get_compile_path($tpl_name);
        $compile_content = file::get_contents($compile_path);
        $pattern = '/(<\?php\s+include\s+("|\')([^"\']+)\s*("|\');\s*\?>)/ui';
        preg_match_all($pattern, $compile_content, $include);
        if (!empty($include[3])) {
            foreach ($include[3] as $key=>$tpl_name) {
                $include_compile_path = $this->get_compile_path($tpl_name);
                $content = str_replace($include[0][$key],  file::get_contents($include_compile_path), $content);
                $this->assembly_cache_contents($content, $tpl_name);
            }
        }
        return $this;
    }

    /**
     * Compiling template
     *
     * @param string $tpl_name template name, extension is not necessarily
     * @param string $compile_name the name of the source file
     * unless you pass - used $tpl_name
     * @return template
     */
    protected function compile($tpl_name, $compile_name = NULL) {
        if ($compile_name === NULL) $compile_name = $tpl_name;
        $compile_path = $this->get_compile_path($compile_name);
        if ($this->precompile) {
            if (is_file($compile_path) && ($this->tpl_dir_mtime <= $this->compile_dir_mtime)) {
                return $this;
            }
        }
        $tpl_path = $this->get_tpl_path($tpl_name);
        $content = file::get_contents($tpl_path);
        if (!empty($content)) {
            if ($this->drop_blank) {
                $content = str::reduce_spaces(str::drop_blank($content));
            }
            $tpl_name_key = $this->get_tpl_name_key($tpl_name);
            if ($this->extends_blocks) {
                $extends_content = $content;
                $this->blocks = array();
                $this->extends_blocks($extends_content, $tpl_name_key);
                $this->extends_blocks = false;
            }
            $this->parse_tpl($content, $tpl_name_key);
        }
        $this->save_compile_tpl($compile_name, $content);
        return $this;
    }

    /**
     * Saving compiled template
     *
     * @param string $tpl_name
     * @param string $content
     * @return template
     */
    protected function save_compile_tpl($tpl_name, $content) {
        $compile_path = $this->get_compile_path($tpl_name);
        $comment_lines   = array();
        $comment_lines[] = '<?php';
        $comment_lines[] = '/* This file was automatically created by ' . pf::get_title() . ' template engine */';
        $comment_lines[] = '/* @var ' . __CLASS__ . ' $this */';
        $comment_lines[] = '?>';
        $comment_lines[] = '';
        file::put_contents($compile_path, implode(PHP_EOL, $comment_lines) . $content);
        $this->compiled_files[] = $compile_path;
        return $this;
    }

    /**
     * Including the compiled template file with parameters
     *
     * @param string $compile_path
     * @throws exception_file if $compile_path is not file
     * @return template
     */
    protected function include_compile_tpl($compile_path) {
        if (!is_file($compile_path)) {
            throw new exception_file($compile_path, 1);
        }
        if (!empty($this->params)) {
            extract($this->params);
        }
        include $compile_path;
        return $this;
    }

    /**
     * Extending templates blocks
     *
     * @param string &$content
     * @param string $tpl_name_key
     * @return template
     */
    protected function extends_blocks(&$content, $tpl_name_key) {
        $ld = $this->ld;
        $rd = $this->rd;
        // {block name}
        $pattern_block_open  = $ld . '\s*block\s+(([^\s' . $rd . '])*[^\s])\s*' . $rd;
        // {/block}
        $pattern_block_close = $ld . '\s*\/\s*block\s*' . $rd;
        // get the contents of the root block
        // in which no nested blocks
        $pattern = '/' . $pattern_block_open . '((?:(?!' . $ld . '\s*block).)*)' . $pattern_block_close . '/suiU';
        if (preg_match($pattern, $content, $block_matches)) {
            $block       = $block_matches[0];
            $block_name  = $block_matches[1];
            $block_inner = $block_matches[3];
            $extend = false;
            // find blocks with the same name
            foreach ($this->blocks as $blocks_tpl_name=>$blocks_block) {
                if (array_key_exists($block_name, $blocks_block)) {
                    $extend = true;
                    // replace the contents of the current block to the contents of the extending block
                    $this->blocks[$tpl_name_key][$block_name] = $blocks_block[$block_name];
                    // change the contents of the current block
                    $block_inner = $blocks_block[$block_name];
                    // clear the contents of the extending block
                    $this->blocks[$blocks_tpl_name][$block_name] = '';
                }
            }
            if (!$extend) {
                // extending was not, we bring new block
                $this->blocks[$tpl_name_key][$block_name] = $block_inner;
            }
            // leave only the contents of the block
            $content = str_replace($block, $block_inner, $content);
            // extend other blocks
            $this->extends_blocks($content, $tpl_name_key);
        } else {
            // parse nested extends
            // {extends filename.*} / {extends $filename}
            // find included files
            $pattern = '/(' . $ld . '\s*extends\s+("|\')?\s*(?!(\.\.|\/|\\\))([\S])+\s*("|\')?\s*' . $rd . ')/ui';
            if (preg_match_all($pattern, $content, $extends_matches)) {
                // parse files sequentially
                foreach ($extends_matches[1] as $extends) {
                    // extract the name of the included file
                    $pattern = '/' . $ld . '\s*extends\s*("|\')?\s*([^"\'\s' . $ld . $rd . ']+)\s*("|\')?\s*' . $rd . '/ui';
                    if (preg_match($pattern, $extends, $extend_matches)) {
                        $extend_file = $extend_matches[2];
                        $content = file::get_contents($this->get_tpl_path($extend_file));
                        // extend other blocks
                        $this->extends_blocks($content, $extend_file);
                    }
                }
            }
        }
        return $this;
    }

    /**
     * Parse template
     *
     * @param string &$content
     * @param string $tpl_name_key
     * @return template
     */
    protected function parse_tpl(&$content, $tpl_name_key) {
        $this->parse_blocks($content, $tpl_name_key)
             ->parse_extends($content, $tpl_name_key)
             ->parse_include($content, 'include')
             ->parse_include($content, 'insert')
             ->parse_comments($content)
             ->parse_if($content)
             ->parse_foreach($content)
             ->parse_section($content)
             ->parse_params($content)
             ->parse_echo_nofilter($content)
             ->parse_echo($content);
        return $this;
    }

    /**
     * Parse blocks
     *
     * @param string &$content
     * @param string $tpl_name_key
     * @return template
     */
    protected function parse_blocks(&$content, $tpl_name_key) {
        $ld = $this->ld;
        $rd = $this->rd;
        // {block name}
        $pattern_block_open  = $ld . '\s*block\s+(([^\s' . $rd . '])*[^\s])\s*' . $rd;
        // {/block}
        $pattern_block_close = $ld . '\s*\/\s*block\s*' . $rd;
        // Get the contents of the root block
        // in which no nested blocks
        $pattern = '/' . $pattern_block_open . '((?:(?!' . $ld . '\s*block).)*)' . $pattern_block_close . '/suiU';
        if (preg_match($pattern, $content, $block_matches)) {
            $block       = $block_matches[0];
            $block_name  = $block_matches[1];
            $block_inner = $block_matches[3];
            // content for replace block
            $replace = isset($this->blocks[$tpl_name_key][$block_name]) ?
                             $this->blocks[$tpl_name_key][$block_name] :
                             $block_inner;
            $content = str_replace($block, $replace, $content);
            // replace other blocks
            $this->parse_blocks($content, $tpl_name_key);
        }
        return $this;
    }

    /**
     * Parse extends
     *
     * Can be connected files without extension,
     * then the expansion will be taken from $this->tpl_ext
     *
     * @param string &$content
     * @param string $tpl_name_key
     * @return template
     */
    protected function parse_extends(&$content, $tpl_name_key) {
        $ld = $this->ld;
        $rd = $this->rd;
        // array names for extending blocks based on the level of nesting
        $this->extends_names[] = $tpl_name_key;
        // {extends filename.*} / {extends $filename}
        $pattern = '/(' . $ld . '\s*extends\s+("|\')?\s*(?!(\.\.|\/|\\\))([\S])+\s*("|\')?\s*' . $rd . ')/ui';
        if (preg_match_all($pattern, $content, $extends_matches)) {
            foreach ($extends_matches[1] as $extends) {
                $pattern = '/' . $ld . '\s*extends\s*("|\')?\s*([^"\'\s' . $ld . $rd . ']+)\s*("|\')?\s*' . $rd . '/ui';
                if (preg_match($pattern, $extends, $extend_matches)) {
                    $extend      = $extend_matches[0];
                    $extend_file = $extend_matches[2];
                    $tpl_name_full = $this->get_tpl_name_full($extend_file);
                    $compile_name = implode('.extends.', $this->extends_names) . '.extends.' . $this->get_tpl_name_key($extend_file);
                    $compile_name_full = $this->get_tpl_name_full($compile_name);
                    $this->compile($tpl_name_full, $compile_name_full);
                    $content = str_replace($extend, '<?php include \'' . $compile_name_full . '.php\'; ?>', $content);
                }
            }
        } else {
            // go to the first level
            array_splice($this->extends_names, 1, count($this->extends_names));
        }
        return $this;
    }

    /**
     * Parse include
     *
     * Can be connected files without extension,
     * then the expansion will be taken from $this->tpl_ext
     *
     * @param string &$content
     * @param string $tpl_include_tag tag (type) including (include or insert)
     * @return template
     */
    protected function parse_include(&$content, $tpl_include_tag) {
        $ld = $this->ld;
        $rd = $this->rd;
        $include_function = ($tpl_include_tag == 'insert') ? 'require' : 'include';
        // {include filename.*} / {include $filename}
        $pattern = '/(' . $ld . '\s*' . $tpl_include_tag . '\s+("|\')?\s*(?!(\.\.|\/|\\\))([\S])+\s*("|\')?\s*' . $rd . ')/ui';
        if (preg_match_all($pattern, $content, $incs)) {
            foreach ($incs[1] as $include) {
                $pattern = '/' . $ld . '\s*' . $tpl_include_tag . '\s*("|\')?\s*([^"\'\s' . $ld . $rd . ']+)\s*("|\')?\s*' . $rd . '/ui';
                if (preg_match($pattern, $include, $include_matches)) {
                    $include      = $include_matches[0];
                    $include_file = $include_matches[2];
                    $tpl_name_full = $this->get_tpl_name_full($include_file);
                    $this->compile($tpl_name_full);
                    $content = str_replace(
                        $include,
                        '<?php ' . $include_function . ' \'' . $tpl_name_full . '.php\'; ?>',
                        $content
                    );
                }
            }
        }
        return $this;
    }

    /**
     * Parse comments
     *
     * @param string &$content
     * @return template
     */
    protected function parse_comments(&$content) {
        $ld = $this->ld;
        $rd = $this->rd;
        // remove comments
        // {* comment *}
        $pattern = '/' . $ld . '\s*\*\s*([^' . $rd . '])*\s*\*\s*' . $rd . '/sui';
        $content = preg_replace($pattern, '', $content);
        return $this;
    }

    /**
     * Parse if
     *
     * @param string &$content
     * @return template
     */
    protected function parse_if(&$content) {
        $ld = $this->ld;
        $rd = $this->rd;
        $patterns     = array();
        $replacements = array();
        // {if (statements)}
        $patterns[]     = '/' .$ld . '\s*if\s*(([^' . $rd . '])*[^\s])\s*' . $rd . '/ui';
        $replacements[] = '<?php if ($1) { ?>';
        // {/if elseif (statements)}
        $patterns[]     = '/' .$ld . '\s*else\s*if\s*(([^' . $rd . '])*[^\s])\s*' . $rd . '/ui';
        $replacements[] = '<?php } elseif($1) { ?>';
        // {else}
        $patterns[]     = '/' .$ld . '\s*else\s*' . $rd . '/ui';
        $replacements[] = '<?php } else { ?>';
        // {/if}
        $patterns[]     = '/' .$ld . '\s*\/\s*if\s*' . $rd . '/ui';
        $replacements[] = '<?php } ?>';
        $content = preg_replace($patterns, $replacements, $content);
        return $this;
    }

    /**
     * Parse each/foreach
     *
     * @param string &$content
     * @return template
     */
    protected function parse_foreach(&$content) {
        $ld = $this->ld;
        $rd = $this->rd;
        $patterns     = array();
        $replacements = array();
        // {each/foreach statements as [key=>] value}
        $patterns[]     = '/' .$ld . '\s*(for)?each\s*((([^' . $rd . '])*[^\s])\s+as' .
                          '\s+((([\$]{1}\w+([^' . $rd . '])*)+\s*=>)\s*)?(([\$]{1}\w+)' .
                          '([^' . $rd . '])*))+\s*' . $rd . '/ui';
        $replacements[] = '<?php foreach ($3 as $6$10) { ?>';
        // {/each/foreach}
        $patterns[]     = '/' .$ld . '\s*\/\s*(for)?each\s*' . $rd . '/ui';
        $replacements[] = '<?php } ?>';
        $content = preg_replace($patterns, $replacements, $content);
        return $this;
    }

    /**
     * Parse section (for)
     *
     * @param string &$content
     * @return template
     */
    protected function parse_section(&$content) {
        $ld = $this->ld;
        $rd = $this->rd;
        $patterns     = array();
        $replacements = array();
        // {section name=$index start=statements loop=$array step=statements}
        $patterns[]     = '/' .$ld . '\s*section\s+name\s*=\s*(([^' . $rd . '])*[^\s])' .
                          '\s+start\s*=\s*(([^' . $rd . '])*)\s+loop\s*=\s*(([^' . $rd . '])*[^\s])' .
                          '\s+step\s*=\s*(([^' . $rd . '])*[^\s])\s*' . $rd . '/ui';
        $replacements[] = '<?php for ($1 = $3; $1 < count($5); $1 += $7) { ?>';
        // {section name=$index start=statements loop=$array}
        $patterns[]     = '/' .$ld . '\s*section\s+name\s*=\s*(([^' . $rd . '])*[^\s])' .
                          '\s+start\s*=\s*(([^' . $rd . '])*)\s+loop\s*=\s*(([^' . $rd . '])*[^\s])\s*' . $rd . '/ui';
        $replacements[] = '<?php for ($1 = $3; $1 < count($5); $1 ++) { ?>';
        // {section name=$index start=statements max=statements step=statements}
        $patterns[]     = '/' .$ld . '\s*section\s+name\s*=\s*(([^' . $rd . '])*[^\s])' .
                          '\s+start\s*=\s*(([^' . $rd . '])*)\s+max\s*=\s*(([^' . $rd . '])*[^\s])' .
                          '\s+step\s*=\s*(([^' . $rd . '])*[^\s])\s*' . $rd . '/ui';
        $replacements[] = '<?php for ($1 = $3; $1 <= $5; $1 += $7) { ?>';
        // {section name=$index start=statements max=statements step=statements}
        $patterns[]     = '/' .$ld . '\s*section\s+name\s*=\s*(([^' . $rd . '])*[^\s])' .
                          '\s+start\s*=\s*(([^' . $rd . '])*)\s+max\s*=\s*(([^' . $rd . '])*[^\s])\s*' . $rd . '/ui';
        $replacements[] = '<?php for ($1 = $3; $1 <= $5; $1 ++) { ?>';
        // {/section}
        $patterns[]     = '/' .$ld . '\s*\/\s*section\s*' . $rd . '/ui';
        $replacements[] = '<?php } ?>';
        $content = preg_replace($patterns, $replacements, $content);
        return $this;
    }

    /**
     * Parse template parameters
     *
     * {{ param name=value }}
     *
     * @param string &$content
     * @return template
     */
    protected function parse_params(&$content) {
        $ld = $this->ld;
        $rd = $this->rd;
        // {param(php): statements}
        $content = preg_replace(
            '/' .$ld . '\s*param\s*(([^' . $rd . '])+[^\s])\s*' . $rd . '/ui',
            '<?php $1; ?>',
            $content
        );
        return $this;
    }

    /**
     * Parse echo without filtering
     *
     * {{ nofilter $name }}
     *
     * @param string &$content
     * @return template
     */
    protected function parse_echo_nofilter(&$content) {
        $ld = $this->ld;
        $rd = $this->rd;
        $content = preg_replace(
            '/' .$ld . '\s*nofilter\s*([^' . $rd . ']+[^\s])+\s*' . $rd . '/ui',
            $this->short_tags ? '<?=$1; ?>' : '<?php echo $1; ?>',
            $content
        );
        return $this;
    }

    /**
     * Parse echo
     *
     * @param string &$content
     * @return template
     */
    protected function parse_echo(&$content) {
        $ld = $this->ld;
        $rd = $this->rd;
        $replace = $this->xss_filter ? '$this->filter($1)' : '$1';
        $content = preg_replace(
            '/' .$ld . '\s*([^' . $rd . ']+[^\s])+\s*' . $rd . '/ui',
            $this->short_tags ? '<?=' . $replace . '; ?>' : '<?php echo ' . $replace . '; ?>',
            $content
        );
        return $this;
    }
}