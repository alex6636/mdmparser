<?php
/**
 * @package pFramework
 * @class   template_factory
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Template, Factory
 */

namespace lib\pf\template;

class template_factory
{
    /**
     * Creating template object
     *
     * @param array $config
     * array(
     *     'tpl_dir'         =>string
     *     [,'compile_dir'   =>string,]
     *     [,'cache_dir'     =>string,]
     *     [,'cache_lifetime'=>int (default 0),]
     *     [,'xss_filter'    =>bool (default true),]
     *     [,'drop_blank'    =>bool (default false),]
     *     [,'precompile'    =>bool (default true),]
     *     [,'short_tags'    =>bool (default false),]
     *     [,'rd'            =>string (default {{),]
     *     [,'ld'            =>string (default }}),]
     *     [,'cache_key'     =>string (default REQUEST_URI),]
     *     [,'tpl_ext'       =>string (default .tpl)]
     * )
     * @return template $template
     */
    public static function make(array $config) {
        $config = $config + array(
            'tpl_dir'       =>NULL,        // templates directory
            'compile_dir'   =>NULL,        // compiled files directory
            'cache_dir'     =>NULL,        // cache directory
            'cache_lifetime'=>0,           // cache lifetime
            'xss_filter'    =>true,        // XSS filter
            'drop_blank'    =>false,       // removing spaces, tabs, and hyphenation
            'precompile'    =>true,        // templates precompilation
            'short_tags'    =>false,       // short php-tags
            'ld'            =>'{{',        // left delimeter
            'rd'            =>'}}',        // right delimeter
            'cache_key'     =>$_SERVER['REQUEST_URI'], // cache key
            'tpl_ext'       =>'.tpl',      // extension template files
        );
        $template = new template($config['tpl_dir'], $config['compile_dir'], $config['cache_dir'], $config['ld'], $config['rd']);
        $template->set_cache_lifetime($config['cache_lifetime'])
                 ->set_xss_filter($config['xss_filter'])
                 ->set_drop_blank($config['drop_blank'])
                 ->set_precompile($config['precompile'])
                 ->set_short_tags($config['short_tags'])
                 ->set_cache_key($config['cache_key'])
                 ->set_tpl_ext($config['tpl_ext']);
        return $template;
    }
}