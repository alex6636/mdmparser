<?php
/**
 * @package pFramework
 * @class   logger_exception
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Logger exceptions and errors
 */

namespace lib\pf\loggers;

class logger_exception
{
    protected
        $type,
        $message,
        $code,
        $file,
        $line,
        $logger,    // pf_logger
        $date_format = 'd.m.Y H:i:s'; // date format in logs

    /**
     * Constructor
     *
     * @param logger $logger
     * @param string $type
     * @param string $message
     * @param int $code
     * @param string $file
     * @param int $line
     */
    public function __construct(logger $logger, $type, $message, $code, $file, $line) {
        $this->logger = $logger;
        $this->type = $type;
        $this->message = $message;
        $this->code = $code;
        $this->file = $file;
        $this->line = $line;
    }

    /**
     * Getting date format
     *
     * @return string
     */
    public function get_date_format() {
        return $this->date_format;
    }

    /**
     * Setting date format
     *
     * @param string $date_format date format for the method date
     * @return logger_exception
     */
    public function set_date_format($date_format) {
        $this->date_format = $date_format;
        return $this;
    }

    /**
     * Writing logs
     *
     * @return int number of bytes
     */
    public function write() {
        $data_arr = array();
        $data_arr[] = $this->type;
        $data_arr[] = 'Message: [' . $this->code . '] ' . $this->message;
        $data_arr[] = 'File: ' . $this->file;
        $data_arr[] = 'Line: ' . $this->line;
        $data_arr[] = 'Date: ' . date($this->date_format);
        return $this->logger->write(implode(PHP_EOL, $data_arr) . PHP_EOL . PHP_EOL);
    }
}