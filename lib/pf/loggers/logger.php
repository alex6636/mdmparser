<?php
/**
 * @package pFramework
 * @class   logger
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    August, 2013
 * @version $Id
 *
 * Logger
 */

namespace lib\pf\loggers;
use \lib\pf\file;

class logger
{
    protected $file_path; // path to log file

    /**
     * Constructor
     *
     * @param string $file_path path to log file
     */
    public function __construct($file_path) {
        $this->set_file_path($file_path);
    }

    /**
     * Getting path to file
     *
     * @return string
     */
    public function get_file_path() {
        return $this->file_path;
    }

    /**
     * Setting path to file
     *
     * @param string $file_path path to file
     * @return logger
     */
    public function set_file_path($file_path) {
        $file_dir = dirname($file_path);
        file::create_writable_dir($file_dir);
        file::create_writable_file($file_path);
        $this->file_path = $file_path;
        return $this;
    }

    /**
     * Writing logs
     *
     * @param string $data data
     * @return int number of bytes
     */
    public function write($data) {
        return file::put_contents($this->file_path, $data, FILE_APPEND | LOCK_EX);
    }
}