<?php
/**
 * @package pFramework
 * @class   message
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    August, 2013
 * @version $Id
 *
 * Message
 */

namespace lib\pf;
use lib\pf\exceptions\exception_argument;

class message
{
    const
        // format of the returned data
        RETURN_ASSOC     = 0, // array('error'=>'error message')
        RETURN_LIST      = 1, // array('error', 'error message')
        RETURN_FULL_LIST = 2; // array('group', 'error', 'error message')

    protected
        $messages     = array(),
        $groups_codes = array();

    /**
     * Constructor
     *
     * @param array $messages as
     * array(
     *     'add'=>array(
     *         'error'=>'error message'[,...])
     *     ) [,...]
     * )
     */
    public function __construct(array $messages) {
        $this->messages = $messages;
    }

    /**
     * Adding group and message code on the stack
     *
     * @param mixed $group
     * @param mixed $code
     * @throws exception_argument if the message is not found in $this->messages
     */
    public function add($group, $code) {
         if (!arr::depth($this->messages, array($group, $code))) {
            throw new exception_argument('Message not found');
        }
        $this->groups_codes[$group][] = $code;
    }

    /**
     * Getting the last added message
     *
     * After calling this method, the group code and the last message
     * will be extracted from the array $this->groups_codes, if they exist.
     *
     * @param int $return_format
     * @return mixed array depending on the $return_format or false
     */
    public function get_last($return_format = self::RETURN_ASSOC) {
        if (!$this->exists()) {
            return false;
        }
        end($this->groups_codes);
        $group = key($this->groups_codes);
        $code = array_pop($this->groups_codes[$group]);
        if (empty($this->groups_codes[$group])) {
            unset($this->groups_codes[$group]);
        }
        reset($this->groups_codes);
        $message = $this->get($group, $code);
        switch ($return_format) {
            case self::RETURN_LIST:
                return array($code, $message);
            case self::RETURN_FULL_LIST:
                return array($group, $code, $message);
            case self::RETURN_ASSOC:
            default:
                return array($code=>$message);
        }
    }

    /**
     * Getting message by group and code
     *
     * @param mixed $group
     * @param mixed $code
     * @return mixed string 'message' or false if the message does not exist
     */
    public function get($group, $code) {
        return arr::depth($this->messages, array($group, $code), false);
    }

    /**
     * Checking the existence of added message
     *
     * @return bool
     */
    public function exists() {
        return (bool) count($this->groups_codes);
    }
}