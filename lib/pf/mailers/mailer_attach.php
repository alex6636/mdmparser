<?php
/**
 * @package pFramework
 * @class   mailer_attach
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    November, 2013
 * @version $Id
 *
 * Mail sender with attachments
 */

namespace lib\pf\mailers;

class mailer_attach extends mailer
{
    protected $attaches = array(); // attachments

    /**
     * Attachment files
     *
     * @param string $file_path path to file
     * @param string $file_name file name (use basename from $file_path if empty)
     * @return bool result
     * false if the file does not exist, or if cannot read the file
     */
    public function attach_file($file_path, $file_name = NULL) {
        if (is_file($file_path)) {
            if ($handle = fopen($file_path, 'rb')) {
                $file_size = filesize($file_path);
                $content = fread($handle, $file_size);
                if ($file_name === NULL) {
                    $file_name = basename($file_path);
                }
                $content_type = mime_content_type($file_path);
                $this->attaches[] = array(
                    $file_name,
                    $content_type ? $content_type : 'application/octet-stream',
                    chunk_split(base64_encode($content))
                );
                fclose($handle);
                return true;
            }
        }
        return false;
    }

    /**
     * Sending email
     *
     * @param string $to e-mail recipient in the form
     * user@example.com
     * user@example.com, anotheruser@example.com
     * User <user@example.com>
     * User <user@example.com>, Another User <anotheruser@example.com>
     * @param string $subject subject
     * @param string $message message
     * @param string $from sender e-mail
     * user@example.com, User <user@example.com>
     * @param array $additional_headers additional headers
     * @return bool result
     */
    public function send($to, $subject, $message, $from, array $additional_headers = NULL) {
        if (empty($this->attaches)) {
            // send simple email without attachments
            return parent::send($to, $subject, $message, $from, $additional_headers);
        }
        $boundary = md5(uniqid(time()));
        $headers = array();
        $headers[] = 'Mime-Version: 1.0';
        $headers[] = 'Subject: ' .  $subject;
        $headers[] = 'From: ' . $from . ' <' . $from . '>';
        $headers[] = 'Reply-To: ' . $from;
        $headers[] = 'X-Mailer: PHP/' . phpversion();
        $headers[] = 'Content-Type:multipart/mixed;boundary="' . $boundary . '"';
        $headers[] = '';
        $headers[] = '';
        if ($additional_headers !== NULL) {
            $headers = array_merge($headers, $additional_headers);
        }
        $multipart = array();
        $multipart[] = '--' . $boundary;
        $multipart[] = 'Content-Type: ' . $this->content_type . '; charset=' . $this->charset;
        $multipart[] = 'Content-Transfer-Encoding: 8bit';
        $multipart[] = '';
        $multipart[] = '';
        $multipart[] = $message;
        $multipart[] = '';
        $multipart[] = '--' . $boundary;
        foreach ($this->attaches as $attach) {
            list($file_name, $content_type, $content) = $attach;
            $multipart[] = '--' . $boundary;
            $multipart[] = 'Content-Type: ' . $content_type . ';name="' . $file_name . '"';
            $multipart[] = 'Content-Transfer-Encoding:base64';
            $multipart[] = 'Content-Disposition:attachment;filename="' . $file_name . '"';
            $multipart[] = '';
            $multipart[] = $content;
            $multipart[] = '';
        }
        return mail(
            $this->encode($to),
            $this->encode($subject),
            $this->encode(implode("\n", $multipart)),
            $this->encode(implode("\n", $headers))
        );
    }
}