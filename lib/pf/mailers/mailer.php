<?php
/**
 * @package pFramework
 * @class   mailer
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    December, 2013
 * @version $Id
 *
 * Mail sender
 */

namespace lib\pf\mailers;
use lib\pf\exceptions\exception_runtime;

class mailer
{
    const
        CONTENT_TYPE_TEXT_PLAIN = 'text/plain',
        CONTENT_TYPE_TEXT_HTML  = 'text/html';

    protected
        $content_type = mailer::CONTENT_TYPE_TEXT_PLAIN, // headers Content-Type
        $charset      = PF_ENCODING;                         // charset

    /**
     * Constructor
     *
     * @throws exception_runtime if mail function is not found
     * @throws exception_runtime if extension mbstring is not installed
     */
    public function __construct() {
        if (!function_exists('mail')) {
            throw new exception_runtime('mail', 6);
        }
        if (!extension_loaded('mbstring')) {
            throw new exception_runtime('mbstring', 5);
        }
    }

    /**
     * Sending email
     *
     * @param string $to e-mail recipient in the form
     * user@example.com
     * user@example.com, anotheruser@example.com
     * User <user@example.com>
     * User <user@example.com>, Another User <anotheruser@example.com>
     * @param string $subject subject
     * @param string $message message
     * @param string $from sender e-mail
     * user@example.com, User <user@example.com>
     * @param array $additional_headers additional headers
     * @return bool result
     */
    public function send($to, $subject, $message, $from, array $additional_headers = NULL) {
        // headers
        $headers  = array();
        $headers[] = 'Mime-Version: 1.0';
        $headers[] = 'From: ' . $from;
        $headers[] = 'X-Mailer: PHP/' . phpversion();
        $headers[] = 'Content-Type: ' . $this->content_type . '; charset=' . $this->charset;
        if ($additional_headers !== NULL) {
            $headers = array_merge($headers, $additional_headers);
        }
        return mail(
            $this->encode($to),
            $this->encode($subject),
            $this->encode($this->prepare_message($subject, $message)),
            $this->encode(implode("\n", $headers))
        );
    }

    /**
     * Getting headers Content-type
     *
     * @return string
     */
    public function get_content_type() {
        return $this->content_type;
    }

    /**
     * Setting headers Content-type
     *
     * @param string $content_type Content-Type
     * for example 'text/plain' or 'text/html'
     * @return mailer
     */
    public function set_content_type($content_type) {
        $this->content_type = $content_type;
        return $this;
    }

    /**
     * Getting charset
     *
     * @return string
     */
    public function get_charset() {
        return $this->charset;
    }

    /**
     * Setting charset
     *
     * @param string $charset charset
     * @return mailer
     */
    public function set_charset($charset) {
        $this->charset = strtolower($charset);
        return $this;
    }

    /**
     * Prepare message to sending
     *
     * @param string $subject subject
     * @param string $message message
     * @return string
     */
    protected function prepare_message($subject, $message) {
        $message = wordwrap($message, 70);
        if (($this->content_type == 'text/html') && !preg_match('/^\s*<html>/ui', $message)) {
            $message = "<html>\n<head>\n<title>" . $subject . "</title>\n</head>\n" .
                       "<body>\n" . $message . "\n</body>\n</html>";
        }
        return $message;
    }

    /**
     * Encode to chosen charset
     *
     * @param string $str source string
     * @return string
     */
    protected function encode($str) {
        if ($this->charset == 'koi8-r') {
            // to koi8-r
            return $this->koi8_encode($str);
        }
        // other charset
        return mb_convert_encoding($str, $this->charset, 'auto');
    }

    /**
     * Encode to koi8
     *
     * @param string $str source string
     * @return string
     */
    protected function koi8_encode($str) {
        // to windows-1251
        $str = mb_convert_encoding($str, 'windows-1251', $this->charset);
        // to koi8-r
        return convert_cyr_string($str, 'w', 'k');
    }
}