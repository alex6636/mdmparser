<?php
/**
 * @package pFramework
 * @class   app
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    October, 2013
 * @version $Id
 *
 * Application
 */

namespace lib\pf;
use lib\pf\config\config;
use lib\pf\config\file\config_factory;
use lib\pf\routers\router;
use lib\pf\controllers\controller;
use lib\pf\db\drivers\driver_pdo;
use lib\pf\i18n\i18n;
use lib\pf\i18n\i18n_factory;

class app
{
    const
        // events
        EVENT_BEFORE_CALL_CONTROLLER = 'before_call_controller',
        EVENT_AFTER_CALL_CONTROLLER  = 'after_call_controller',
        EVENT_FAILED_CALL_CONTROLLER = 'failed_call_controller';

    protected
        $config,
        $router,
        $request,
        $controller,
        $db_driver,
        $i18n,
        $benchmark_app,
        $benchmark_controller;

    /**
     * Constructor
     *
     * @param string $config_file_path path to configuration file
     * @param string $url URL, default current
     */
    public function __construct($config_file_path, $url = NULL) {
        if (PF_DEBUG) {
            $this->benchmark_app = profiler::start('app', 'debug');
        }
        $this->config = config_factory::make($config_file_path);
        $parser_url = new parser_url($url ?: $_SERVER['REQUEST_URI']);
        $request = new request($parser_url);
        $router = new router($parser_url->get_parts());
        $this->set_request($request)
             ->set_router($router);
        $pdo_config = $this->config->get(array('db'=>'pdo'));
        if (!empty($pdo_config)) {
            $this->set_db_driver(new driver_pdo($pdo_config));
        }
        $i18n_config = $this->config->get('i18n');
        if (!empty($i18n_config)) {
            $this->set_i18n(i18n_factory::make($i18n_config));
        }
    }

    /**
     * Getting configuration
     *
     * @return config
     */
    public function get_config() {
        return $this->config;
    }

    /**
     * Setting configuration
     *
     * @param config $config
     * @return app
     */
    public function set_config(config $config) {
        $this->config = $config;
        return $this;
    }

    /**
     * Getting router
     *
     * @return router
     */
    public function get_router() {
        return $this->router;
    }

    /**
     * Setting router
     *
     * @param router $router
     * @return app
     */
    public function set_router(router $router) {
        $this->router = $router;
        return $this;
    }

    /**
     * Getting request
     *
     * @return request
     */
    public function get_request() {
        return $this->request;
    }

    /**
     * Setting request
     *
     * @param request $request
     * @return app
     */
    public function set_request(request $request) {
        $this->request = $request;
        return $this;
    }

    /**
     * Getting active controller
     *
     * @return controller
     */
    public function get_controller() {
        return $this->controller;
    }

    /**
     * Setting active controller
     *
     * @param controller $controller
     * @return app
     */
    public function set_controller(controller $controller) {
        $this->controller = $controller;
        return $this;
    }

    /**
     * Getting database driver
     *
     * @return driver_pdo
     */
    public function get_db_driver() {
        return $this->db_driver;
    }

    /**
     * Setting database driver
     *
     * @param driver_pdo $db_driver
     * @return app
     */
    public function set_db_driver(driver_pdo $db_driver) {
        $this->db_driver = $db_driver;
        return $this;
    }

    /**
     * Getting internationalization
     *
     * @return i18n
     */
    public function get_i18n() {
        return $this->i18n;
    }

    /**
     * Setting internationalization
     *
     * @param i18n $i18n
     * @return app
     */
    public function set_i18n(i18n $i18n) {
        $this->i18n = $i18n;
        return $this;
    }

    /**
     * Translation
     *
     * @param string $source_text
     * @return string
     */
    public function translate($source_text) {
        if (!$i18n = $this->get_i18n()) {
            // translator has not been initialized, do not translate
            return $source_text;
        }
        return $i18n->translate($source_text);
    }

    /**
     * Alias for translate
     *
     * @param string $source_text
     * @return string
     */
    public function t($source_text) {
        return $this->translate($source_text);
    }

    /**
     * Getting benchmark application profiling
     *
     * @return int
     */
    public function get_benchmark_app() {
        return $this->benchmark_app;
    }

    /**
     * Setting benchmark application profiling
     *
     * @return int
     */
    public function get_benchmark_controller() {
        return $this->benchmark_controller;
    }

    /**
     * Run an application
     *
     * @return mixed
     */
    public function run() {
        // include buffering to prevent any output
        // including exceptions (errors) that occurred before
        // display the result of a controller action executes
        ob_start();
        if ($search = $this->get_router()->search_route()) {
            list($route, $controller_class) = $search;
            $this->get_request()->set_route($route);
            if (PF_DEBUG) {
                $this->benchmark_controller = profiler::start('controller', 'debug');
            }
            event::trigger(self::EVENT_BEFORE_CALL_CONTROLLER);
            $controller = new $controller_class;
            $this->set_controller($controller);
            if ($this->get_router()->dispatch($route, $controller) !== false) {
                event::trigger(self::EVENT_AFTER_CALL_CONTROLLER);
                return ob_get_clean();
            }
        }
        // 404
        event::trigger(self::EVENT_FAILED_CALL_CONTROLLER);
        $this->get_request()->failed();
        return ob_get_clean();
    }
}