<?php
/**
 * @package pFramework
 * @class   cropper
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    August, 2013
 * @version $Id
 *
 * Resize and cropping image
 */

namespace lib\pf\image;

class cropper extends resizer
{
    /**
     * Changing the image size
     *
     * @param int $thumb_width width
     * @param int $thumb_height height
     * @param string $thumb_path path to modify images
     * default use path to source image
     * @return bool result
     */
    public function crop($thumb_width, $thumb_height, $thumb_path = NULL) {
        $src_width  = $this->src_img->get_width();
        $src_height = $this->src_img->get_height();
        $src_ratio   = $src_width   / $src_height;
        $thumb_ratio = $thumb_width / $thumb_height;
        if ($src_ratio >= $thumb_ratio) {
            $new_height = $thumb_height;
            $new_width  = $src_width / ($src_height / $thumb_height);
        } else {
            $new_width  = $thumb_width;
            $new_height = $src_height / ($src_width / $thumb_width);
        }
        $dst_x = 0 - ($new_width  - $thumb_width);
        $dst_y = 0 - ($new_height - $thumb_height);
        $src_x = $dst_x / 2;
        $src_y = $dst_y / 2;
        return $this->create_image($thumb_path, $new_width, $new_height, $dst_x, $dst_y, $src_x, $src_y);
    }
}