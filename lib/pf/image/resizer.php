<?php
/**
 * @package pFramework
 * @class   resizer
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    August, 2013
 * @version $Id
 *
 * Resize image
 */

namespace lib\pf\image;

class resizer
{
    protected $src_img; // object of source image

    /**
     * Constructor
     *
     * @param image $src_img object of source image
     */
    public function __construct(image $src_img) {
        $this->src_img = $src_img;
    }

    /**
     * Resize
     *
     * @param int $thumb_width width
     * @param int $thumb_height height
     * @param string $thumb_path path
     * default use path to source image
     * @return bool
     */
    public function resize($thumb_width, $thumb_height, $thumb_path = NULL) {
        list($thumb_width, $thumb_height) = $this->get_sizes($thumb_width, $thumb_height);
        return $this->create_image($thumb_path, $thumb_width, $thumb_height);
    }

    /**
     * Getting sizes
     *
     * @param int $thumb_width width
     * @param int $thumb_height height
     * @return array parameters as array(int $thumb_width, int $thumb_height)
     */
    protected function get_sizes($thumb_width, $thumb_height) {
        $src_width  = $this->src_img->get_width();
        $src_height = $this->src_img->get_height();
        if ($thumb_width  > $src_width)  $thumb_width  = $src_width;
        if ($thumb_height > $src_height) $thumb_height = $src_height;
        $src_ratio = $src_width / $src_height;
        $thumb_ratio = $thumb_width / $thumb_height;
        if ($thumb_ratio > $src_ratio) {
            $thumb_width  = $thumb_height * $src_ratio;
        } else {
            $thumb_height = $thumb_width  / $src_ratio;
        }
        return array($thumb_width, $thumb_height);
    }

    /**
     * Creating an image
     *
     * @param string $thumb_path path
     * @param int $thumb_width width
     * @param int $thumb_height height
     * @param int $dst_x indentation along the X axis of the resulting image
     * @param int $dst_y indentation along the Y axis of the resulting image
     * @param int $src_x indent the original image X axis
     * @param int $src_y indent the original image Y axis
     * @return bool result
     */
    protected function create_image(
                                    $thumb_path = NULL,
                                    $thumb_width,
                                    $thumb_height,
                                    $dst_x = 0,
                                    $dst_y = 0,
                                    $src_x = 0,
                                    $src_y = 0
                                   )
    {
        $src_path   = $this->src_img->get_path();
        $src_width  = $this->src_img->get_width();
        $src_height = $this->src_img->get_height();
        if ($thumb_path === NULL) $thumb_path = $src_path;
        $dest_img = imagecreatetruecolor($thumb_width + $dst_x, $thumb_height + $dst_y);
        switch($this->src_img->get_type()) {
            case 1: // gif
                $new_img = imagecreatefromgif($src_path);
                imagecolortransparent($new_img, imagecolorallocate($new_img, 0, 0, 0));
                $resample = imagecopyresampled(
                    $dest_img,
                    $new_img,
                    $dst_x,
                    $dst_y,
                    $src_x,
                    $src_y,
                    $thumb_width,
                    $thumb_height,
                    $src_width,
                    $src_height
                );
                imagegif($dest_img, $thumb_path);
            break;
            case 2: // jpeg
                $new_img = imagecreatefromjpeg($src_path);
                $resample = imagecopyresampled(
                    $dest_img,
                    $new_img,
                    $dst_x,
                    $dst_y,
                    $src_x,
                    $src_y,
                    $thumb_width,
                    $thumb_height,
                    $src_width,
                    $src_height
                );
                imagejpeg($dest_img, $thumb_path, 100);
            break;
            case 3: // png
                $new_img = imagecreatefrompng($src_path);
                imagealphablending($dest_img, false);
                imagesavealpha($dest_img, true);
                $resample = imagecopyresampled(
                    $dest_img,
                    $new_img,
                    $dst_x,
                    $dst_y,
                    $src_x,
                    $src_y,
                    $thumb_width,
                    $thumb_height,
                    $src_width,
                    $src_height
                );
                imagepng($dest_img, $thumb_path);
            break;
            default:
                imagedestroy($dest_img);
                return false;
        }
        imagedestroy($dest_img);
        imagedestroy($new_img);
        return $resample;
    }
}