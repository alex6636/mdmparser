<?php
/**
 * @package pFramework
 * @class   marker
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    August, 2013
 * @version $Id
 *
 * Image marker
 */

namespace lib\pf\image;

class marker
{
    protected
        $src_img,  // object of source image
        $mark_img; // object of image to marking

    /**
     * Constructor
     *
     * @param image $src_img source image
     * @param image $mark_img image-marker
     */
    public function __construct(image $src_img, image $mark_img) {
        $this->src_img  = $src_img;
        $this->mark_img = $mark_img;
    }

    /**
     * Marking
     *
     * @param int $x indentation on the X axis
     * @param int $y indentation on the Y axis
     * @return bool result
     */
    public function mark($x = 0, $y = 0) {
        if (!$mark = $this->mark_img->get_resource()) {
            return false;
        }
        $mark_width = $this->mark_img->get_width();
        $mark_height = $this->mark_img->get_height();
        $src_path = $this->src_img->get_path();
        switch($this->src_img->get_type()) {
            case 1: // gif
                $img = imagecreatefromgif($src_path);
                imagecopy($img, $mark, $x, $y, 0, 0, $mark_width, $mark_height);
                imagegif($img, $src_path);
            break;
            case 2: // jpeg
                $img = imagecreatefromjpeg($src_path);
                imagecopy($img, $mark, $x, $y, 0, 0, $mark_width, $mark_height);
                imagejpeg($img, $src_path, 100);
            break;
            case 3: // png
                $img = imagecreatefrompng($src_path);
                imagealphablending($img, false);
                imagesavealpha($img, true);
                imagecopy($img, $mark, $x, $y, 0, 0, $mark_width, $mark_height);
                imagepng($img, $src_path);
            break;
            default:
                imagedestroy($mark); 
                return false;
        }
        imagedestroy($mark); 
        imagedestroy($img);
        return true;
    }
}