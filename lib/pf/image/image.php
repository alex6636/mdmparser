<?php
/**
 * @package pFramework
 * @class   image
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * Determination of image parameters
 */

namespace lib\pf\image;
use lib\pf\exceptions\exception_runtime;
use lib\pf\exceptions\exception_file;
use lib\pf\arr;

class image
{
    protected
        $path,     // path to image
        $width,    // width
        $height,   // height
        $type,     // type
        $mime,     // mime-type
        $attrs,    // image attributes
        $bits,     // number of bits
        $channels; // number of channels

    /**
     * Constructor
     *
     * @param string $path path to image
     * @param bool $throw_exception throwing an exception
     * @throws \lib\pf\exceptions\exception_runtime if GD extension is not installed
     * or file is not image
     * @throws \lib\pf\exceptions\exception_file if file is not exists
     */
    public function __construct($path, $throw_exception = true) {
        if (extension_loaded('gd')) {
            if (is_file($path)) {
                if ($image = @getimagesize($path)) {
                    $this->path = $path;
                    list($this->width, $this->height, $this->type, $this->attrs) = $image;
                    list($this->mime, $this->bits, $this->channels) =
                        arr::extract($image, array('mime'=>NULL, 'bits'=>NULL, 'channels'=>NULL), false);
                } elseif ($throw_exception) {
                    throw new exception_runtime('Not possible to getting the parameters of the image');
                }
            } elseif ($throw_exception) {
                throw new exception_file($path, 1);
            }
        } elseif ($throw_exception) {
            throw new exception_runtime('GD', 5);
        }
    }

    /**
     * Path to image
     *
     * @return string
     */
    public function get_path() {
        return $this->path;
    }

    /**
     * Width
     *
     * @return int width in pixels
     */
    public function get_width() {
        return $this->width;
    }

    /**
     * Height
     *
     * @return int height in pixels
     */
    public function get_height() {
        return $this->height;
    }

    /**
     * Numeric type image
     *
     * 1 => gif
     * 2 => jpeg
     * 3 => png
     * 6 => bmp etc
     * @return int
     */
    public function get_type() {
        return $this->type;
    }

    /**
     * String type image
     *
     * Determined based on mime-type
     * image/gif  => gif
     * image/jpeg => jpeg
     * image/png  => png
     * image/bmp  => bmp etc
     *
     * @return string
     */
    public function get_string_type() {
        return substr(strrchr($this->mime, '/'), 1);
    }

    /**
     * Dynamic image based on the current
     *
     * To save memory, the created image
     * need to delete using the method imagedestroy
     *
     * @return resource image or false,
     * if the image is incorrect type
     */
    public function get_resource() {
        // check image type
        switch ($this->get_type()) {
            // gif
            case 1: return imagecreatefromgif($this->path);
            // jpeg
            case 2: return imagecreatefromjpeg($this->path);
            // png
            case 3: return imagecreatefrompng($this->path);
            // bmp
            case 6: return imagecreatefromwbmp($this->path);
        }
        // image type is invalid
        return false;
    }

    /**
     * Mime-type
     *
     * @return string
     */
    public function get_mime() {
        return $this->mime;
    }

    /**
     * Attributes
     *
     * @return string as 'width="xxx" height="yyy"'
     */
    public function get_attrs() {
        return $this->attrs;
    }

    /**
     * Number of bits
     *
     * @return int
     */
    public function get_bits() {
        return $this->bits;
    }

    /**
     * Number of channels
     *
     * @return int
     */
    public function get_channels() {
        return $this->channels;
    }
}