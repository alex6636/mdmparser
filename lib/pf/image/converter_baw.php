<?php
/**
 * @package pFramework
 * @class   converter_baw
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    August, 2013
 * @version $Id
 *
 * Converter color image to black and white
 */

namespace lib\pf\image;
use lib\pf\exceptions\exception_argument;

class converter_baw
{
    protected $src_img; // source image

    /**
     * Constructor
     *
     * @param image $src_img source image
     */
    public function __construct(image $src_img) {
        $this->src_img = $src_img;
    }

    /**
     * Convert
     *
     * @param string $convert_path path to modify image
     * default use path to source image
     * @param array $rgb_gray percent each gray color RGB palette
     * array(0.3, 0.3, 0.3) - 30% red, 30% green, 30% blue for jpeg and gif only
     * @throws exception_argument if an invalid format RGB
     * @return bool conversion result
     */
    public function convert($convert_path = NULL, array $rgb_gray = array(0.3, 0.3, 0.3)) {
        // check palette
        if (count($rgb_gray) !== 3) {
            throw new exception_argument('Invalid parameters RGB format of image');
        }
        $src_path = $this->src_img->get_path();
        if ($convert_path === NULL) $convert_path = $src_path;
        // check type
        switch($this->src_img->get_type()) {
            case 1: // gif
                $new_img = imagecreatefromgif($src_path);
                imagecolortransparent($new_img, imagecolorallocate($new_img, 255, 255, 255));
                $this->set_image_gray_colors($new_img, $rgb_gray);
                imagegif($new_img, $convert_path);
            break;
            case 2: // jpeg
                $new_img = imagecreatefromjpeg($src_path);
                $this->set_image_gray_colors($new_img, $rgb_gray);
                imagejpeg($new_img, $convert_path);
            break;
            case 3: // png
                $new_img = imagecreatefrompng($src_path);
                imagealphablending($new_img, false);
                imagesavealpha($new_img, true);
                imagefilter($new_img, IMG_FILTER_GRAYSCALE);
                imagepng($new_img, $convert_path);
            break;
            default:
                // invalid image type
                return false;
        }
        imagedestroy($new_img);
        return true;
    }

    /**
     * Setting gray colors image (jpg, gif)
     *
     * @param resource $new_img image resource identifier
     * @param array $rgb_gray percent each gray color RGB palette
     * array(0.3, 0.3, 0.3) - 30% red, 30% green, 30% blue for jpeg and gif only
     */
    protected function set_image_gray_colors($new_img, array $rgb_gray) {
        // determine the total number of colors
        if (!$colors_total = imagecolorstotal($new_img)) {
            // default 256
            $colors_total = 256;
        }
        // convert the image to a paletted
        imagetruecolortopalette($new_img, false, $colors_total);
        // set colors
        for ($i = 0; $i < $colors_total; $i ++) {
            $rgb = imagecolorsforindex($new_img, $i);
            $gray = round(($rgb_gray[0] * $rgb['red']) + ($rgb_gray[1] * $rgb['green']) + ($rgb_gray[2] * $rgb['blue']));
            imagecolorset($new_img, $i, $gray, $gray, $gray);
        }
    }
}