<?php
/**
 * @package pFramework
 * @class   uploader
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    August, 2012
 * @version $Id
 *
 * Class for uploading images
 */

namespace lib\pf\image;
use lib\pf\file;
use lib\pf\str;

class uploader
{
   /** @var image $tmp_image temporary image */
    protected $tmp_image;

    protected
        $upload_dir,               // directory to upload
        $max_size_mb         = 5,  // maximum size, mb
        $file_name_length    = 10, // file name length
        $allowed_types       = array('jpeg', 'gif', 'png'), // allowed image types
        $copy_if_upload_fail = false; // copying a file if move_uploaded_file does not work

    /**
     * Constructor
     *
     * @param string $upload_dir directory to upload
     * @param image $tmp_image temporary image, for example
     * new image($_FILES['img']['tmp_name'], false)
     */
    public function __construct($upload_dir, image $tmp_image) {
        $this->set_upload_dir($upload_dir)
             ->set_tmp_image($tmp_image);
    }

    /**
     * Getting upload directory
     *
     * @return string
     */
    public function get_upload_dir() {
        return $this->upload_dir;
    }

    /**
     * Setting upload directory
     *
     * @param string $upload_dir directory
     * @return uploader
     */
    public function set_upload_dir($upload_dir) {
        file::create_writable_dir($upload_dir);
        $this->upload_dir = str::append_last_slash($upload_dir);
        return $this;
    }

    /**
     * Getting temporary image
     *
     * @return image
     */
    public function get_tmp_image() {
        return $this->tmp_image;
    }

    /**
     * Setting temporary image
     *
     * @param image $tmp_image temporary image
     * @return uploader
     */
    public function set_tmp_image(image $tmp_image) {
        $this->tmp_image = $tmp_image;
        return $this;
    }

    /**
     * Getting maximum size, mb
     *
     * @return float
     */
    public function get_max_size_mb() {
        return $this->max_size_mb;
    }

    /**
     * Setting maximum size, mb
     *
     * @param float $max_size_mb
     * @return uploader
     */
    public function set_max_size_mb($max_size_mb) {
        $this->max_size_mb = $max_size_mb;
        return $this;
    }

    /**
     * Getting the length of the file name
     *
     * @return int
     */
    public function get_file_name_length() {
        return $this->file_name_length;
    }

    /**
     * Setting the length of the file name
     *
     * @param int $length length (no more than 32 characters)
     * @return uploader
     */
    public function set_file_name_length($length) {
        $this->file_name_length = $length;
        return $this;
    }

    /**
     * Getting allowed types of images
     *
     * @return array
     */
    public function get_allowed_types() {
        return $this->allowed_types;
    }

    /**
     * Setting allowed types of images
     *
     * @param array $allowed_types allowed types,
     * corresponding image::get_string_type
     * @return uploader
     */
    public function set_allowed_types(array $allowed_types) {
        $this->allowed_types = $allowed_types;
        return $this;
    }

    /**
     * Copying a file if move_uploaded_file does not work
     *
     * @param bool $flag copying flag (false - safe mode)
     * @return uploader
     */
    public function copy_if_upload_fail($flag) {
        $this->copy_if_upload_fail = $flag;
        return $this;
    }

    /**
     * Upload
     *
     * @return string path to the uploaded file
     * or false, if failed upload
     */
    public function upload() {
        if ($this->is_allowed_image()) {
            $tmp_path  = $this->tmp_image->get_path();
            $dest_path = $this->get_upload_dest_path();
            if (move_uploaded_file($tmp_path, $dest_path)) {
                return $dest_path;
            } elseif ($this->copy_if_upload_fail) {
                if (copy($tmp_path, $dest_path)) {
                    return $dest_path;
                }
            }
        }
        return false;
    }

    /**
     * Validation image
     *
     * @return bool
     */
    public function is_allowed_image() {
        return (
            // type
            in_array($this->tmp_image->get_string_type(), $this->allowed_types) &&
            // size
            (file::size_mb($this->tmp_image->get_path()) <= $this->max_size_mb)
        );
    }

    /**
     * Getting upload destination path
     *
     * @return bool
     */
    protected function get_upload_dest_path() {
        return $this->upload_dir .
               file::unique_name(
                   $this->file_name_length,
                   image_type_to_extension($this->tmp_image->get_type())
               );
    }
}