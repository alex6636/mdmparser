<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo PF_ENCODING; ?>"/>
        <meta name="robots" content="noindex, nofollow"/>
        <title><?php echo \lib\pf\pf::get_title() ?></title>
        <style>
            * {
                margin: 0;
                padding: 0;
                outline: 0;
            }
            html, body {
                width: 100%;
                height: 100%;
            }
            body {
                background-color: #e6e6e6;
                color: #333;
                font-family: Arial, sans-serif;
                font-size: 0.8em;
            }
            .powered {
                position: absolute;
                right: 1em;
                bottom: 10px;
            }
            h1 {
                padding-bottom: 0.4em;
                color: #1b7fcc;
                font-size: 2.3em;
                line-height: 1em;
                text-shadow: #ccc 1px 1px 1px, #e6e6e6 0 0 1px;
            }
            h1, h3, h4 {
                text-align: center;
            }
            a {
                color: #1b7fcc;
                text-decoration: underline;
            }
            a:hover {
                text-decoration: none;
            }
            p {
                color: #333;
                font-size: 1.4em;
                line-height: 1.3em;
            }
                #main {
                    position: relative;
                    width: 100%;
                    height: 100%;
                }
                    #container {
                        position: relative;
                        margin: 0 auto;
                        width: 70%;
                        min-width: 600px;
                        max-width: 1000px;
                        height: 100%;
                        display: block;
                    }
                        #center {
                            position: relative;
                            width: 100%;
                            box-shadow: 0 0 10px #333;
                            background-color: #fff;
                        }
                            #content {
                                padding: 1em 1em 40px 1em;
                                height: 100%;
                            }
                                #content .page {
                                    width: 100%;
                                    height: 100%;
                                    overflow: hidden;
                                }
                                    #content .page .tracer {
                                        width: 100%;
                                    }
                                        #content .page .tracer .hide {
                                            display: none;
                                        }
                                        #content .page .tracer .arrow {
                                            float: right;
                                            color: #999;
                                        }
                                        #content .page .tracer .errline {
                                            padding: 10px 0;
                                            color: #999;
                                        }
                                        #content .page .tracer .section {
                                            margin-top: 0.4em;
                                            padding: 0.1em 0;
                                            width: 100%;
                                            display: block;
                                            background-color: #e6e6e6;
                                            cursor: pointer;
                                            text-align: center;
                                            text-decoration: none;
                                            color: #333;
                                            font: 1.5em/1.5em Arial, sans-serif;
                                        }
                                        #content .page .tracer .active {
                                            background-color: #1b7fcc;
                                            color: #fff;
                                        }
                                            #content .page .tracer .section .arrow {
                                                padding-right: 10px;
                                                font: 1em/1.2em Arial, sans-serif;
                                            }
                                            #content .page .tracer .active .arrow {
                                                color: #fff;
                                            }
                                        #content .page .tracer .array {
                                            position: relative;
                                            margin-bottom: 10px;
                                            width: 100%;
                                            height: auto;
                                            background: #e6e6e6;
                                            overflow-x: auto;
                                            text-align: left;
                                            font: 12px/12px Arial, sans-serif;
                                        }
                                            #content .page .tracer .array table {
                                                width: 100%;
                                                border-collapse: collapse;
                                                border: 0;
                                            }
                                                #content .page .tracer .array table td {
                                                    padding: 10px;
                                                    color: #333;
                                                    font-size: 12px;
                                                }
                                        #content .page .tracer .path {
                                            margin-bottom: 10px;
                                            padding: 0 1.5em 4px 1.5em;
                                            background: #e6e6e6;
                                            text-align: left;
                                        }
                                            #content .page .tracer .path .script {
                                                display: block;
                                                background: none !important;
                                                cursor: pointer;
                                                color: #1b7fcc;
                                                text-decoration: none;
                                                font: 1.7em/2em Arial, sans-serif;
                                            }
                                                #content .page .tracer .path .script .arrow {
                                                    color: #999;
                                                    font-size: 0.7em;
                                                }
                                            #content .page .tracer .path .active,
                                            #content .page .tracer .path .script:hover {
                                                text-decoration: underline;
                                            }
                                            #content .page .tracer .path .code_box {
                                                margin: 0 auto 10px;
                                                width: 100%;
                                                overflow: hidden;
                                                border: 1px solid #e6e6e6;
                                                text-align: left;
                                            }
                                                #content .page .tracer .path .code_box .code {
                                                    position: relative;
                                                    width: 100%;
                                                    display: inline-block;
                                                    overflow-x: auto;
                                                }
                                                    #content .page .tracer .path .code_box .code .file {
                                                        position: absolute;
                                                        top: 0;
                                                        left: 33px;
                                                        padding: 0 10px;
                                                        max-height: 32px;
                                                        overflow: hidden;
                                                        border: 1px solid #ccc;
                                                        border-bottom: 0;
                                                        background-color: #fff;
                                                        color: #666;
                                                        font: 1em/2.5em Arial, sans-serif;
                                                    }
                                                    #content .page .tracer .path .code_box .code table {
                                                        margin-top: 32px;
                                                        width: 100%;
                                                        overflow: hidden;
                                                        border: 1px solid #ccc;
                                                        border-collapse: collapse;
                                                        background-color: #fff;
                                                    }
                                                        #content .page .tracer .path .code_box .code table td {
                                                            width: 100%;
                                                        }
                                                            #content .page .tracer .path .code_box .code table td .main {
                                                                position: relative;
                                                                padding-right: 10px;
                                                                width: 100%;
                                                            }
                                                                #content .page .tracer .path .code_box .code table td .main .lines {
                                                                    position: relative;
                                                                }
                                                                    #content .page .tracer .path .code_box .code table td .main .lines .err_line {
                                                                        position: absolute;
                                                                        top: 0;
                                                                        left: 0;
                                                                        width: 100%;
                                                                        height: 17px;
                                                                        display: block;
                                                                        z-index: 1;
                                                                        background-color: #ffff33;
                                                                        opacity: 0.2;
                                                                    }
                                                                    #content .page .tracer .path .code_box .code table td .main .lines .line {
                                                                        position: relative;
                                                                        margin: 0;
                                                                        padding: 2px 0;
                                                                        width: 100%;
                                                                        height: 12px;
                                                                        display: block;
                                                                        overflow: hidden;
                                                                        white-space: nowrap;
                                                                        font: 12px/12px Arial, sans-serif;
                                                                    }
                                                                    #content .page .tracer .path .code_box .code table td .main .lines .line .num_bg,
                                                                    #content .page .tracer .path .code_box .code table td .main .lines .line .num {
                                                                        position: absolute;
                                                                        padding: 2px 0 0 2px;
                                                                        top: 0;
                                                                        left: 0;
                                                                        width: 30px;
                                                                        height: 17px;
                                                                    }
                                                                    #content .page .tracer .path .code_box .code table td .main .lines .line .num_bg {
                                                                        z-index: 0;
                                                                        border-right: 1px solid #e6e6e6;
                                                                        background-color: #eee;
                                                                    }
                                                                    #content .page .tracer .path .code_box .code table td .main .lines .line .num {
                                                                        z-index: 1;
                                                                        line-height: 12px;
                                                                        color: #999;
                                                                    }
                                                                #content .page .tracer .path .code_box .code table td .main .highlight {
                                                                    position: absolute;
                                                                    top: 0;
                                                                    left: 45px;
                                                                    font-size: 13px;
                                                                }
                                                                    #content .page .tracer .path .code_box .code table td .main .highlight * {
                                                                        line-height: 12px;
                                                                    }
                                                                #content .page .tracer .path .code_box .code table td .main code {
                                                                    position: relative;
                                                                    z-index: 1;
                                                                }
                                            #content .page .tracer .path .args_box {
                                                position: relative;
                                                padding: 10px 0;

                                                max-height: 400px;
                                                display: block;
                                                background: #e6e6e6;
                                                overflow: auto;
                                                border-bottom: 1px solid #CCCCCC;
                                                font: 12px/12px Arial, sans-serif;
                                            }
                                            #content .page .tracer .path .args_box:last-child {
                                                border: 0;
                                            }
                                                #content .page .tracer .path .args_box table {
                                                    width: 100%;
                                                    border-collapse: collapse;
                                                    border: 0;
                                                    font-size: 12px;
                                                }
                                                    #content .page .tracer .path .args_box span {
                                                        color: #0000bb;
                                                    }
                                                    #content .page .tracer .path .args_box table .open {
                                                        padding-left: 10px;
                                                        vertical-align: top;
                                                        white-space: nowrap;
                                                        text-align: left;
                                                    }
                                                        #content .page .tracer .path .args_box table .open .method {
                                                            color: #333;
                                                        }
                                                    #content .page .tracer .path .args_box table .args {
                                                        width: 100%;
                                                        vertical-align: top;
                                                        text-align: left;
                                                    }
                                                        #content .page .tracer .path .args_box table .args div {
                                                            padding: 15px 10px 0 0;
                                                            color: #555;
                                                        }
                                                    #content .page .tracer .path .args_box table .close {
                                                        vertical-align: top;
                                                        text-align: right;
                                                    }
                                        #content .page .tracer .phpinfo {
                                            position: relative;
                                            margin-bottom: 10px;
                                            width: 100%;
                                            height: auto;
                                            background: #eee;
                                            text-align: left;
                                            font: 12px/12px Arial, sans-serif;
                                        }
                                            #phpinfo_iframe {
                                                width: 100%;
                                                height: 600px;
                                                border: 0;
                                            }
        </style>
    </head>
    <body>
        <div id="main">
            <div id="container">
                <div id="center">
                    <div id="content">
                        <div class="page">
                            <div class="tracer">
                                <?php echo $this->get_all_items_html(); ?>
                            </div>
                        </div>
                    </div>
                    <?php echo lib\pf\pf::powered(); ?>
                </div>
            </div>
        </div>
        <script>
            (function() {
                var tracer = {
                    dom: {
                        getByClass: function(class_list, parent_node) {
                            parent_node = parent_node || document;
                            var list = parent_node.getElementsByTagName('*'),
                                classes = class_list.split(/\s*,\s*/),
                                result = [];
                            for (var i = 0, list_length = list.length; i < list_length; i ++) {
                                for (var j = 0, classes_length = classes.length; j < classes_length; j ++) {
                                    if (tracer.dom.hasClass(list[i], classes[j])) {
                                        result.push(list[i]);
                                        break;
                                    }
                                }
                            }
                            return result;
                        },
                        hasClass: function(el, class_name) {
                            if (typeof(el.className) === 'undefined') {
                                return false;
                            }
                            return (el.className.match(new RegExp('(\\s|^)' + class_name + '(\\s|$)'))) ? true : false;
                        },
                        addClass: function(el, class_name) {
                            el.className = el.className ?
                                           (tracer.dom.hasClass(el, class_name) ? el.className : el.className + ' ' + class_name) :
                                           class_name;
                        },
                        removeClass: function(el, class_name) {
                            el.className = el.className.replace(' ' + class_name, '');
                            el.className = el.className.replace(class_name + ' ', '');
                            el.className = el.className.replace(class_name, '');
                        },
                        toggleClass: function(el, class_name) {
                            if (tracer.dom.hasClass(el, class_name)) {
                                tracer.dom.removeClass(el, class_name);
                            } else {
                                tracer.dom.addClass(el, class_name);
                            }
                        },
                        nextNode: function(node) {
                            do node = node.nextSibling;
                            while (node && node.nodeType != 1);
                            return node;
                        }
                    },
                    event: {
                        add: function(el, type, fn) {
                            if (el.addEventListener) {
                                el.addEventListener(type, fn, false);
                            } else if (el.attachEvent) {
                                el.attachEvent('on' + type, fn);
                            } else {
                                el['on' + type] = fn;
                            }
                        }
                    },
                    toggle: function() {
                        var toggle_class = 'hide',
                            toggles = tracer.dom.getByClass('section, script');
                        for (var i = 0, toggles_length = toggles.length; i < toggles_length; i ++) {
                            (function() {
                                var el = toggles[i], toggle = tracer.dom.nextNode(el);
                                if ((i === 0) || (i === 1)) {
                                    tracer.dom.addClass(el, 'active');
                                } else {
                                    tracer.dom.addClass(toggle, 'hide');
                                }
                                var arrow = document.createElement('span');
                                arrow.className = 'arrow';
                                arrow.innerHTML = tracer.dom.hasClass(el, 'active') ? '&uarr;' : '&darr;';
                                el.appendChild(arrow);
                                tracer.event.add(el, 'click', function(event) {
                                    tracer.dom.toggleClass(el, 'active');
                                    if (tracer.dom.hasClass(toggle, toggle_class)) {
                                        tracer.dom.removeClass(toggle, toggle_class);
                                        arrow.innerHTML = '&uarr;';
                                    } else {
                                        tracer.dom.addClass(toggle, toggle_class);
                                        arrow.innerHTML = '&darr;';
                                    }
                                    event.preventDefault();
                                    return false;
                                });
                            })();
                        }
                        return tracer;
                    },
                    phpinfo: function() {
                        var template = document.getElementById('phpinfo_template'),
                            iframe = document.getElementById('phpinfo_iframe');
                        var doc = iframe.contentWindow.document;
                        doc.open();
                        doc.write(template.innerHTML);
                        doc.close();
                        return tracer;
                    }
                };
                tracer.toggle().phpinfo();
            })();
        </script>
    </body>
</html>