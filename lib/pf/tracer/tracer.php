<?php
/**
 * @package pFramework
 * @class   tracer
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Tracer
 */

namespace lib\pf\tracer;
use lib\pf\pf;
use lib\pf\arr;
use lib\pf\file;
use lib\pf\profiler;
use lib\pf\event;

abstract class tracer
{
    protected
        $error_type,
        $message,
        $code,
        $file,
        $line,
        $trace = array(),
        $code_lines = PF_LIB_TRACER_CODE_LINES,
        $code_highlight = array(
            PF_LIB_TRACER_CODE_HL_STRING,
            PF_LIB_TRACER_CODE_HL_COMMENT,
            PF_LIB_TRACER_CODE_HL_KEYWORD,
            PF_LIB_TRACER_CODE_HL_DEFAULT,
            PF_LIB_TRACER_CODE_HL_HTML,
        ),
        $code_highlight_default = array();

    /**
     * Constructor
     *
     * @param string $error_type error/exception type
     * @param string $message message
     * @param int $code code
     * @param string $file file
     * @param int $line line
     * @param array $trace trace
     */
    public function __construct($error_type, $message, $code, $file, $line, array $trace = array()) {
        $this->error_type = $error_type;
        $this->message = $message;
        $this->code = $code;
        $this->file = $file;
        $this->line = $line;
        $this->trace = $trace;
    }

    /**
     * Display the result of trace
     *
     * @return string
     */
    public function __toString() {
        if ($app = pf::get_app())  {
            if (($request = $app->get_request()) && $request->is_ajax()) {
                // ajax-request, display ajax-error
                return $this->get_ajax_error_text();
            }
        }
        if (headers_sent()) {
            // display simple error
            return $this->get_simple_error_html();
        }
        // initialize the default highlight
        $this->code_highlight_default = array(
            ini_get('highlight.string'),
            ini_get('highlight.comment'),
            ini_get('highlight.keyword'),
            ini_get('highlight.default'),
            ini_get('highlight.html'),
        );
        return $this->get_tracer_result_html();
    }

    /**
     * Getting message about Ajax-error
     *
     * @return string
     */
    protected function get_ajax_error_text() {
        return '[' . $this->code . '] ' . $this->message .
               "\nFile: " . $this->file . "\nLine: " . $this->line;
    }

    /**
     * Getting simple message about error
     *
     * @return string
     */
    protected function get_simple_error_html() {
        ob_start();
        include 'html/simple_error.php';
        return ob_get_clean();
    }

    /**
     * Tracing in HTML form
     *
     * @return string
     */
    protected function get_tracer_result_html() {
        ob_start();
        include 'html/tracer.php';
        return ob_get_clean();
    }

    /**
     * All items (trace, files, classes etc.) in HTML form
     *
     * @return string
     */
    protected function get_all_items_html() {
        $include_path_arr = file::include_path_to_array();
        $included_files = get_included_files();
        $loaded_classes = pf::get_loader()->get_classes();
        $profiling_stats = profiler::get_stats();
        $defined_constants = get_defined_constants(true);
        $defined_functions = get_defined_functions();
        return (
            '<h1>' . $this->error_type .'</h1>' .
            '<h3>[' . $this->code. '] ' . $this->message . '</h3>' .
            '<h4 class="errline">' . $this->file . ' (Line ' . $this->line . ')</h4>' .
        // Stack trace
            $this->get_section_html('Stack trace', count($this->trace)) .
            $this->get_path_html() .
        // Included files
            $this->get_section_html('Included files', count($included_files)) .
            $this->get_array_html($included_files) .
        // Loaded classes
            $this->get_section_html('Loaded classes', count($loaded_classes)) .
            $this->get_array_html($loaded_classes) .
        // Include path
            $this->get_section_html('Include path', count($include_path_arr)) .
            $this->get_array_html($include_path_arr) .
        // Event listeners
            $this->get_section_html('Event listeners', count(event::get_listeners())) .
            $this->get_array_html(event::get_listeners()) .
        // Profiling
            $this->get_section_html('Profiling', count($profiling_stats)) .
            $this->get_array_html($profiling_stats) .
        // APP
            $this->get_app_html() .
        // defined constants
            $this->get_section_html('User constants', count($defined_constants['user'])) .
            $this->get_array_html($defined_constants['user']) .
            $this->get_section_html('All constants', count(get_defined_constants())) .
            $this->get_array_html(get_defined_constants()) .
        // defined functions
            $this->get_section_html('User functions', count($defined_functions['user'])) .
            $this->get_array_html($defined_functions['user']) .
            $this->get_section_html('All functions', count($defined_functions['internal'])) .
            $this->get_array_html($defined_functions['internal']) .
        // GET
            $this->get_section_html('GET', count($_GET)) .
            $this->get_array_html($_GET) .
        // POST
            $this->get_section_html('POST', count($_POST)) .
            $this->get_array_html($_POST) .
        // COOKIE
            $this->get_section_html('COOKIE', count($_COOKIE)) .
            $this->get_array_html($_COOKIE) .
        // SESSION
            $this->get_section_html('SESSION', isset($_SESSION) ? count($_SESSION) : 0) .
            $this->get_array_html(isset($_SESSION) ? $_SESSION : array()) .
        // ENV
            $this->get_section_html('ENV', count($_ENV)) .
            $this->get_array_html($_ENV) .
        // SERVER
            $this->get_section_html('SERVER', count($_SERVER)) .
            $this->get_array_html($_SERVER) .
        // PHP Info
            $this->get_section_html('PHP Info', phpversion()) .
            $this->get_phpinfo()
        );
    }

    /**
     * Result application trace in the form of HTML
     *
     * @return string $html
     */
    protected function get_app_html() {
        $html = '';
        if ($app = pf::get_app()) {
            if (($request = $app->get_request()) && $request->is_dispatched()) {
                $params = $request->get_route()->get_all_params();
                $html .= $this->get_section_html('APP request', count($params));
                $html .= $this->get_array_html($params);
            }
        }
        return $html;
    }

    /**
     * HTML section
     *
     * @param mixed $title1 string
     * @param mixed $title2 string
     * @return string
     */
    protected function get_section_html($title1, $title2) {
        return '<div class="section">' . $title1 . ' (' . $title2 . ')</div>';
    }

    /**
     * HTML array
     *
     * @param mixed $arr
     * @return string $html
     */
    protected function get_array_html($arr) {
        $html  = '<div class="array">';
        $html .= '<table><tr><td>' .
                 '<pre>' . htmlspecialchars(print_r($arr, true)) . '</pre>' .
                 '</td></tr></table>';
        $html .= '</div>';
        return $html;
    }

    /**
     * Trace the path starting from the input file
     * and ending with the place where the exception was thrown (generated error)
     *
     * @return string $html
     */
    protected function get_path_html() {
        $html = '<div class="path">';
        $trace_html = array();
        $t_args = array();
        foreach ($this->trace as $t) {
            if (!isset($t['file'])) {
                $t_args[] = $t;
                continue;
            }
            // title
            $part_html  = $this->get_path_head($t['file'], $t['line']);
            $part_html .= '<div class="code_box">';
            // code
            $part_html .= $this->get_path_code($t['file'], $t['line'], $this->code_lines);
            //$type
            $part_html .= $this->get_path_args(
                arr::get($t, 'class', ''),
                arr::get($t, 'function', ''),
                arr::get($t, 'type', ''),
                arr::get($t, 'args', array())
            );
            if (!empty($t_args)) {
                foreach ($t_args as $t_arg) {
                    $part_html .= $this->get_path_args(
                        arr::get($t_arg, 'class', ''),
                        arr::get($t_arg, 'function', ''),
                        arr::get($t_arg, 'type', ''),
                        arr::get($t_arg, 'args', array())
                    );
                }
                $t_args = array();
            }
            $part_html .= '</div>';
            $trace_html[] = $part_html;
        }
        $html .= implode($trace_html);
        $html .= '</div>';
        return $html;
    }

    /**
     * Title of the traced file part
     *
     * @param string $file_name
     * @param int $line
     * @return string $html
     */
    protected function get_path_head($file_name, $line) {
        $html  = '<div class="script">' . basename($file_name) . ' (' . $line . ')</div>';
        return $html;
    }

    /**
     * PHP-info
     *
     * @return string $html
     */
    protected function get_phpinfo() {
        ob_start();
        phpinfo();
        $phpinfo = ob_get_clean();
        $html  = '<div class="phpinfo">';
        $html .= '<script id="phpinfo_template" type="text/template">' . $phpinfo . '</script>';
        $html .= '<iframe id="phpinfo_iframe" src="about:blank"></iframe>';
        $html .= '</div>';
        return $html;
    }

    /**
     * Code of the tracer file part
     *
     * @param string $file_path
     * @param int $line
     * @param int $length the number of rows on both sides
     * relative to the line that caused the exception
     * @return string $html
     */
    protected function get_path_code($file_path, $line, $length = 10) {
        $html = '<div class="code"><span class="file">' . $file_path . '</span><table><tr><td><div class="main">';
        if ($highlight_file = $this->get_highlight_file($file_path)) {
            // determine the number of the first row
            $start = $line - $length - 1;
            if ($start < 0) {
                $start = 0;
            }
            $count_lines = count($highlight_file);
            if ((($length * 2) + $start) > $count_lines) {
                $length = $count_lines - $start;
            } else {
                $length *= 2;
                $length ++;
            }
            $html_lines = '<div class="lines">';
            $html_highlight = '<div class="highlight">';
            $highlight_lines = array_slice($highlight_file, $start, $length, true);
            foreach ($highlight_lines as $num=>$highlight_line) {
                ++ $num;
                $html_lines .= '<div class="line" style="color:' . PF_LIB_TRACER_CODE_HL_KEYWORD . '">';
                if ($num == $line)  {
                    $html_lines .= '<div class="err_line"></div>';
                }
                $html_lines .= '<div class="num_bg"></div><div class="num">' . $num . '</div>';
                $html_lines .= '</div>';
                $html_highlight .= $highlight_line;
                $html_highlight .= '<br/>';
            }
            $html_highlight .= '</div>';
            $html_lines .= $html_highlight;
            $html_lines .= '</div>';
            $html .= $html_lines;
        }
        $html .= '</div></td></tr></table></div>';
        return $html;
    }

    /**
     * Getting the highlighted file lines
     *
     * @param string $file_path
     * @return array or false
     */
    protected function get_highlight_file($file_path) {
        if (is_file($file_path)) {
            if ($highlight_file = highlight_file($file_path, true)) {
                // replace the default highlight
                $highlight_file = str_replace(
                    $this->code_highlight_default,
                    $this->code_highlight,
                    $highlight_file
                );
                return preg_split('/(<br\s*\/?\s*>)/u', $highlight_file);
            }
        }
        return false;
    }

    /**
     * Arguments of the traced file part
     *
     * @param string $class
     * @param string $method
     * @param string $type call type (-> or ::)
     * @param array $args
     * @return string $html
     */
    protected function get_path_args($class, $method, $type, array $args) {
        $html = '';
        if (!empty($args)) {
            $print_args = arr::to_string($args);
            // remove the Array at the beginning and at the end of output arguments
            $print_args = preg_replace('/^<pre>array\s+\(/ui', '', $print_args);
            $print_args = preg_replace('/\s+\)\s+<\/pre>$/ui', '', $print_args);
            $print_args = '<pre>' . htmlspecialchars($print_args) . '</pre>';
            $html  = '<div class="args_box">';
            $html .= '<table><tr>';
            $html .= '<td class="open"><span>'. $class . '</span><span>' . $type . '</span>' .
                     '<span class="method">' . $method . '</span><span>(</span></td>';
            $html .= '<td class="args"><div>' .  $print_args . '</div></td>';
            $html .= '</tr><tr>';
            $html .= '<td class="close"><span>)</span></td>';
            $html .= '</tr></table>';
            $html .= '</div>';
        }
        return $html;
    }
}