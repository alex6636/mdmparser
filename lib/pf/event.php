<?php
/**
 * @package pFramework
 * @class   event
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    December, 2013
 * @version $Id
 *
 * Event manager
 */

namespace lib\pf;
use lib\pf\exceptions\exception_runtime;

class event
{
    protected static $events = array();

    /**
     * Adding handler
     *
     * @param string $event event
     * @param mixed callback
     * @param int $priority
     * 1 - handler is added to the beginning, 0 - to the end
     */
    public static function on($event, $callback, $priority = 1) {
        if (!static::has($event)) {
            static::$events[$event] = array();
        }
        if ($priority) {
            array_unshift(static::$events[$event], $callback);
        } else {
            static::$events[$event][] = $callback;
        }
    }

    /**
     * Deleting handler
     *
     * @param string $event event
     * @param mixed callback
     * if not transferring - will remove all handlers for the specified event
     */
    public static function off($event, $callback = NULL) {
        if (static::has($event)) {
            if ($callback !== NULL) {
                if (($key = array_search($callback, static::$events[$event])) !== false) {
                    unset(static::$events[$event][$key]);
                    if (empty(static::$events[$event])) {
                        unset(static::$events[$event]);
                    }
                }
            } else {
                unset(static::$events[$event]);
            }
        }
    }

    /**
     * Calling event
     *
     * @param string $event event
     * @param array $args arguments
     * @throws exception_runtime if unable to call handler
     */
    public static function trigger($event, array $args = NULL) {
        if (static::has($event)) {
            foreach (static::$events[$event] as $callback) {
                if (!is_callable($callback)) {
                    throw new exception_runtime(var_export($callback, true), 3);
                }
                if ($args !== NULL) {
                    call_user_func_array($callback, $args);
                } else {
                    call_user_func($callback);
                }
            }
        }
    }

    /**
     * Checking the existence of event handlers
     *
     * @param string $event event name
     * @return bool
     */
    public static function has($event) {
        return array_key_exists($event, static::$events);
    }

    /**
     * Getting all the event listeners
     *
     * @param string $event event name
     * @return array
     */
     public static function get_listeners($event = NULL) {
         if ($event !== NULL) {
             return static::has($event) ? static::$events[$event] : array();
         }
         return static::$events;
     }
}