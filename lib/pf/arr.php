<?php
/**
 * @package pFramework
 * @class   arr
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Class for working with array
 */

namespace lib\pf;

class arr
{
    /**
     * Array dump
     */
    public static function dump() {
        $dump_args = func_get_args();
        if (!empty($dump_args)) {
            foreach ($dump_args as $dump_arg) {
                echo '<pre>';
                print_r($dump_arg);
                echo '</pre>';
            }
        }
    }

    /**
     * Array to string
     *
     * @param mixed $arr
     * @return string
     */
    public static function to_string($arr) {
        return '<pre>' . print_r($arr, true) . '</pre>';
    }

    /**
     * Checking whether an associative array
     *
     * @param array $arr
     * @return bool
     */
    public static function is_assoc(array $arr) {
        $keys = array_keys($arr);
        foreach ($keys as $key) {
            if (is_string($key)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checking whether an numeric array
     *
     * @param array $arr
     * @return bool
     */
    public static function is_numeric(array $arr) {
        foreach ($arr as $val) {
            if (!is_numeric($val)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checking whether the array key numbers
     *
     * @param array $arr
     * @return bool
     */
    public static function is_numeric_keys(array $arr) {
        $keys = array_keys($arr);
        foreach ($keys as $key) {
            if (!is_numeric($key)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checking whether a multidimensional array
     *
     * @param array $arr
     * @return bool
     */
    public static function is_multidim_array(array $arr) {
        foreach ($arr as $a) {
            if (is_array($a)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Getting the value from an array by key
     *
     * @param array $arr
     * @param mixed $key
     * @param string $default the default value for a non-existent element
     * @return mixed
     */
    public static function get(array $arr, $key, $default = NULL) {
        return array_key_exists($key, $arr) ? $arr[$key] : $default;
    }

    /**
     * Getting a key value from the array and all subarrays
     *
     * lib\pf\arr::get_all(array('Alex', array('unknown'=>'Unknown'), 'Dmitry'), 'unknown', 'not found')
     * =>'Unknown'
     *
     * @param array $arr
     * @param mixed $key
     * @param string $default the default value for a non-existent element
     * @return mixed
     */
    public static function get_all(array $arr, $key, $default = NULL) {
        if (array_key_exists($key, $arr)) {
            return $arr[$key];
        }
        foreach ($arr as $val) {
            if (is_array($val)) {
                return static::get_all($val, $key, $default);
            }
        }
        return $default;
    }

    /**
     * Extracting array elements with the specified key
     *
     * lib\pf\arr::extract(array('name'=>'John', 'surname'=>'Doe'), array('name'=>NULL, 'surname'=>NULL, 'age'=>25, 'weight'=>70))
     * => array('name'=>'John', 'surname'=>'Doe', 'age'=>25, 'weight'=>70)
     *
     * @param array $arr
     * @param array $keys_defaults
     * @param bool $preserve_keys
     * @return array $result
     */
    public static function extract(array $arr, array $keys_defaults, $preserve_keys = true) {
        $result = array();
        if ($preserve_keys) {
            foreach ($keys_defaults as $key=>$default) {
                $result[$key] = static::get($arr, $key, $default);
            }
        } else {
            foreach ($keys_defaults as $key=>$default) {
                $result[] = static::get($arr, $key, $default);
            }
        }
        return $result;
    }

    /**
     * Recursive inheritance arrays
     *
     * lib\pf\arr::extend(array(array(1,2,3), array(4, 5, 6)), array(array(0, 0, 0, 4, 5, 6), array(0, 0, 0, 7, 8, 9)))
     * => array(array(1, 2, 3, 4, 5, 6), array(4, 5, 6, 7, 8, 9))
     *
     * @param array $extend
     * @param array $parent
     * @return array $extend
     */
    public static function extend(array $extend, array $parent) {
        foreach ($parent as $key=>$val) {
            if (is_array($val)) {
                if (array_key_exists($key, $extend)) {
                    $extend[$key] = static::extend($extend[$key], $val);
                } else {
                    $extend[$key] = $val;
                }
            } else {
                if (!array_key_exists($key, $extend)) {
                    $extend[$key] = $val;
                }
            }
        }
        return $extend;
    }

    /**
     * Getting the column with the specified key in the two-dimensional array
     *
     * lib\pf\arr::column(array(array('name'=>'John', 'surname'=>'Doe'), array('name'=>'Alex', 'surname'=>'Sergey')), 'name')
     * =>array('John', 'Alex')
     *
     * @param array $arr
     * @param mixed $key
     * @param string $default the default value for a non-existent element
     * @return array $result
     */
    public static function column(array $arr, $key, $default = NULL) {
        $result = array();
        foreach ($arr as $val) {
            $result[] = array_key_exists($key, $val) ? $val[$key] : $default;
        }
        return $result;
    }

    /**
     * Getting some columns with the specified keys in the two-dimensional array
     *
     * lib\pf\arr::columns(
     * array(
     *     array('name'=>'Dmitry', 'surname'=>'Ivanov'),
     *     array('name'=>'Alex', 'surname'=>'Sergey', 'years'=>25),
     *     array('name'=>'Shamil', 'years'=>35),
     * ),
     * array('name'=>'John', 'surname'=>'Zaurov'))
     * =>array(
     *     array('name'=>'Dmitry', 'surname'=>'Ivanov'),
     *     array('name'=>'Alex', 'surname'=>'Sergey'),
     *     array('name'=>'Shamil', 'surname'=>'Zaurov'),
     * ),
     *
     * @param array $arr
     * @param array $keys_defaults
     * @return array $result
     */
    public static function columns(array $arr, array $keys_defaults) {
        $result = array();
        foreach ($arr as $key=>$val) {
            foreach ($keys_defaults as $key_default=>$default) {
                $result[$key][$key_default] = array_key_exists($key_default, $val) ? $val[$key_default] : $default;
            }
        }
        return $result;
    }

    /**
     * Deleting the column with the specified key in the two-dimensional array
     *
     * @param array $arr
     * @param mixed $key
     * @return array
     */
    public static function column_delete(array $arr, $key) {
        array_walk(
            $arr,
            create_function('&$a, $k, $f', 'unset($a[$f]);'),
            $key
        );
        return $arr;
    }

    /**
     * Getting the column with the specified key in a multidimensional array
     *
     * lib\pf\arr::column_all(
     *     array(
     *         array('name'=>'John Top', array('name'=>'John', 'surname'=>'Doe'), array('name'=>'John Inner', 'surname'=>'Doe'))
     *         array('name'=>'Alex', 'surname'=>'Smith', array('name'=>'Alex Inner', 'surname'=>'Smith'))
     *     ),
     *     'name',
     *     $column
     * )
     * => $column = array('John Top', 'John', 'John Inner', 'Alex', 'Alex Inner')
     *
     * @param array $arr
     * @param mixed $key
     * @param array &$result
     */
    public static function column_all(array $arr, $key, array &$result) {
        foreach ($arr as $k=>$val) {
            if (is_array($val)) {
                static::column_all($val, $key, $result);
            } else {
                if ($k === $key) {
                    $result[] = $val;
                }
            }
        }
    }

    /**
     * Application of a mathematical operation to a column array
     *
     * @param array $arr
     * @param mixed $key
     * @param string $operation operation, '+' returns the sum of the elements of the array with the key $key
     * @return mixed
     */
    public static function column_operation(array $arr, $key, $operation) {
        return array_reduce(
            $arr,
            create_function(
                '$result, $next',
                '$result ' . $operation . '=$next["' . $key . '"]; return $result;'
            )
       );
    }

    /**
     * Sorting an array by column
     *
     * @param array $arr associative array, each nested subarray must contain key $key
     * @param mixed $key
     * @param string $sort_type 'asc' or 'desc'
     * @return array $arr
     */
    public static function sort_by_column(array $arr, $key, $sort_type = 'asc') {
        $operation = ($sort_type == 'asc') ? '>' : '<';
        usort(
            $arr,
            create_function(
                '$a,$b',
                'return $a["' . $key . '"] ' . $operation . ' $b["' . $key . '"];'
            )
        );
        return $arr;
    }

    /**
     * Sorting an array by keys array
     *
     * lib\pf\arr::sort_by_keys(array('name'=>'Alex', 'surname'=>'Sergey', 'years'=>25), array('years', 'name', 'surname'))
     * =>array('years'=>25, 'name'=>'Alex', 'surname'=>'Sergey')
     *
     * @param array $arr associative array
     * @param array $keys
     * @return array $arr
     */
    public static function sort_by_keys(array $arr, array $keys) {
        return array_merge(array_flip($keys), $arr);
    }

    /**
     * Grouping array by two columns
     *
     * $result = array();
     * $arr = array(
     *     array('id'=>1, 'name'=>'Alex',   'parent_id'=>2),
     *     array('id'=>2, 'name'=>'Dmitry', 'parent_id'=>0),
     *     array('id'=>4, 'name'=>'Dan',    'parent_id'=>5),
     *     array('id'=>5, 'name'=>'John',   'parent_id'=>7),
     *     array('id'=>6, 'name'=>'Ashley', 'parent_id'=>4),
     *     array('id'=>7, 'name'=>'Pete',   'parent_id'=>0),
     * );
     * lib\pf\arr::group_columns($arr, $result, 'id', 'parent_id')
     * =>array(
     *     array('id'=>2, 'name'=>'Dmitry', 'parent_id'=>0),
     *     array('id'=>1, 'name'=>'Alex',   'parent_id'=>2),
     *     array('id'=>7, 'name'=>'Pete',   'parent_id'=>0),
     *     array('id'=>5, 'name'=>'John',   'parent_id'=>7),
     *     array('id'=>4, 'name'=>'Dan',    'parent_id'=>5),
     *     array('id'=>6, 'name'=>'Ashley', 'parent_id'=>4),
     * )
     *
     * @param array $array
     * @param array &$result
     * @param mixed $key
     * @param string $relation_key
     * @param int $start parent uid to start the search
     */
    public static function group_columns(array $array, array &$result, $key, $relation_key, $start = 0) {
        foreach ($array as $k=>$val) {
            if ($val[$relation_key] === $start) {
                $result[] = $val;
                unset($array[$k]);
                static::group_columns($array, $result, $key, $relation_key, $val[$key]);
            }
        }
    }

    /**
     * Recursive search value in depth in a multidimensional array
     *
     * lib\pf\arr::depth(
     *     array('names'=>array('surnames'=>array('one'=>'Alex', 'two'=>'Dmitry')))
     *     array('names', 'surnames', 'two')
     * ), => 'Dmitry'
     *
     * @param array $arr
     * @param array $keys
     * @param string $default
     * @return mixed
     */
    public static function depth(array $arr, array $keys, $default = NULL) {
        if (($key = array_shift($keys)) !== false) {
            if (array_key_exists($key, $arr)) {
                if (!count($keys)) {
                    return $arr[$key];
                }
                if (is_array($arr[$key])) {
                    return static::depth($arr[$key], $keys, $default);
                }
            }
        }
        return $default;
    }

    /**
     * Remove spaces at the edges of items in the array
     *
     * @param array $arr
     * @return array
     */
    public static function trim(array $arr) {
        return array_map('trim', $arr);
    }

    /**
     * Filtering array
     *
     * lib\pf\arr::filter(array(1, 2, 'x', 3, 'x'), 'x') => array(1, 2, 3)
     *
     * @param array $arr
     * @param mixed $val value of the filtered element
     * @param bool $preserve_keys
     * @return array
     */
    public static function filter(array $arr, $val, $preserve_keys = true) {
        return $preserve_keys ? array_diff($arr, array($val)) : array_values(array_diff($arr, array($val)));
    }

    /**
     * Conversion elements of the array to an integer
     *
     * @param array $arr
     * @return array
     */
    public static function intval(array $arr) {
        return array_map('intval', $arr);
    }

    /**
     * Recursive array map
     *
     * @param mixed $callback
     * @param array $arr
     * @return array $arr
     */
    public static function map($callback, array $arr) {
        foreach ($arr as $key=>$val) {
            $arr[$key] = is_array($val) ? static::map($callback, $val) : call_user_func($callback, $val);
        }
        return $arr;
    }

    /**
     * Recursive array map with some callbacks
     *
     * @param array $callbacks
     * @param array $arr
     * @return array $arr
     */
    public static function map_some_callbacks(array $callbacks, array $arr) {
        foreach ($callbacks as $callback) {
            $arr = static::map($callback, $arr);
        }
        return $arr;
    }

    /**
     * Extraction an item from an array by key
     *
     * @param array &$arr
     * @param mixed $key
     * @return mixed $result value of the item to extraction item or false
     */
    public static function shift_key(array &$arr, $key) {
        if (array_key_exists($key, $arr)) {
            $result = $arr[$key];
            unset($arr[$key]);
            return $result;
        }
        return false;
    }

    /**
     * Adding branch to array
     *
     * $arr = array();
     * lib\pf\arr::push_branch($arr, array('peoples', 'young', 'surname'), 'Alex')
     * => array('peoples'=>array('young'=>array('surname'=>'Alex')))
     *
     * @param array &$arr
     * @param array $keys
     * @param mixed $deepmost_val
     * @param bool $replace
     */
    public static function push_branch(array &$arr, array $keys, $deepmost_val = NULL, $replace = false) {
        if (($key = array_shift($keys)) !== NULL) {
            $key_exists = array_key_exists($key, $arr);
            if (count($keys)) {
                if (!$key_exists || ($key_exists && $replace)) {
                    $arr[$key] = array();
                }
                static::push_branch($arr[$key], $keys, $deepmost_val, $replace);
            } else {
                if (!$key_exists || ($key_exists && $replace)) {
                    $arr[$key] = $deepmost_val;
                }
            }
        }
    }

    /**
     * Converting an object to array
     *
     * @param object $obj
     * @return array
     */
    public static function obj2arr($obj) {
        if (!is_object($obj) && !is_array($obj)) {
            return $obj;
        }
        return array_map(array('static', 'obj2arr'), (array) $obj);
    }
}