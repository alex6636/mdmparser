<?php
/**
 * @package pFramework
 * @class   router
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    December, 2013
 * @version $Id
 *
 * Router
 */

namespace lib\pf\routers;
use lib\pf\routers\route\route;
use lib\pf\exceptions\exception_argument;
use lib\pf\controllers\controller;
use lib\pf\str;

class router
{
    protected
        $url_parts = array(),
        $routes    = array(),
        $controller_class_prefix  = 'controller_',
        $controller_action_prefix = 'action_';

    /**
     * Constructor
     *
     * @param array $url_parts
     * @return router
     */
    public function __construct(array $url_parts) {
        $this->set_url_parts($url_parts);
    }

    /**
     * Getting URL parts
     *
     * @return array
     */
    public function get_url_parts() {
        return $this->url_parts;
    }

    /**
     * Setting URL parts
     *
     * @param array $url_parts
     * @return router
     */
    public function set_url_parts(array $url_parts) {
        $this->url_parts = $url_parts;
        return $this;
    }

    /**
     * Adding route
     *
     * @param string $name
     * @param route $route
     * @param mixed $namespace controller namespace
     * string if one, array if several
     * @throws exception_argument if route is not exists
     * @return router
     */
    public function add_route($name, route $route, $namespace) {
        if (array_key_exists($name, $this->routes)) {
            throw new exception_argument('Route with name {' . $name . '} is already exists');
        }
        $this->routes[$name] = array($route, $namespace);
        return $this;
    }

    /**
     * Getting route
     *
     * @param string $name
     * @throws exception_argument if route is not exists
     * @return route
     */
    public function get_route($name) {
        if (!array_key_exists($name, $this->routes)) {
            throw new exception_argument('Route with name {' . $name . '} is not exists');
        }
        return $this->routes[$name][0];
    }

    /**
     * Search route
     *
     * @return mixed array as
     * array(0=>route $route, 1=>string $class, 'name'=>string $router_route_name)
     * or false, if the route was not found
     */
    public function search_route() {
        if (!empty($this->routes)) {
            foreach ($this->routes as $name=>$rt) {
                /** @var route $route */
                list($route, $namespace) = $rt;
                // find route parameters for the query, and the controller
                if (
                    $route->set_url_parts($this->url_parts)->match_route() &&
                    ($class = $this->exists_controller($route, (array) $namespace))
                ) {
                    return array($route, $class, 'name'=>$name);
                }
            }
        }
        // route was now found
        return false;
    }

    /**
     * Send request to controller
     *
     * @param route $route
     * @param controller $controller
     * @return mixed result
     */
    public function dispatch(route $route, controller $controller) {
        $action = $this->controller_action_prefix . $route->get_action();
        // action
        if (!is_callable(array($controller, $action))) {
            return false;
        }
        // call controller action
        return call_user_func_array(
            array($controller, $action),
            $route->get_params()
        );
    }

    /**
     * Build route with arbitrary dynamic parameters by name of the route
     *
     * @param string $name
     * @param array $params array as array('action'=>'action_key', 'p1'=>'12345')
     * @return string
     */
    public function assemble_route($name, array $params = NULL) {
        return $this->get_route($name)->assemble_route($params);
    }

    /**
     * Getting controller class prefix
     *
     * return string
     */
    public function get_controller_class_prefix() {
        return $this->controller_class_prefix;
    }

    /**
     * Setting controller class prefix
     *
     * @param string $prefix
     * @return router
     */
    public function set_controller_class_prefix($prefix) {
        $this->controller_class_prefix = $prefix;
        return $this;
    }

    /**
     * Getting controller action prefix
     *
     * return string
     */
    public function get_controller_action_prefix() {
        return $this->controller_action_prefix;
    }

    /**
     * Setting controller action prefix
     *
     * @param string $prefix
     * @return router
     */
    public function set_controller_action_prefix($prefix) {
        $this->controller_action_prefix = $prefix;
        return $this;
    }

    /**
     * Verify the existence the controller by route
     *
     * @param route $route
     * @param array $namespaces
     * @return mixed string $class controller class name or false
     */
    protected function exists_controller(route $route, array $namespaces) {
        $controller = $this->controller_class_prefix . $route->get_controller();
        $action     = $this->controller_action_prefix . $route->get_action();
        foreach ($namespaces as $ns) {
            $ns = str::append_first_slash(str::append_last_slash($ns, '\\'), '\\');
            $class = $ns . $controller;
            if (class_exists($class) && method_exists($class, $action)) {
                return $class;
            }
        }
        return false;
    }
}