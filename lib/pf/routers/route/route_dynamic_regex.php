<?php
/**
 * @package pFramework
 * @class   route_dynamic_regex
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * Route with dynamic parameters and regular expressions
 *
 * new route_dynamic_regex(
 *     ':controller/:action/:p1/:p2',
 *     array(
 *         'controller'=>'index',
 *         'action'    =>'index',
 *         'p1'        =>'123',
 *         'p2'        =>'abc'
 *     ),
 *     array(
 *         'p1'=>'\d+',
 *         'p2'=>'[a-z]+'
 *     )
 * )
 */

namespace lib\pf\routers\route;

class route_dynamic_regex extends route_dynamic
{
    protected $params_patterns = array();

    /**
     * Constructor
     *
     * @param string $route
     * @param array $defaults
     * @param array $params_patterns as
     * array('param1'=>'\d+', 'param2'=>'\w+') without line start and line end characters
     * applies to all parameters, except for the basic
     */
    public function __construct($route, array $defaults, array $params_patterns) {
        parent::__construct($route, $defaults);
        $this->set_params_patterns($params_patterns);
    }

    /**
     * Assemble route
     *
     * @param array $params as array('action'=>'action', 'p1'=>'12345')
     * all not passed parameters will be replaced with the default parameters
     * @return string
     */
    public function assemble_route(array $params = NULL) {
        $route_url_parts = array();
        foreach ($this->route_parts as $route_part) {
            if ($part_key = $this->is_dynamic_route_part($route_part)) {
                $part = '';
                if (array_key_exists($part_key, (array) $params)) {
                    // use default parameter
                    $part = $params[$part_key];
                }
                elseif (array_key_exists($part_key, $this->defaults)) {
                    $part = $this->defaults[$part_key];
                }
                // check pattern
                if (array_key_exists($part_key, $this->params_patterns)) {
                    // check for validity
                    if (!$this->match_param($this->params_patterns[$part_key], $part)) {
                        $part = '';
                    }
                }
                if ($part) {
                    $route_url_parts[] = $part;
                }
            } else {
                $route_url_parts[] = $route_part;
            }
        }
        return !empty($route_url_parts) ? '/' . implode('/', $route_url_parts) : '/';
    }

    /**
     * Getting patterns parameters
     *
     * @return array
     */
    public function get_params_patterns() {
        return $this->params_patterns;
    }

    /**
     * Setting patterns parameters
     *
     * @param array $params_patterns as array('param1'=>'\d+', 'param2'=>'\w+')
     * without line start and line end characters
     * @return route_dynamic_regex
     */
    public function set_params_patterns(array $params_patterns) {
        $this->params_patterns = $params_patterns;
        return $this;
    }

    /**
     * Comparing and getting parameters by URL parts
     *
     * @return mixed array as array('controller', 'action', array(['param1', 'param2'])) or false
     */
    protected function match_by_url_parts() {
        if (!$this->check_route_length()) {
            return false;
        }
        if (!$this->check_static_parts()) {
            return false;
        }
        $others_params = $this->get_others_params();
        if (!$this->check_others_params($others_params)) {
            return false;
        }
        list($controller, $action) = $this->get_required_params();
        return array($controller, $action, $others_params);
    }

    /**
     * Check additional parameters for compliance
     * Regular expression patterns
     *
     * @param array $others_params
     * @return bool
     */
    protected function check_others_params(array $others_params) {
        if (!empty($others_params)) {
            foreach ($others_params as $param_key=>$param) {
                if (array_key_exists($param_key, $this->params_patterns)) {
                    if (!$this->match_param($this->params_patterns[$param_key], $param)) {
                        // parameter value is invalid
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Checking the validity of a regular expression
     *
     * @param string $pattern pattern
     * @param string $param parameter value
     * @return bool
     */
    protected function match_param($pattern, $param) {
        return (bool) preg_match('/^' . $pattern . '$/ui', $param);
    }
}