<?php
/**
 * @package pFramework
 * @class   route_static
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    August, 2013
 * @version $Id
 *
 * Static route
 *
 * new route_static(
 *     static/part1/partx',
 *     array(
 *         'controller'=>'index',
 *         'action'    =>'index'
 *     )
 * )
 */

namespace lib\pf\routers\route;

class route_static extends route
{
    /**
     * Assemble route
     *
     * @param array $params
     * does not make sense for this type of route!
     * @return string
     */
    public function assemble_route(array $params = NULL) {
        return '/' . $this->route;
    }

    /**
     * Comparing and getting parameters by URL parts
     *
     * @return mixed as array('controller', 'action', array(['param1', 'param2'])) or false
     */
    protected function match_by_url_parts() {
        if ($this->url_parts === $this->route_parts) {
            return array(
                $this->defaults['controller'],
                $this->defaults['action'],
                $this->get_defaults_others_params()
            );
        }
        return false;
    }

    /**
     * Parameters for the default route
     * except controller and action
     *
     * @return array
     */
    protected function get_defaults_others_params() {
        return array_diff_key(
            $this->defaults,
            array_flip($this->required_defaults)
        );
    }
}