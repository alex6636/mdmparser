<?php
/**
 * @package pFramework
 * @class   route_regex
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * Route with regular expressions
 *
 * new route_regex(
 *     'archive/(\d{2})-(\d{2})-(\d{4})/([a-z]+)\.html',
 *     array(
 *         'controller'=>'archive',
 *         'action'    =>'category',
 *     ),
 *     array(
 *         'day'     =>1,
 *         'month'   =>2,
 *         'year'    =>3,
 *         'category'=>4,
 *     )
 * )
 */

namespace lib\pf\routers\route;
use lib\pf\arr;
use lib\pf\exceptions\exception_argument;

class route_regex extends route
{
    protected $patterns_numbers = array();

    /**
     * Constructor
     *
     * @param string $route
     * @param array $defaults default parameters, including the controller and action
     * @param array $patterns_numbers patterns of regular expressions as
     * array(
     *      'param_name'=>int bracket number of regular expression
     *      [,...]
     * )
     */
    public function __construct($route, array $defaults, array $patterns_numbers) {
        parent::__construct($route, $defaults);
        $this->patterns_numbers = $patterns_numbers;
    }

    /**
     * Assemble route
     *
     * @param array $params as
     * array(
     *      'param_name'=>mixed value
     *      [,...]
     * )
     * @return string
     */
    public function assemble_route(array $params = NULL) {
        $route_url_parts = array();
        $pattern_num = 0;
        foreach ($this->route_parts as $route_part) {
            if ($this->is_regex_route_part($route_part)) {
                if ($patterns = $this->get_regex_route_part_patterns($route_part)) {
                    foreach ($patterns as $pattern) {
                        if (($param_name = array_search(++ $pattern_num, $this->patterns_numbers)) !== false) {
                            if ($part = arr::get((array) $params, $param_name, '')) {
                                if (!$this->match_param($pattern, $part)) {
                                    $part = '';
                                }
                            }
                            $route_part = substr_replace($route_part, $part, strpos($route_part, $pattern), strlen($pattern));
                        }
                    }
                }
                $route_part = str_replace('\\', '', $route_part);
                $route_url_parts[] = $route_part;
            } else {
                $route_url_parts[] = $route_part;
            }
        }
        return !empty($route_url_parts) ? '/' . implode('/', $route_url_parts) : '/';
    }

    /**
     * Checking match route by request
     *
     * @return bool
     */
    public function match_route() {
        if ($result = $this->match_by_url_parts()) {
            list($controller, $action, $params) = $result;
            $this->set_controller($controller)
                 ->set_action($action)
                 ->set_params($params);
            return true;
        }
        return false;
    }

    /**
     * Comparing and getting parameters by URL parts
     *
     * @return mixed array as array('controller', 'action', array(['param1', 'param2'])) or false
     */
    protected function match_by_url_parts() {
        if (!$this->check_route_length()) {
            return false;
        }
        if (($regex_params_values = $this->check_all_parts()) === false) {
            return false;
        }
        return array(
            $this->defaults['controller'],
            $this->defaults['action'],
            $this->assemble_regex_params($regex_params_values)
        );
    }

    /**
     * Check of compliance of route length
     *
     * @return bool
     */
    protected function check_route_length() {
        return (count($this->url_parts) === count($this->route_parts));
    }

    /**
     * Check of compliance all route parts
     *
     * @return mixed an array of parts of the route,
     * Is a regular expression (can be empty)
     * or false, if the route does not match the request
     */
    protected function check_all_parts() {
        $regex_params_values = array();
        foreach ($this->route_parts as $part_key=>$route_part) {
            if ($this->is_regex_route_part($route_part)) {
                if (!$matches = $this->match_param($route_part, $this->url_parts[$part_key])) {
                    return false;
                }
                for ($i = 1; $i < count($matches); $i ++) {
                    $regex_params_values[] = $matches[$i];
                }
            } elseif ($route_part !== $this->url_parts[$part_key]) {
                return false;
            }
        }
        return $regex_params_values;
    }

    /**
     * Checking whether the part of the route regular expression
     *
     * @param string $route_part
     * @return bool
     */
    protected function is_regex_route_part($route_part) {
        return (bool) (bool) preg_match('/\(.*\)/ui', $route_part);
    }

    /**
     * Assemble regular expression parameters
     *
     * @param array $regex_params_values
     * @throws exception_argument if you can not find a pattern with number
     * corresponding to the number of parameter values
     * @return array $regex_params
     */
    protected function assemble_regex_params(array $regex_params_values) {
        $regex_params = array();
        if (!empty($regex_params_values)) {
            for ($i = 0; $i < count($regex_params_values); $i ++) {
                if (($param_name = array_search($i + 1, $this->patterns_numbers)) === false) {
                    throw new exception_argument('Not found pattern with number ' . ($i + 1));
                }
                $regex_params[$param_name] = $regex_params_values[$i];
            }
        }
        return $regex_params;
    }

    /**
     * Getting route part patterns
     *
     * @param string $route_part
     * @return mixed array or false
     */
    protected function get_regex_route_part_patterns($route_part) {
        preg_match_all('/\([^\)]*\)/ui', $route_part, $patterns);
        return arr::get($patterns, 0, false);
    }

    /**
     * Check the validity of the parameter
     * by regular expression
     *
     * @param string $pattern pattern
     * @param string $param parameter value
     * @return mixed array matches if the match was successful or false
     */
    protected function match_param($pattern, $param) {
        if (preg_match('/^' . $pattern . '$/ui', $param, $matches)) {
            return $matches;
        }
        return false;
    }
}