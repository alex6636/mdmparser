<?php
/**
 * @package pFramework
 * @class   route
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    November, 2013
 * @version $Id
 *
 * Base route
 */

namespace lib\pf\routers\route;
use lib\pf\str;
use lib\pf\exceptions\exception_argument;

abstract class route
{
    protected
        $route,
        $route_parts       = array(),
        $url_parts         = array(),
        $defaults          = array(),
        $required_defaults = array('controller', 'action'),
        $controller,
        $action,
        $params            = array();

    /**
     * Constructor
     *
     * @param string $route
     * @param array $defaults default parameters, including the controller and action
     */
    public function __construct($route, array $defaults) {
        $this->set_route($route)
             ->set_defaults($defaults);
    }

    /**
     * Assemble route
     *
     * @param array $params
     * array('action'=>'action', 'p1'=>'12345')
     * @return string route
     */
    abstract public function assemble_route(array $params = NULL);

    /**
     * Checking match route by request
     *
     * @return bool result
     */
    public function match_route() {
        if ($result = $this->match_by_url_parts()) {
            list($controller, $action, $params) = $result;
            $this->set_controller($controller)
                 ->set_action($action)
                 ->set_params($params);
            return true;
        }
        return false;
    }

    /**
     * Getting route
     *
     * @return string
     */
    public function get_route() {
        return $this->route;
    }

    /**
     * Setting route
     *
     * @param string $route
     * @return route
     */
    public function set_route($route) {
        $this->route = str::trim_slashes($route);
        $this->route_parts = explode('/', $this->route);
        return $this;
    }

    /**
     * Getting route parts
     *
     * @return array
     */
    public function get_route_parts() {
        return $this->route_parts;
    }

    /**
     * Getting URL parts
     *
     * @return array
     */
    public function get_url_parts() {
        return $this->url_parts;
    }

    /**
     * Setting URL parts
     *
     * @param array $url_parts
     * @return route
     */
    public function set_url_parts(array $url_parts) {
        $this->url_parts = $url_parts;
        return $this;
    }

    /**
     * Getting controller
     *
     * @return string
     */
    public function get_controller() {
        return $this->controller; 
    }

    /**
     * Setting controller
     *
     * @param string $controller
     * @return route
     */
    public function set_controller($controller) {
        $this->controller = $controller;
        return $this;
    }

    /**
     * Getting action
     *
     * @return string
     */
    public function get_action() {
        return $this->action;
    }

    /**
     * Setting action
     *
     * @param string $action
     * @return route
     */
    public function set_action($action) {
        $this->action = $action;
        return $this;
    }

    /**
     * Getting parameters
     *
     * @return array
     */
    public function get_params() {
        return $this->params;
    }

    /**
     * Setting parameters
     *
     * @param array $params
     * @return route
     */
    public function set_params(array $params) {
        $this->params = $params;
        return $this;
    }

    /**
     * Getting all parameters, with controller and action
     *
     * @return array
     */
    public function get_all_params() {
        return array_merge(
            array('controller'=>$this->controller, 'action'=>$this->action),
            $this->params
        );
    }

    /**
     * Getting default parameters
     *
     * @return array
     */
    public function get_defaults() {
        return $this->defaults;
    }

    /**
     * Setting default parameters
     *
     * @param array $defaults default parameters including the controller and action
     * @throws exception_argument if not set required default parameter
     * @return route
     */
    public function set_defaults(array $defaults) {
        foreach ($this->required_defaults as $param) {
            if (!array_key_exists('controller', $defaults)) {
                throw new exception_argument('Route {' . htmlspecialchars($this->route) . '} ' .
                                             'must contain required default param {' . $param . '}');
            }
        }
        $this->defaults = $defaults;
        return $this;
    }

    /**
     * Compare and getting parameters by URL parts
     *
     * Returns the the controller, action and additional parameters
     *
     * @return mixed array
     * array('controller', 'action', 'params'=array()) or false
     */
    abstract protected function match_by_url_parts();
}