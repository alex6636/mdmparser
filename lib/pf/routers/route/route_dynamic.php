<?php
/**
 * @package pFramework
 * @class   route_dynamic
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * Route with dynamic parameters
 *
 * new route_dynamic(
 *     ':controller/:action/:p1/:p2',
 *     array(
 *         'controller'=>'index',
 *         'action'    =>'index',
 *         'p1'        =>'123'
 *     )
 * )
 */

namespace lib\pf\routers\route;
use lib\pf\arr;

class route_dynamic extends route
{
    protected 
        $route_static_length,
        $required_dynamic_params = array(':controller', ':action');

    /**
     * Constructor
     *
     * @param string $route
     * @param array $defaults
     */
    public function __construct($route, array $defaults) {
        $prepare_route = $this->prepare_route($route);
        parent::__construct($prepare_route, $defaults);
        $this->set_route_static_length(($route === $prepare_route) ? true : false);
    }

    /**
     * Assemble route
     *
     * @param array $params as array('action'=>'action', 'p1'=>'12345')
     * all not passed parameters will be replaced with the default parameters
     * @return string
     */
    public function assemble_route(array $params = NULL) {
        $route_url_parts = array();
        foreach ($this->route_parts as $route_part) {
            if ($part_key = $this->is_dynamic_route_part($route_part)) {
                $part = '';
                if (array_key_exists($part_key, (array) $params)) {
                    // use default parameter
                   $part = $params[$part_key];
                }
                elseif (array_key_exists($part_key, $this->defaults)) {
                    $part = $this->defaults[$part_key];
                }
                if ($part) {
                    $route_url_parts[] = $part;
                }
            } else {
                $route_url_parts[] = $route_part;
            }
        }
        return !empty($route_url_parts) ? '/' . implode('/', $route_url_parts) : '/';
    }

    /**
     * Preparing roue
     *
     * @param string $route
     * @return string
     */
    protected function prepare_route($route) {
        return preg_replace('/\*+$/', '', $route);
    }

    /**
     * Setting route static length
     *
     * @param bool $static_length
     * @return route_dynamic
     */
    protected function set_route_static_length($static_length) {
        $this->route_static_length = $static_length;
        return $this;
    }

    /**
     * Comparing and getting parameters by URL parts
     *
     * @return mixed array as
     * array('controller', 'action', array(['param1', 'param2'])) or false
     */
    protected function match_by_url_parts() {
        if (!$this->check_route_length()) {
            return false;
        }
        if (!$this->check_static_parts()) {
            return false;
        }
        list($controller, $action) = $this->get_required_params();
        return array($controller, $action, $this->get_others_params());
    }

    /**
     * Checking route length
     *
     * @return bool
     */
    protected function check_route_length() {
        if ($this->route_static_length) {
            return (count($this->url_parts) <= count($this->route_parts));
        }
        return true;
    }

    /**
     * Checking static URL parts and route
     *
     * @return bool
     */
    protected function check_static_parts() {
        if (count(arr::filter($this->route_parts, ''))) {
            foreach ($this->route_parts as $part_key=>$route_part) {
                if (!$this->is_dynamic_route_part($route_part)) {
                    if (
                        !array_key_exists($part_key, $this->url_parts) ||
                        ($route_part !== $this->url_parts[$part_key])
                    ) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Getting dynamic parameter value
     *
     * @param string $route_part
     * @return mixed string value or false
     */
    protected function get_dynamic_param_value($route_part) {
        $part_key = $this->get_route_part_key($route_part);
        if ($part_key !== false) {
            if (array_key_exists($part_key, $this->url_parts)) {
                return $this->url_parts[$part_key];
            }
        }
        if ($part_key = $this->is_dynamic_route_part($route_part)) {
            if (array_key_exists($part_key, $this->defaults)) {
                return $this->defaults[$part_key];
            }
        }
        return false;
    }

    /**
     * Getting the values ​​of the basic parameters :controller and :action
     *
     * @return array $params
     */
    protected function get_required_params() {
        $params = array();
        foreach ($this->required_dynamic_params as $param) {
            $params[] = $this->get_dynamic_param_value($param);
        }
        return $params;
    }

    /**
     * Getting the values ​​of more parameters
     *
     * @return array $others_params
     */
    protected function get_others_params() {
        $others_params = array();
        $route_others_parts = $this->get_route_others_parts();
        if (!empty($route_others_parts)) {
            foreach ($route_others_parts as $route_part) {
                if ($part_key = $this->is_dynamic_route_part($route_part)) {
                    if ($param = $this->get_dynamic_param_value($route_part)) {
                        $others_params[$part_key] = $param;
                    }
                }
            }
        }
        return $others_params;
    }

    /**
     * Getting a key value from route part
     *
     * @param string $route_part
     * @return mixed key value or false
     */
    protected function get_route_part_key($route_part) {
        return array_search($route_part, $this->route_parts);
    }

    /**
     * Getting more route parts, without basic :controller and :action
     *
     * @return array
     */
    protected function get_route_others_parts() {
        return array_diff($this->route_parts, $this->required_dynamic_params);
    }

    /**
     * Checking whether the dynamic part of the route
     *
     * @param string $route_part
     * @return mixed string key or false
     */
    protected function is_dynamic_route_part($route_part) {
        if (preg_match('/^:(.+)$/u', $route_part, $matches)) {
            return $matches[1];
        }
        return false;
    }
}