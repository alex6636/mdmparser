<?php
/**
* @package pFramework
* @class   profiler
* @author  Alex Sergey (createtruesite@gmail.com)
* @date    December, 2013
* @version $Id
*
* Profiler
*/

namespace lib\pf;
use lib\pf\exceptions\exception_runtime;

class profiler
{
    protected static $benchmarks = array();

    /**
     * Start profiling
     *
     * @param string $group
     * @param string $text
     * @param array $params
     * @return int $key
     */
    public static function start($group, $text = '', array $params = array()) {
        static $key = -1;
        ++ $key;
        static::$benchmarks[$key] = array(
            'group'       =>$group,
            'text'        =>$text,
            'start_time'  =>microtime(true),
            'start_memory'=>memory_get_usage(),
            'stop_time'   =>false,
            'stop_memory' =>false
        ) + $params;
        return $key;
    }

    /**
     * End profiling
     *
     * @param int $key
     * @return array as
     * array(
     *     'group'       =>'group name',
     *     'text'        =>'text',
     *     'start_time'  =>int microtime,
     *     'start_memory'=>int memory,
     *     'stop_time'   =>int microtime,
     *     'stop_memory' =>int memory
     * )
     */
    public static function stop($key) {
        static::get_benchmark($key);
        static::$benchmarks[$key]['stop_time']   = microtime(true);
        static::$benchmarks[$key]['stop_memory'] = memory_get_usage();
        return static::$benchmarks[$key];
    }

    /**
     * Getting the benchmark
     *
     * @param int $key
     * @throws exception_runtime if benchmark is not found
     * @return array as
     * array(
     *     'group'       =>'group name',
     *     'text'        =>'text',
     *     'start_time'  =>int microtime,
     *     'start_memory'=>int memory,
     *     'stop_time'   =>int microtime or false,
     *     'stop_memory' =>int memory or false
     * )
     */
    public static function get_benchmark($key) {
        if (!array_key_exists($key, static::$benchmarks)) {
            throw new exception_runtime('Profiling benchmark not found');
        }
        return static::$benchmarks[$key];
    }

    /**
     * Getting all benchmarks
     *
     * @return array
     */
    public static function get_benchmarks() {
        return static::$benchmarks;
    }

    /**
     * Getting runtime and memory
     *
     * @param int $key
     * @return array as
     * array(0=>$time, 1=>$memory)
     */
    public static function get_total($key) {
        $benchmark = static::get_benchmark($key);
        if ($benchmark['stop_time'] === false) {
            $benchmark = static::stop($key);
        }
        return array(
            $benchmark['stop_time']   - $benchmark['start_time'],
            $benchmark['stop_memory'] - $benchmark['start_memory'],
        );
    }

    /**
     * Getting statistic benchmark
     *
     * @param int $key
     * @param int $precision
     * @return array as
     * array(
     *     'group' =>'group name',
     *     'text'  =>'text',
     *     'time'  =>float time,
     *     'memory'=>float memory (kb)
     * )
     */
    public static function get_stat($key, $precision = 5) {
        $benchmark = static::get_benchmark($key);
        list($time, $memory) = static::get_total($key);
        // filter the start_ * stop_ * and appends totals
        return array_diff_key($benchmark, array('start_time'=>0, 'start_memory'=>1, 'stop_time'=>2, 'stop_memory'=>3)) + array(
            'time'  =>round($time, $precision),
            'memory'=>round($memory / 1024, $precision)
        );
    }

    /**
     * Getting statistic all benchmarks
     *
     * @param int $precision
     * @return array as
     * array(
     *     array(
     *         'group' =>'group name',
     *         'text'  =>'text',
     *         'time'  =>int time,
     *         'memory'=>int memory
     *     )
     *     [,...]
     * )
     */
    public static function get_stats($precision = 5) {
        $stats = array();
        foreach (static::$benchmarks as $key=>$benchmark) {
            $stats[] = static::get_stat($key, $precision);
        }
        return $stats;
    }

    /**
     * Deleting benchmark
     *
     * @param int $key
     */
    public static function delete($key) {
        static::get_benchmark($key);
        unset(static::$benchmarks[$key]);
    }
}