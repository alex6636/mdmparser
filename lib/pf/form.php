<?php
/**
 * @package pFramework
 * @class   form
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    November, 2013
 * @version $Id
 *
 * Form
 */

namespace lib\pf;
use lib\pf\validators\validator;
use lib\pf\exceptions\exception_argument;

abstract class form
{
    const
        // fields
        FIELD_TEXT     = 'text',
        FIELD_RADIO    = 'radio',
        FIELD_CHECKBOX = 'checkbox',
        FIELD_FLAG     = 'flag',
        // validation conditions
        VALIDATE_EMAIL    = 'email',
        VALIDATE_URL      = 'url',
        VALIDATE_REQUIRED = 'required',
        VALIDATE_IN       = 'in',
        VALIDATE_LENGTH   = 'length',
        VALIDATE_REGEX    = 'regex',
        VALIDATE_UIN      = 'uin',
        VALIDATE_SKYPE    = 'skype',
        VALIDATE_CAPTCHA  = 'captcha',
        // result types
        HANDLE_VALUES_AND_ERRORS = 0,
        HANDLE_ONLY_VALUES       = 1,
        HANDLE_ONLY_ERRORS       = 2;

    protected
        $rules,
        $validator;

    /**
     * Constructor
     *
     * @param validator $validator or NULL (validator by default)
     */
    public function __construct(validator $validator = NULL) {
        $this->validator = ($validator !== NULL) ? $validator : new validator;
    }

    /**
     * Rules
     *
     * @return array as
     * array(
     *    0=>string field name
     *        arr[name] if used subarrays
     *    [1=>string type of the field to determine how to get the value
     *        form::FIELD_TYPE_TEXT, form::FIELD_TYPE_RADIO,
     *        form::FIELD_TYPE_CHECKBOX, form::FIELD_TYPE_FLAG, callback
     *        form::FIELD_TYPE_TEXT by default]
     *    [2=>string error text]
     *    [3=>mixed validation condition
     *        form::VALIDATE_EMAIL, form::VALIDATE_URL, form::VALIDATE_REQUIRED,
     *        form::VALIDATE_IN, form::VALIDATE_LENGTH, form::VALIDATE_REGEX,
     *        form::VALIDATE_UIN, form::VALIDATE_SKYPE, form::VALIDATE_CAPTCHA, callback
     *        form::VALIDATE_REQUIRED by default]
     *    [4=>mixed additional validation parameters
     *        passed to the validation function
     *        array(5, 10) / array(5) / array('', 10) for condition form::VALIDATE_LENGTH
     *        string pattern for condition form::VALIDATE_REGEX
     *        string key for condition form::VALIDATE_CAPTCHA
     *        or in any other form for other validation methods]
     *    ['required'=>bool required fields
     *        true by default, if false - for empty value the validation function is not called]
     *    ['validate_only'=>bool only validation field,
     *        value misses the resulting array after handling, false by default]
     *    ['controller'=>string controllers for running condition validation]
     *    ['action']=>string actions for running condition validation]
     * )
     */
    abstract public function get_rules();

    /**
     * Getting validator
     *
     * @return validator
     */
    public function get_validator() {
        return $this->validator;
    }

    /**
     * Setting validator
     *
     * @param validator $validator
     * @return form
     */
    public function set_validator(validator $validator) {
        $this->validator = $validator;
        return $this;
    }

    /**
     * Form handling
     *
     * @param array $arr
     * for example $_POST or $_GET
     * @param int $handle_type
     * HANDLE_VALUES_AND_ERRORS, HANDLE_ONLY_VALUES, HANDLE_ONLY_ERRORS
     * @return array as
     * array(array $values, array $errors) if $validate = true or
     * array $values, if $validate = false
     */
    public function handle_form(array $arr, $handle_type = self::HANDLE_VALUES_AND_ERRORS) {
        $this->prepare_rules();
        $values = $this->get_form_values($arr);
        if ($handle_type === self::HANDLE_ONLY_VALUES) {
            return $this->filter_form_values($values);
        }
        $errors = $this->valid_form_values($values);
        if ($handle_type === self::HANDLE_ONLY_ERRORS) {
            return $errors;
        }
        return array($this->filter_form_values($values), $errors);
    }

    /**
     * Getting current controller
     *
     * @return string
     */
    public function get_current_controller() {
        if ($app = pf::get_app()) {
            if (($request = $app->get_request()) && $request->is_dispatched()) {
               return $request->get_route()->get_controller();
            }
        }
        return false;
    }

    /**
     * Getting current action
     *
     * @return string
     */
    public function get_current_action() {
        if ($app = pf::get_app()) {
            if (($request = $app->get_request()) && $request->is_dispatched()) {
               return $request->get_route()->get_action();
            }
        }
        return false;
    }

    /**
     * Preparing processing rules fo the form
     *
     * The filter rules and actions of the current controller.
     * The field names are converted from type arr [name] [...]
     * To the form array ('arr', 'name', '...').
     * Names that do not contain [] remain unchanged
     *
     * @return form
     */
    protected function prepare_rules() {
        $rules = $this->get_rules();
        $current_controller = $this->get_current_controller();
        $current_action = $this->get_current_action();
        foreach ($rules as $key=>$rule) {
            if (isset($rule['controller'])) {
                if (!$current_controller) {
                    unset($rules[$key]);
                    continue;
                }
                $controllers = arr::trim(explode(',', $rule['controller']));
                if (!in_array($current_controller, $controllers)) {
                    unset($rules[$key]);
                    continue;
                }
            }
            if (isset($rule['action'])) {
                if (!$current_action) {
                    unset($rules[$key]);
                    continue;
                }
                $actions = arr::trim(explode(',', $rule['action']));
                if (!in_array($current_action, $actions)) {
                    unset($rules[$key]);
                }
            }
            if (isset($rule[0])) {
                preg_match_all('/\[([^\]]*)\]/ui', $rule[0], $matches);
                if (!empty($matches[0])) {
                    $rules[$key][0] = array_merge((array) str_replace($matches[0], '', $rule[0]), $matches[1]);
                }
            }
        }
        $this->rules = $rules;
        return $this;
    }

    /**
     * Getting for values
     *
     * @param array $arr
     * for example $_POST or $_GET
     * @return array $values as array('name'=>'value'[, 'text'=>'value'[,...]])
     */
    protected function get_form_values(array $arr) {
        $values = array();
        foreach ($this->rules as $rule) {
            list($name, $type) = arr::extract($rule, array(0=>NULL, 1=>self::FIELD_TEXT));
            if (!is_array($name)) {
                $value = arr::get($arr, $name);
                $values[$name] = $this->get_field_value_by_type($type, $value);
            } else {
                $value = arr::depth($arr, $name);
                arr::push_branch($values, $name, $this->get_field_value_by_type($type, $value));
            }
        }
        return $values;
    }

    /**
     * Form fields validation
     *
     * @param array $values
     * @return array an array of errors after validation
     */
    protected function valid_form_values(array $values) {
        $this->validator->reset_errors();
        $conditions = $this->get_valid_conditions();
        foreach ($conditions as $condition) {
            list($name, $error, $condition_type, $condition_params, $required) = $condition;
            $value = !is_array($name) ? arr::get($values, $name) : arr::depth($values, $name);
            if (!$required && empty($value)) {
                continue;
            }
            $this->validator->check($value, $error, $condition_type, $condition_params);
        }
        return $this->validator->get_errors();
    }

    /**
     * Filtering form fields from those
     * values ​​are only for validation
     *
     * @param array $values as array('name'=>'value', 'text'=>'value'[,...])
     * @return array an array of errors after validation
     */
    protected function filter_form_values(array $values) {
        foreach ($this->rules as $rule) {
            list($name, $validate_only) = arr::extract($rule, array(0=>NULL, 'validate_only'=>false), false);
            if ($validate_only) {
                unset($values[$name]);
            }
        }
        return $values;
    }

    /**
     * Getting validation condition
     *
     * @return array $conditions fields as
     * array(
     *     [0]=>string name
     *     [1]=>string error text
     *     [2]=>mixed validation condition
     *     [4]=>mixed additional validation parameters
     *     ['self::VALIDATE_REQUIRED']=>bool required fields
     * )
     *
     */
    protected function get_valid_conditions() {
        $conditions = array();
        foreach ($this->rules as $rule) {
            if (isset($rule[2])) {
                $conditions[] = arr::extract(
                    $rule,
                    // by default
                    array(
                        0=>NULL,                       // name
                        2=>NUll,                       // error text
                        3=>self::VALIDATE_REQUIRED,    // validation condition
                        4=>NULL,                       // parameters
                        self::VALIDATE_REQUIRED=>true, // filling flag
                    ),
                    false
                );
            }
        }
        return $conditions;
    }

    /**
     * Getting the value of the field by type
     *
     * @param mixed $type field type
     * @param mixed $value passed value from a form
     * @throws exception_argument if an invalid field type
     * @return mixed
     */
    protected function get_field_value_by_type($type, $value) {
        switch($type) {
            case self::FIELD_TEXT:
            case self::FIELD_RADIO:
            case self::FIELD_CHECKBOX:
            case NULL:
                return trim((string) $value);
            case self::FIELD_FLAG:
                return !empty($value) ? 1 : 0;
            default:
                if (!is_callable($type)) {
                    throw new exception_argument('Field type {' . var_export($type, true) . '} is invalid');
                }
                return call_user_func($type, $value);
        }
    }
}