<?php
/**
 * @package pFramework
 * @class   dictionary
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    August, 2013
 * @version $Id
 *
 * Dictionaries of internationalization
 */

namespace lib\pf\i18n;
use lib\pf\arr;
use lib\pf\exceptions\exception_file;
use lib\pf\config\config;
use lib\pf\config\file\config_arr;

class dictionary
{
    protected $dictionaries = array(); // dictionaries

    /**
     * Load dictionary
     *
     * @param string $dictionary_path path to dictionary file
     * @throws exception_file if dictionary file is not exists
     * @return config $dictionary config object or false
     */
    public function load($dictionary_path) {
        if (!$dictionary = $this->get_loaded($dictionary_path)) {
            if (!file_exists($dictionary_path)) {
                throw new exception_file($dictionary_path, 1);
            }
            // loading dictionary
            $dictionary = new config(new config_arr($dictionary_path));
            $this->dictionaries[$dictionary_path] = $dictionary;
        }
        return $dictionary;
    }

    /**
     * Checking dictionary
     *
     * @param string $dictionary_path path to dictionary file
     * @return config_arr dictionary object pf_config_file_array or false
     */
    public function get_loaded($dictionary_path) {
        return arr::get($this->dictionaries, $dictionary_path, false);
    }

    /**
     * Getting number of loaded dictionaries
     *
     * @return int
     */
    public function get_number_loaded() {
        return count($this->dictionaries);
    }
}