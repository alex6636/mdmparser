<?php
/**
 * @package pFramework
 * @class   transliterator
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * Transliterator
 */

namespace lib\pf\i18n;

class transliterator
{
    protected
        // table of the transliteration
        $transtable = array(
            'а'=>'a',  'А'=>'A',  'б'=>'b',    'Б'=>'B',    'в'=>'v',  'В'=>'V',
            'г'=>'g',  'Г'=>'G',  'д'=>'d',    'Д'=>'D',    'е'=>'e',  'Е'=>'E',
            'ё'=>'e',  'Ё'=>'E',  'ж'=>'zh',   'Ж'=>'ZH',   'з'=>'z',  'З'=>'Z',
            'и'=>'i',  'И'=>'I',  'й'=>'y',    'Й'=>'Y',    'к'=>'k',  'К'=>'K',
            'л'=>'l',  'Л'=>'L',  'м'=>'m',    'М'=>'M',    'н'=>'n',  'Н'=>'N',
            'о'=>'o',  'О'=>'O',  'п'=>'p',    'П'=>'P',    'с'=>'s',  'С'=>'S',
            'т'=>'t',  'Т'=>'T',  'у'=>'u',    'У'=>'U',    'ф'=>'f',  'Ф'=>'F',
            'х'=>'h',  'Х'=>'H',  'ц'=>'ts',   'Ц'=>'TS',   'ч'=>'ch', 'Ч'=>'CH',
            'ш'=>'sh', 'Ш'=>'SH', 'щ'=>'sch',  'Щ'=>'SCH',  'ъ'=>'y',  'Ъ'=>'Y',
            'ы'=>'i',  'Ы'=>'I',  'ь'=>'',     'Ь'=>'',     'э'=>'e',  'Э'=>'E',
            'ю'=>'yu', 'Ю'=>'YU', 'я'=>'ya',   'Я'=>'YA'
        );

    /**
     * Transliteration
     *
     * @param string $source_text source text
     * @return string transliteration result
     */
    public function translate($source_text) {
        return strtr($source_text, $this->transtable);
    }

    /**
     * Getting transliteration table
     *
     * @return array
     */
    public function get_transtable() {
        return $this->transtable;
    }

    /**
     * Setting transliteration table
     *
     * @param array $transtable
     * @return transliterator
     */
    public function set_transtable(array $transtable) {
        $this->transtable = $transtable;
        return $this;
    }
}