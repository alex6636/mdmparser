<?php
/**
 * @package pFramework
 * @class   i18n_factory
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    November, 2013
 * @version $Id
 *
 * Internationalization, Factory
 */

namespace lib\pf\i18n;

class i18n_factory
{
    /**
     * Creation an i18n-object
     *
     * @param array $config configuration
     * array(
     *     ['dirs']         =>array
     *     [,'default'      =>string,]
     *     [,'allowed_langs'=>array,]
     *     [,'auto_detect'  =>bool,]
     * )
     * @return i18n $i18n
     */
    public static function make(array $config) {
        $config = $config + array(
            'dirs'         =>NULL,
            'default'      =>'en',
            'allowed_langs'=>NULL,
            'auto_detect'  =>true
        );
        $i18n = new i18n($config['default'], $config['auto_detect']);
        $i18n->set_dictionary_dirs((array) $config['dirs'])
             ->set_allowed_langs((array) $config['allowed_langs']);
        return $i18n;
    }
}