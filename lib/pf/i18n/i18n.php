<?php
/**
 * @package pFramework
 * @class   i18n
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    November, 2013
 * @version $Id
 *
 * Internationalization
 */

namespace lib\pf\i18n;
use lib\pf\session;
use lib\pf\str;
use lib\pf\arr;
use lib\pf\exceptions\exception_file;

class i18n
{
    protected
        $dictionary_dirs = array(),         // dictionaries dirs
        $dictionary,                        // dictionary
        $allowed_langs = array('en', 'ru'), // langs
        $source_text_lang = 'en';           // sorce lang

    /**
     * Construct
     *
     * @param string $default_lang default language
     * @param bool $auto_detect auto detection language
     */
    public function __construct($default_lang = 'en', $auto_detect = false) {
        $lang = session::get_instance()->get('lang');
        if (empty($lang)) {
            if ($auto_detect) {
                // browser language
                $lang = $this->get_browser_lang();
            }
            if (empty($lang)) {
                // default language
                $lang = $default_lang;
            }
            $this->change_lang($lang);
        }
        $this->dictionary = new dictionary;
    }

    /**
     * Getting current language
     *
     * @return string
     */
    public function get_lang() {
        return session::get_instance()->get('lang');
    }

    /**
     * Change language
     *
     * @param string $lang language
     * @return bool result of a change
     */
    public function change_lang($lang) {
        if (($lang !== session::get_instance()->get('lang')) && $this->is_allowed_lang($lang)) {
            session::get_instance()->set('lang', $lang);
            return true;
        }
        return false;
    }

    /**
     * Checking whether a valid language
     *
     * @param string $lang language
     * @return bool checking result
     */
    public function is_allowed_lang($lang) {
        return in_array($lang, $this->allowed_langs);
    }

    /**
     * Getting allowed languages
     *
     * @return array
     */
    public function get_allowed_langs() {
        return $this->allowed_langs;
    }

    /**
     * Setting allowed languages
     *
     * @param array $allowed_langs
     * @return i18n
     */
    public function set_allowed_langs(array $allowed_langs) {
        $this->allowed_langs = $allowed_langs;
        return $this;
    }

    /**
     * Getting the source language
     *
     * @return string
     */
    public function get_source_text_lang() {
        return $this->source_text_lang;
    }

    /**
     * Setting the source language
     *
     * @param string $lang language
     * @return i18n
     */
    public function set_source_text_lang($lang) {
        $this->source_text_lang = $lang;
        return $this;
    }

    /**
     * Getting dictionary directories
     *
     * @return array
     */
    public function get_dictionary_dirs() {
        return $this->dictionary_dirs;
    }

    /**
     * Setting dictionary directories
     *
     * @param array $dirs directories
     * @return i18n
     */
    public function set_dictionary_dirs(array $dirs) {
        foreach ($dirs as $dir) {
            $this->set_dictionary_dir($dir);
        }
        return $this;
    }

    /**
     * Setting dictionary directory
     *
     * @param string $dir directory
     * @throws exception_file if directory is not exists
     * @return i18n
     */
    public function set_dictionary_dir($dir) {
        $dir = str::append_last_slash($dir);
        if (!in_array($dir, $this->dictionary_dirs)) {
            if (!is_dir($dir)) {
                throw new exception_file($dir, 13);
            }
            $this->dictionary_dirs[] = $dir;
        }
        return $this;
    }

    /**
     * Translation
     *
     * @param string $source_text source text
     * @throws exception_file if $dictionary_dir is not a directory
     * @return string translation result
     */
    public function translate($source_text) {
        $lang = $this->get_lang();
        if ($lang !== $this->source_text_lang) {
            if (!empty($this->dictionary_dirs)) {
                foreach ($this->dictionary_dirs as $dictionary_dir) {
                    // path to dictionary
                    $dictionary_path = $dictionary_dir . $lang . '.php';
                    if ($dictionary = $this->dictionary->load($dictionary_path)) {
                        // find translation
                        if ($translate = $dictionary->get($source_text)) {
                            return $translate;
                        }
                    }
                }
            }
        }
        return $source_text;
    }

    /**
     * Translation with parameters
     *
     * 'English {msg:message text}'=>'Russian {msg}'==>'Russian {message text}'
     *
     * @param string $source_text source text
     * @return string translation result
     */
    public function translate_witch_params($source_text) {
        // extract all parameters of the text
        preg_match_all('/{([^}]*)}/ui', $source_text, $match_params);
        if (!empty($match_params[1])) {
            $params = array();
            foreach ($match_params[1] as $mp) {
                preg_match('/[^:]*/ui', $mp, $p);
                $name  = arr::get($p, 0);
                $value = str_replace($name . ':', '', $mp);
                $params[] = array($name=>$value);
                $source_text = str_replace(':' . $value . '}', '}', $source_text);
            }
            $source_text = $this->translate($source_text, $this->source_text_lang);
            foreach ($params as $param) {
                list($name, $value) = each($param);
                $source_text = str_replace('{' . $name . '}', '{' . $value . '}', $source_text);
            }
            return $source_text;
        }
        return $this->translate($source_text, $this->source_text_lang);
    }

    /**
     * Getting browser language
     *
     * @return string browser language or false
     */
    protected function get_browser_lang() {
        if ($lang_str = $_SERVER['HTTP_ACCEPT_LANGUAGE']) {
            // a line with the language like 'en-US,en;q=0.5'
            // cut all to character '-'
            if (($length = strpos($lang_str, '-')) !== false) {
                return substr($lang_str, 0, $length);
            }
        }
        return false;
    }
}