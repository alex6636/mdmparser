<?php
/**
 * @package pFramework
 * @class   config
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    October, 2013
 * @version $Id
 *
 * Configuration
 */

namespace lib\pf\config;
use lib\pf\config\file\config_file;
use lib\pf\arr;

class config
{
    protected
        $config_file,   // config_file object
        $arr = array(); // config array parameters

    /**
     * Constructor
     *
     * @param config_file $config_file
     */
    public function __construct(config_file $config_file) {
        $this->config_file = $config_file;
        $this->arr = $config_file->get_arr();
    }

    /**
     * Getting file
     *
     * @return config_file
     */
    public function get_config_file() {
        return $this->config_file;
    }

    /**
     * Getting path to file
     *
     * @return string
     */
    public function get_path() {
        return $this->config_file->get_path();
    }

    /**
     * Getting array parameters
     *
     * @return array
     */
    public function get_array() {
        return $this->arr;
    }

    /**
     * Getting parameter value
     *
     * @param mixed $key key of parameter string|array
     * if value have the section, the parameter key has the form array('section'=>'key')
     * @return mixed depends on the parameter type, NULL if parameter is not found
     */
    public function get($key) {
        if (is_array($key)) {
            // section
            list($section, $key) = each($key);
            // check section
            if (array_key_exists($section, $this->arr)) {
                if (!is_array($this->arr[$section])) {
                    return NULL;
                }
                return arr::get($this->arr[$section], $key);
            }
        }
        // does not belong to section
        return arr::get($this->arr, $key);
    }

    /**
     * Setting parameter value
     *
     * @param mixed $key key of parameter string|array
     * if value have the section, the parameter key has the form array('section'=>'key')
     * @param mixed $value parameter value
     */
    public function set($key, $value) {
        if (is_array($key)) {
            // section
            list($section, $k) = each($key);
            if (array_key_exists($section, $this->arr)) {
                if (array_key_exists($k, $this->arr[$section])) {
                    // change the old value
                    $this->arr[$section][$k] = $value;
                } else {
                    // add a new parameter
                    $this->arr[$section][$k] = $value;
                }
            } else {
                // add a new section and parameter
                $this->arr[$section] = array();
                $this->arr[$section][$k] = $value;
            }
        } else {
            // does not belong to section
            if (array_key_exists($key, $this->arr)) {
                // change the old value
                $this->arr[$key] = $value;
            } else {
                // add a new parameter
                $this->arr[$key] = $value;
            }
        }
    }

    /**
     * Writing configuration array to file
     *
     * @return int number of bytes
     */
    public function write() {
        return $this->config_file->write($this->config_file->to_source($this->arr));
    }
}