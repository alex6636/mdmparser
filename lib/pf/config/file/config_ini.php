<?php
/**
 * @package pFramework
 * @class   config_ini
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    October, 2013
 * @version $Id
 *
 * Configuration files as ini-files
 */

namespace lib\pf\config\file;
use lib\pf\exceptions\exception_file;

class config_ini extends config_file
{
    /**
     * Getting an array of parameters
     *
     * @throws exception_file if configuration file is not array
     * @return array $config_arr
     */
    public function get_arr() {
        $config_arr = parse_ini_file($this->path, true);
        if ($config_arr === false) {
            throw new exception_file($this->path, 9);
        }
        return $config_arr;
    }

    /**
     * Transformation to source format
     *
     * @param array $arr array
     * @return string
     */
    public function to_source(array $arr) {
        $source = '';
        foreach ($arr as $key=>$val) {
            if (is_array($val)) {
                // write section
                $source .= '[' . $key . ']' . PHP_EOL;
                foreach ($val as $k=>$v) {
                    // write parameter
                    $source .= $k . '=' . $v . PHP_EOL;
                }
            } else {
                // write parameter
                $source .= $key . '=' . $val . PHP_EOL;
            }
        }
        return $source;
    }
}