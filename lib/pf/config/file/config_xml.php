<?php
/**
 * @package pFramework
 * @class   config_xml
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Configuration files as XML-files
 */

namespace lib\pf\config\file;
use lib\pf\exceptions\exception_runtime;
use lib\pf\exceptions\exception_file;
use lib\pf\arr;

class config_xml extends config_file
{
    protected $xml; // SimpleXML object

    /**
     * Constructor
     *
     * @param string $file_path path to the file
     * @throws exception_runtime if extension simplexml is not installed
     * @throws exception_file if unable to parse xml-file using simplexml_load_file
     */
    public function __construct($file_path) {
        // check simplexml extension
        if (!extension_loaded('simplexml')) {
            throw new exception_runtime('simplexml', 5);
        }
        parent::__construct($file_path);
        // load XML-object
        if (!$this->xml = @simplexml_load_file($this->path)) {
            throw new exception_file($this->path, 9);
        }
    }

    /**
     * Getting an array of parameters
     *
     * @throws exception_file if the XML-file does not contain <config>
     * @return array $config_arr
     */
    public function get_arr() {
        if (!$config = $this->xml->xpath('/config')) {
            throw new exception_file('File {' . $this->path . '} must contain {&lt;config&gt;}');
        }
        return arr::obj2arr(array_shift($config));
    }

    /**
     * Transformation to source format
     *
     * @param array $arr array
     * @return string
     */
    public function to_source(array $arr) {
        // getting config-object
        $config = $this->xml->xpath('/config');
        // remove subnodes
        $config[0][0] = '';
        $this->assemble_to_source($arr, $this->xml);
        return $this->xml->asXML();
    }

    /**
     * Assembling the array and transformation to source format
     *
     * @param array $config_arr configuration array
     * @param \SimpleXMLElement $xml XML-node
     */
    protected function assemble_to_source(array $config_arr, \SimpleXMLElement $xml) {
        foreach ($config_arr as $key=>$val) {
            if (is_array($val)) {
                // section
                $this->assemble_to_source($val, $xml->addChild($key));
            } else {
                // simple parameter
                $xml->addChild($key, $val);
            }
        }
    }
}