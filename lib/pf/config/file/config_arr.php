<?php
/**
 * @package pFramework
 * @class   config_arr
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    October, 2013
 * @version $Id
 *
 * Configuration files as PHP arrays
 */

namespace lib\pf\config\file;
use lib\pf\exceptions\exception_file;

class config_arr extends config_file
{
    /**
     * Getting an array of parameters
     *
     * @throws exception_file if configuration file is not array
     * @return array $config_arr
     */
    public function get_arr() {
        $config_arr = require $this->path;
        if (!is_array($config_arr)) {
            throw new exception_file('File {' . $this->path .
                                     '} must contain {content:&lt;?php return array([...])?&gt;}');
        }
        return $config_arr;
    }

    /**
     * Transformation to source format
     *
     * @param array $arr array
     * @return string
     */
    public function to_source(array $arr) {
        return '<?php' . PHP_EOL . 'return ' . var_export($arr, true) . ';';
    }
}