<?php
/**
 * @package pFramework
 * @class   config_factory
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    October, 2013
 * @version $Id
 *
 * Creating an object of the configuration file, Factory
 */

namespace lib\pf\config\file;
use lib\pf\str;
use lib\pf\config\config;

class config_factory
{
    /**
     * Creating object
     *
     * @param string $file_path path to the configuration file
     * @param string $file_ext_default default extension
     * @return config
     */
    public static function make($file_path, $file_ext_default = PF_CONFIG_FILE_DEFAULT_EXT) {
        // define an extension of the file
        if (!$file_ext = str::extension($file_path)) {
            // use and appends default extension
            $file_ext = $file_ext_default;
            $file_path .= $file_ext;
        }
        // check extension
        switch ($file_ext) {
            case '.xml':
                return new config(new config_xml($file_path));
            case '.ini':
                return new config(new config_ini($file_path));
            case '.php':
            default:
                return new config(new config_arr($file_path));
        }
    }
}