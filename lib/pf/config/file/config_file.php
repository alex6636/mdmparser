<?php
/**
 * @package pFramework
 * @class   config_file
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    October, 2013
 * @version $Id
 *
 * Configuration files
 */

namespace lib\pf\config\file;
use lib\pf\exceptions\exception_file;
use lib\pf\file;

abstract class config_file
{
    protected $path;

    /**
     * Constructor
     *
     * @param string $path path to the file
     */
    public function __construct($path) {
        $this->set_path($path);
    }

    /**
     * Getting an array of parameters
     *
     * @return array
     */
    abstract public function get_arr();

    /**
     * Transformation to source format
     *
     * @param array $arr array
     * @return string
     */
    abstract public function to_source(array $arr);

    /**
     * Getting path to the file
     *
     * @return string
     */
    public function get_path() {
        return $this->path;
    }

    /**
     * Setting path to the file
     *
     * @param string $path path
     * @throws exception_file if $path does not point to a file
     * @return config_file
     */
    public function set_path($path) {
        if (!is_file($path)) {
            throw new exception_file($path, 1);
        }
        $this->path = $path;
        return $this;
    }

    /**
     * Writing configuration file
     *
     * @param string $content
     * @return int number of bytes
     */
    public function write($content) {
        file::create_writable_file($this->path);
        return file::put_contents($this->path, $content);
    }
}