<?php
/**
 * @package pFramework
 * @class   request
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Request
 */

namespace lib\pf;
use lib\pf\routers\route\route;
use lib\pf\exceptions\exception_runtime;
use lib\pf\exceptions\exception_http;

class request
{
   /** @var route $route */
    protected $route;

   /** @var parser_url $parser_url */
    protected $parser_url;

    /**
     * Constructor
     *
     * @param parser_url $parser_url
     */
    public function __construct(parser_url $parser_url) {
        $this->set_parser_url($parser_url);
    }

    /**
     * Checking the dispatching request
     *
     * @return bool
     */
    public function is_dispatched() {
        return ($this->route !== NULL);
    }

    /**
     * Getting the current route
     *
     * @return route
     */
    public function get_route() {
        return $this->route;
    }

    /**
     * Setting the current route
     *
     * @param route $route
     * @return request
     */
    public function set_route(route $route) {
        $this->route = $route;
        return $this;
    }

    /**
     * Getting the URL parser
     *
     * @return parser_url
     */
    public function get_parser_url() {
        return $this->parser_url;
    }

    /**
     * Setting the URL parser
     *
     * @param parser_url $parser_url
     * @return request
     */
    public function set_parser_url(parser_url $parser_url) {
        $this->parser_url = $parser_url;
        return $this;
    }

    /**
     * Getting the current route parameters
     *
     * @param string $key
     * @param string $default
     * @return mixed
     */
    public function get_param($key, $default = NULL) {
        return arr::get($this->route->get_params(), $key, $default);
    }

    /**
     * Redirect
     *
     * @param string $url
     * @param int $code HTTP-code
     * @throws exception_runtime if the headers is already sent
     */
    public function redirect($url, $code = 302) {
        if (headers_sent()) {
            throw new exception_runtime('Headers already sent');
        }
        header('location: ' . $url, true, $code); exit;
    }

    /**
     * Failed request
     */
    public function failed() {
        if (!$this->is_ajax()) {
            echo new exception_http('', 404);
        }
    }

    /**
     * Checking whether the request is AJAX-request
     *
     * @return bool
     */
    public function is_ajax() {
        return (strtolower(arr::get($_SERVER, 'HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest');
    }

    /**
     * Checking the existence of the query
     *
     * @param string $method GET/POST/HEAD/etc
     * @return bool
     */
    public function is_method($method) {
        return (strtoupper($method) === arr::get($_SERVER, 'REQUEST_METHOD'));
    }
}