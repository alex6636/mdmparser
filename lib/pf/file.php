<?php
/**
 * @package pFramework
 * @class   file
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * File
 */

namespace lib\pf;
use lib\pf\exceptions\exception_file;

class file
{
    /**
     * Getting the full path
     *
     * @param string $path
     * @return mixed full path to the file or false, if file is not exists
     */
    public static function full_path($path) {
        if ($realpath = realpath($path)) {
            return str_replace('\\', '/', $realpath);
        }
        return false;
    }

    /**
     * Setting paths to incude_path
     *
     * @param mixed $paths string path or an array of file paths
     */
    public static function set_include_paths($paths) {
        $paths = static::array_to_include_path((array) $paths);
        set_include_path(
            '.'
            . PATH_SEPARATOR
            . $paths
            . PATH_SEPARATOR
            . get_include_path()
        );
    }

    /**
     * Convert array Include path
     *
     * @param array $arr
     * @return string
     */
    public static function array_to_include_path($arr) {
        return implode(PATH_SEPARATOR, $arr);
    }

    /**
     * Convert Include path to array
     *
     * @return array
     */
    public static function include_path_to_array() {
        return explode(PATH_SEPARATOR, get_include_path());
    }

    /**
     * Getting file size in megabytes
     *
     * @param string $path
     * @throws exception_file if file is not exists
     * @return float
     */
    public static function size_mb($path) {
        if (!file_exists($path)) {
            throw new exception_file($path, 1);
        }
        return filesize($path) / 1024 / 1024;
    }

    /**
     * Getting rights to file (directory) as 0777/0666
     *
     * @param string $path
     * @throws exception_file if file is not exists
     * @return int
     */
    public static function perms($path) {
        if (!file_exists($path)) {
            throw new exception_file($path, 1);
        }
        return substr(decoct(fileperms($path)), 2);
    }

    /**
     * Getting a unique file name
     *
     * @param int $length file, up to 32 characters
     * @param string $extension file extension
     * @return string
     */
    public static function unique_name($length = 32, $extension = '') {
        return substr(md5(uniqid()), 0, $length) . $extension;
    }

    /**
     * Creating a directory with writing rights
     *
     * @param string $dir
     * @throws exception_file if unable to create the directory
     */
    public static function create_writable_dir($dir) {
        if (!is_dir($dir)) {
            @mkdir($dir, 0777);
        }
        if (!is_writable($dir) && !@chmod($dir, 0777)) {
            throw new exception_file($dir, 16);
        }
    }

    /**
     * Creating a file with writing rights
     *
     * @param string $path
     * @throws exception_file if can't open the file
     * @throws exception_file if can't create file with writing rights
     */
    public static function create_writable_file($path) {
        if (!is_file($path)) {
            if (!$file = fopen($path, 'a')) {
                throw new exception_file($path, 5);
            }
            fclose($file);
        }
        if (!is_writable($path) && !@chmod($path, 0777)) {
            throw new exception_file($path, 5);
        }
    }

    /**
     * Getting file contents
     *
     * @param string $path
     * @throws exception_file if $path does not file
     * @throws exception_file if cannot read the file
     * @return string
     */
    public static function get_contents($path) {
        if (!is_file($path)) {
            throw new exception_file($path, 1);
        }
        $content = file_get_contents($path);
        if ($content === false) {
            throw new exception_file($path, 3);
        }
        return $content;
    }

    /**
     * Putting the file contents
     *
     * @param string $path
     * @param string $content
     * @param int $flags flags for file_put_contents
     * @throws exception_file if cannot write the file
     * @return int $bytes
     */
    public static function put_contents($path, $content, $flags = LOCK_EX) {
        $bytes = file_put_contents($path, $content, $flags);
        if ($bytes === false) {
            throw new exception_file($path, 4);
        }
        return $bytes;
    }

    /**
     * Time of last modified to the directory
     *
     * @param string $dir
     * @throws exception_file if $dir is not directory
     * @return int $dirmtime timestamp
     */
    public static function dirmtime($dir) {
        if (!is_dir($dir)) {
            throw new exception_file($dir, 13);
        }
        $dir = str::append_last_slash($dir);
        $dirmtime = filemtime($dir);
        if ($dir_handle = opendir($dir)) {
            while (($file = readdir($dir_handle)) !== false) {
                $filemtime = filemtime($dir . '/' .$file);
                if ($filemtime > $dirmtime) {
                    $dirmtime = $filemtime;
                }
            }
            closedir($dir_handle);
        }
        return $dirmtime;
    }

    /**
     * Deleting a file (directory)
     *
     * @param string $path
     * @throws exception_file if unable to open directory
     * @throws exception_file if can't delete the file (directory)
     */
    public static function delete($path) {
        if (is_dir($path)) {
            if (!$dir = opendir($path)) {
                throw new exception_file($path, 14);
            }
            $path = str::append_last_slash($path);
            while (($file = readdir($dir)) !== false) {
                if ($file != '.' && $file != '..') {
                    file::delete($path . $file);
                }
            }
            if (!@rmdir($path)) {
                throw new exception_file($path, 17);
            }
            closedir($dir);
        } else {
            if (!@unlink($path)) {
                throw new exception_file($path, 6);
            }
        }
    }
}