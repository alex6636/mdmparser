<?php
/**
 * @package pFramework
 * @class   error_handler
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Error handler
 */

namespace lib\pf\exceptions;
use \Exception;
use lib\pf\arr;
use lib\pf\loggers\logger;
use lib\pf\loggers\logger_exception;
use lib\pf\tracer\tracer_error;
use lib\pf\tracer\tracer_exception;

class error_handler
{
    protected static $error_types = array(
        E_ERROR  =>'Error',
        E_WARNING=>'Warning',
        E_NOTICE =>'Notice',
    );

    /**
     * Handler
     *
     * @param int $code code
     * @param string $message message
     * @param string $file error file
     * @param string $line error line
     * @throws error
     */
    public static function handler($code, $message, $file, $line) {
        if (ob_get_length() !== false) {
            // clean buffer
            ob_get_clean();
        }
        try {
            // generate error
            throw new error($message, $code, 1, $file, $line);
        } catch (error $error) {
            $error_type = arr::get(static::$error_types, $error->getCode(), 'Error');
            if (PF_LOGS) {
                try {
                    // write logs
                    $logger_exception = new logger_exception(
                        new logger(PF_LOGS_ERRORS_PATH),
                        $error_type,
                        $error->getMessage(),
                        $error->getCode(),
                        $error->getFile(),
                        $error->getLine()
                    );
                    $logger_exception->write();
                } catch (Exception $exception) {
                    if (PF_DEBUG) {
                        echo new tracer_exception(
                            get_class($exception),
                            $exception->getMessage(),
                            $exception->getCode(),
                            $exception->getFile(),
                            $exception->getLine(),
                            $exception->getTrace()
                        );
                    } else {
                        echo new exception_http('', 503);
                    }
                    exit;
                }
            }
            if (PF_DEBUG) {
                echo new tracer_error(
                    $error_type,
                    $error->getMessage(),
                    $error->getCode(),
                    $error->getFile(),
                    $error->getLine(),
                    $error->getTrace()
                );
            } else {
                echo new exception_http('', 503);
            }
            exit;
        }
    }
}