<?php
/**
 * @package pFramework
 * @class   exception_handler
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Exception handler
 */

namespace lib\pf\exceptions;
use \Exception;
use lib\pf\loggers\logger;
use lib\pf\loggers\logger_exception;
use lib\pf\tracer\tracer_exception;

class exception_handler
{
    /**
     * Handler
     *
     * @param Exception $exception
     */
    public static function handler(Exception $exception) {
        if (ob_get_length() !== false) {
            // clean buffer
            ob_get_clean();
        }
        if (PF_LOGS) {
            try {
                // write logs
                $logger_exception = new logger_exception(
                    new logger(PF_LOGS_EXCEPTIONS_PATH),
                    get_class($exception),
                    $exception->getMessage(),
                    $exception->getCode(),
                    $exception->getFile(),
                    $exception->getLine()
                );
                $logger_exception->write();
            } catch (Exception $exception) {
                if (PF_DEBUG) {
                    echo new tracer_exception(
                        get_class($exception),
                        $exception->getMessage(),
                        $exception->getCode(),
                        $exception->getFile(),
                        $exception->getLine(),
                        $exception->getTrace()
                    );
                } else {
                    echo new exception_http('', 503);
                }
                exit;
            }
        }
        if (PF_DEBUG) {
            echo new tracer_exception(
                get_class($exception),
                $exception->getMessage(),
                $exception->getCode(),
                $exception->getFile(),
                $exception->getLine(),
                $exception->getTrace()
            );
        } else {
            echo new exception_http('', 503);
        }
        exit;
    }
}