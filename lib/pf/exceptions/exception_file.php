<?php
/**
 * @package pFramework
 * @class   exception_file
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * Exception Handling associated with the wrong files
 */

namespace lib\pf\exceptions;

class exception_file extends exception_base
{
    /**
     * Standard error messages
     *
     * @return array
     */
    protected function get_standard_messages() {
        return array(
            0 =>'Unknown file exception',
            1 =>'File is not exists',
            2 =>'Failed to open file',
            3 =>'Failed to read file',
            4 =>'Failed to write file',
            5 =>'Failed to create file',
            6 =>'Failed to delete file',
            7 =>'Failed to save file',
            8 =>'Failed to upload file',
            9 =>'Failed to parse file',
            10=>'Wrong type of the file',
            11=>'Failed to lock file',
            12=>'Failed to unlock file',
            13=>'Directory is not exists',
            14=>'Failed to open directory',
            15=>'Failed to write directory',
            16=>'Failed to create directory',
            17=>'Failed to delete directory',
        );
    }
}