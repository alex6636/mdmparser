<?php
/**
 * @package pFramework
 * @class   exception_runtime
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * Handling exceptions that occur during execution
 */

namespace lib\pf\exceptions;

class exception_runtime extends exception_base
{
    /**
     * Standard error messages
     *
     * @return array
     */
    protected function get_standard_messages() {
        return array(
            0=>'Unknown runtime exception',
            1=>'Class is not exists',
            2=>'Method is not exists',
            3=>'Not possible to call',
            4=>'Not possible to load',
            5=>'Extension is not loaded',
            6=>'PHP function is not exists',
        );
    }
}