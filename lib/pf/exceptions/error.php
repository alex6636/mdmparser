<?php
/**
 * @package pFramework
 * @class   error
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * Error handling
 */

namespace lib\pf\exceptions;
use \Exception;
use \ErrorException;
use lib\pf\event;
use lib\pf\arr;

class error extends ErrorException
{
    const EVENT_ON_ERROR = 'error'; // error event

    // error types
    protected $types = array(
        E_ERROR            =>'Error',
        E_WARNING          =>'Warning',
        E_PARSE            =>'Parsing Error',
        E_NOTICE           =>'Notice',
        E_CORE_ERROR       =>'Core Error',
        E_CORE_WARNING     =>'Core Warning',
        E_COMPILE_ERROR    =>'Compile Error',
        E_COMPILE_WARNING  =>'Compile Warning',
        E_USER_ERROR       =>'User Error',
        E_USER_WARNING     =>'User Warning',
        E_USER_NOTICE      =>'User Notice',
        E_STRICT           =>'Strict Notice',
        E_RECOVERABLE_ERROR=>'Recoverable Error'
    );

    /**
     * Constructor
     *
     * @param string $message message
     * @param int $code code
     * @param int $severity severity
     * @param string $filename file name
     * @param int $lineno line number
     * @param Exception $previous previous exception
     */
    public function __construct(
        $message  = '',
        $code     = 0,
        $severity = 1,
        $filename = __FILE__,
        $lineno   = __LINE__,
        Exception $previous = NULL
    ) {
        // event
        event::trigger(self::EVENT_ON_ERROR, func_get_args());
        $message = $this->get_full_message($message, $code);
        parent::__construct($message, $code, $severity, $filename, $lineno, $previous);
    }

    /**
     * Full error message
     *
     * @param string $message message
     * @param int $code code
     * @return string
     */
    protected function get_full_message($message, $code) {
        return arr::get($this->types, $code, 'Unknown error') . ': ' . $message;
    }
}