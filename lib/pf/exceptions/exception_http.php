<?php
/**
 * @package pFramework
 * @class   exception_http
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * HTTP Exception
 */

namespace lib\pf\exceptions;

class exception_http extends exception_base
{
    /**
     * Display exception
     *
     * @return string
     */
    public function __toString() {
        header('HTTP/1.0 ' . $this->getCode() . ' ' . $this->getMessage());
        ob_start();
        include 'html/exception_http.php';
        return ob_get_clean();
    }

    /**
     * Standard error messages
     *
     * @return array
     */
    protected function get_standard_messages() {
        return array(
            404=>'Page Not Found',
            503=>'Internal Server Error',
        );
    }
}