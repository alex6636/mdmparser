<?php
/**
 * @package pFramework
 * @class   exception_argument
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * Exception Handling associated with the arguments of the wrong type
 */

namespace lib\pf\exceptions;

class exception_argument extends exception_base
{
    /**
     * Standard error messages
     *
     * @return array
     */
    protected function get_standard_messages() {
        return array(
            0=>'Unknown argument exception',
            1=>'Argument should be an integer',
            2=>'Argument should be an float',
            3=>'Argument should be an double',
            4=>'Argument should be an string',
            5=>'Argument should be an array',
            6=>'Argument should be an object',
            7=>'Argument must be instance of',
        );
    }
}