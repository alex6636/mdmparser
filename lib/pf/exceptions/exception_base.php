<?php
/**
 * @package pFramework
 * @class   exception_base
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    September, 2013
 * @version $Id
 *
 * The base class of exception handling
 */

namespace lib\pf\exceptions;
use \Exception;
use lib\pf\event;
use lib\pf\arr;

abstract class exception_base extends Exception
{
    const EVENT_ON_EXCEPTION = 'exception'; // event

    /**
     * Constructor
     *
     * @param string $message message
     * @param int $code code
     * @param Exception $previous previous exception
     */
    public function __construct($message = '', $code = 0, Exception $previous = NULL) {
        event::trigger(self::EVENT_ON_EXCEPTION, func_get_args());
        $message = $this->get_full_message($message, $code);
        parent::__construct($message, $code, $previous);
    }

    /**
     * Standard error messages
     *
     * @return array
     * array(
     *     ['0'=>'Message 1'
     *     ,'1'=>'Message 2'
     *     ,...]
     * )
     */
    abstract protected function get_standard_messages();

    /**
     * Full error message
     *
     * @param string $message message
     * @param int $code code
     * @return string
     */
    protected function get_full_message($message, $code) {
        $standard_messages = $this->get_standard_messages();
        $full_message = arr::get($standard_messages, $code, arr::get($standard_messages, 0, 'Unknown exception'));
        return $full_message ?
               $full_message . ($message ? ': ' . $message : '') :
               $message;
    }
}