<?php
/**
 * @package pFramework
 * @class   error_shutdown_handler
 * @author  Alex Sergey (createtruesite@gmail.com)
 * @date    May, 2014
 * @version $Id
 *
 * Error handing after script shutdown
 */

namespace lib\pf\exceptions;
use \Exception;
use lib\pf\arr;
use lib\pf\loggers\logger;
use lib\pf\loggers\logger_exception;
use lib\pf\tracer\tracer_error_shutdown;
use lib\pf\tracer\tracer_exception;

class error_shutdown_handler
{
    protected static $handle = true;
    protected static $error_types = array(
        E_ERROR=>'Error',
        E_PARSE=>'Parse error',
        E_CORE_ERROR =>'Core error',
        E_CORE_WARNING=>'Core warning',
        E_COMPILE_ERROR=>'Compile error',
        E_COMPILE_WARNING=>'Compile warning',
    );

    /**
     * Handle errors or not
     *
     * @param bool $flag
     */
    public static function handle($flag) {
        static::$handle = $flag;
    }

    /**
     * Handler
     */
    public static function shutdown_handler() {
        if (!static::$handle || (!$error = static::get_last_error())) {
            return NULL;
        }
        if (ob_get_length() !== false) {
            // clean buffer
            ob_get_clean();
        }
        $error_type = arr::get(static::$error_types, $error['type'], 'Error');
        if (PF_LOGS) {
            try {
                // write logs
                $logger_exception = new logger_exception(
                    new logger(PF_LOGS_ERRORS_PATH),
                    $error_type,
                    $error['message'],
                    $error['type'],
                    $error['file'],
                    $error['line']
                );
                $logger_exception->write();
            } catch (Exception $exception) {
                if (PF_DEBUG) {
                    echo new tracer_exception(
                        get_class($exception),
                        $exception->getMessage(),
                        $exception->getCode(),
                        $exception->getFile(),
                        $exception->getLine(),
                        $exception->getTrace()
                    );
                } else {
                    echo new exception_http('', 503);
                }
                exit;
            }
        }
        if (PF_DEBUG) {
            echo new tracer_error_shutdown(
                $error_type,
                $error['message'],
                $error['type'],
                $error['file'],
                $error['line'],
                array(
                    array(
                        'file'=>$error['file'],
                        'line'=>$error['line'],
                    ),
                )
            );
        } else {
            echo new exception_http('', 503);
        }
        exit;
    }

    /**
     * Getting last error
     *
     * @return array $error or false
     */
    protected static function get_last_error() {
        $error = error_get_last();
        if (
            ($error !== NULL) && isset($error['type']) &&
            in_array($error['type'], array_keys(static::$error_types))
        ) {
            return $error;
        }
        return false;
    }
}