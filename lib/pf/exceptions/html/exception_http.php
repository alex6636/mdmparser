<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo PF_ENCODING; ?>"/>
        <title><?php echo $this->getCode(); ?> <?php echo $this->getMessage(); ?></title>
        <style>
            * {
                margin: 0;
                padding: 0;
                outline: 0;
            }
            html, body {
                width: 100%;
                height: 100%;
            }
            body {
                background-color: #e6e6e6;
                color: #333;
                font: 1em/1em normal Arial, sans-serif;
            }
            .powered {
                position: absolute;
                right: 1em;
                bottom: 1em;
                text-decoration: underline;
                font: 0.8em/0.8em Arial, sans-serif;
            }
                #main {
                    position: relative;
                    width: 100%;
                    height: 100%;
                }
                    #container {
                        position: absolute;
                        top: 25%;
                        left: 25%;
                        width: 50%;
                        height: 50%;
                        display: block;
                        overflow: hidden;
                        box-shadow: 0 0 10px #333;
                        background-color: #fff;
                    }
                        #content {
                            position: absolute;
                            top: 50%;
                            margin-top: -100px;
                            width: 100%;
                            height: 200px;
                            text-align: center;
                        }
                            #content h1 {
                                font: 8em/1em Arial, sans-serif;
                            }
                            #content h2 {
                                font: 2em/1em Arial, sans-serif;
                            }
                            #content p {
                                margin-top: 1em;
                            }
        </style>
    </head>
    <body>
        <div id="main">
            <div id="container">
                <div id="content">
                    <h1><?php echo $this->getCode(); ?></h1>
                    <h2><?php echo $this->getMessage(); ?></h2>
                    <p>Please go <a href="javascript: history.back()">back</a> or try again</p>
                </div>
                <?php echo lib\pf\pf::powered(); ?>
            </div>
        </div>
    </body>
</html>