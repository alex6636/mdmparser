function initParserForm() {
    var $form = $('#parser_form'),
        $website = $('#website'),
        $location = $('#location'),
        $keyword = $('#keyword'),
        $export_format = $('#export_format'),
        $column_fields = $form.find('.column.fields'),
        $column_results = $form.find('.column.results'),
        $preloader = $('#preloader'),
        $submit = $form.find('button[type="submit"]'),
        $parser_result = $('#parser_result'),
        $fields_list_website = $column_fields.find('.list .list_website'),
        $toggle_fields = $('#toggle_fields'),
        fields = {
            'yellowpages': {
                'category': 'Category', 'city': 'City', 'name': 'Name', 'address': 'Address',
                'state': 'State', 'zip': 'Zip', 'phone': 'Phone', 'categories': 'Categories', 'website': 'Website'
            },
            'yelp': {
                'category': 'Category', 'name': 'Name', 'city': 'City', 'neighborhood': 'Neighborhood'
            }
        };

    $website.change(function() {
        var $selected = $website.find('option:selected'), fields_type = $selected.data('fields');
        var fields_obj = fields[fields_type], fields_html = '';
        $toggle_fields.prop('checked', false);
        hideColumnResults();
        if (fields_obj !== undefined) {
            for (var key in fields_obj) {
                fields_html +=
                    '<div class="item">' +
                        '<label><input type="checkbox" class="option" name="fields[' + key + ']" value="' + fields_obj[key] + '"/><span>' + fields_obj[key] + '</span></label>' +
                    '</div>';
            }
            $fields_list_website.html(fields_html);
            var $fields = $column_fields.find('input[type="checkbox"].option');
            $fields.click(function() {
                if (!$(this).is(':checked')) {
                    if ($toggle_fields.is(':checked')) {
                        $toggle_fields.prop('checked', false);
                    }
                    if (!$fields.filter(':checked').length) {
                        hideColumnResults();
                    }
                } else {
                    if ($fields.length === $fields.filter(':checked').length) {
                        $toggle_fields.prop('checked', true);
                    }
                    showColumnResults()
                }
            });
            showColumnFields();
        } else {
            hideColumnFields();
            hideColumnResults();
        }
    });

    $toggle_fields.click(function() {
        if ($toggle_fields.is(':checked')) {
            $form.find('input[type="checkbox"].option').prop('checked', true);
            showColumnResults()
        } else {
            $form.find('input[type="checkbox"].option').prop('checked', false);
            hideColumnResults();
        }
    });

    $form.submit(function(event) {
        if ($submit.hasClass('disabled')) {
            event.preventDefault();
            return false;
        }
        var errors = 0;
        if (!$website.val()) {
            ++ errors;
            $website.addClass('err');
        } else {
            $website.removeClass('err');
        }
        if (!$.trim($location.val())) {
            ++ errors;
            $location.addClass('err');
        } else {
            $location.removeClass('err');
        }
        if (!$.trim($keyword.val())) {
            ++ errors;
            $keyword.addClass('err');
        } else {
            $keyword.removeClass('err');
        }
        if (!$export_format.val()) {
            ++ errors;
            $export_format.addClass('err');
        } else {
            $export_format.removeClass('err');
        }
        if (!errors) {
            showPreloader();
            var data_str = $form.serialize();
            var controller_url = $website.val();
            $.ajax({
                type: 'POST',
                url: controller_url,
                data: data_str,
                dataType: 'json',
                success: function(json) {
                    var result_html = '';
                    if (json.file !== undefined) {
                        var file = json.file;
                        result_html = '<span>Found <strong>' + file.count + '</strong> results</span>' +
                                      '<a title="' + file.filename + '" href="' + file.href + '" target="_blank">' + getShortFilename(file.filename) + '</a>';
                    } else if (json.error !== undefined) {
                        alert(json.error.message);
                    } else {
                        result_html = '<span>No results found</span>';
                    }
                    $parser_result = $('<div id="parser_result">' + result_html + '</div>').insertAfter($submit);
                    hidePreloader();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert('An occurred error: ' + xhr.status + ' ' + thrownError);
                    hidePreloader();
                }
            });
        }
        event.preventDefault();
        return false;
    });

    function showColumnFields() {
        $column_fields.css('display', 'block');
    }

    function hideColumnFields() {
        $column_fields.css('display', 'none');
    }

    function showColumnResults() {
        $column_results.css('display', 'block');
    }

    function hideColumnResults() {
        $column_results.css('display', 'none');
        var $results = $column_results.find('#parser_result');
        if ($results.length) {
            $results.remove();
        }
    }

    function getShortFilename(filename) {
        if (filename.length > 30) {
            filename = filename.substr(0, 15) + '...' + filename.substr(filename.length - 10, filename.length);
        }
        return filename;
    }

    function showPreloader() {
        $preloader.css('visibility', 'visible');
        $submit.addClass('disabled');
        $parser_result.remove();
    }

    function hidePreloader() {
        $preloader.css('visibility', 'hidden');
        $submit.removeClass('disabled');
    }
}