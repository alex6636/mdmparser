<?php
/**
 * Entry script
 */

use lib\pf\pf;
use lib\pf\routers\route\route_dynamic;
use lib\pf\routers\route\route_dynamic_regex;

require_once '../lib/pf/pf.php';

pf::bootstrap('../bootstrap/dev.php');
$app = pf::create_app('../app/config/dev.php');
$app->get_router()
    ->add_route(
        'basic',
        new route_dynamic(
            ':controller/:action/:param1/:param2',
            array('controller'=>'index', 'action'=>'index')
        ),
        'app\controllers'
    )
    ->add_route(
        'backend',
        new route_dynamic_regex(
            'admin/:controller/:action/:param1/:param2',
            array('controller'=>'auth', 'action'=>'index'),
            array('lang'=>'[a-z]{2}')
        ),
        'app\backend\controllers'
    );
echo $app->run();